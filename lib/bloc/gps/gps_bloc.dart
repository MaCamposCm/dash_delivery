import 'dart:async';

import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

part 'gps_event.dart';
part 'gps_state.dart';

class GpsBloc extends Bloc<GpsEvent, GpsState> {
  StreamSubscription? gpsServiceSubscription;
  final prefs = PreferencesUser();

  GpsBloc()
      : super(const GpsState(
            isGpsEnabled: false,
            isGpsPermissionGranted: false,
            termino: false)) {
    on<GpsPermissionEvent>((event, emit) => emit(state.copyWith(
        isGpsEnabled: event.isGpsEnabled,
        isGpsPermissionGranted: event.isGpsPermissionGranted,
        termino: event.istermino)));
    _init();
  }

  Future<void> _init() async {
    final gpsInitStatus =
        await Future.wait([_checkGpsStatus(), _isPermissionGranted()]);

    add(GpsPermissionEvent(
        isGpsEnabled: gpsInitStatus[0],
        isGpsPermissionGranted: gpsInitStatus[1],
        istermino: true));
  }

  Future<bool> _isPermissionGranted() async {
    final isGranted = await Permission.location.isGranted;
    return isGranted;
  }

  Future<bool> _checkGpsStatus() async {
    final isEnabled = await Geolocator.isLocationServiceEnabled();

    gpsServiceSubscription =
        Geolocator.getServiceStatusStream().listen((event) {
      final isEnable = (event.index == 1) ? true : false;

      add(GpsPermissionEvent(
          isGpsEnabled: isEnable,
          isGpsPermissionGranted: state.isGpsPermissionGranted,
          istermino: state.termino));
    });

    return isEnabled;
  }

  Future<void> setTrue() async {
    add(GpsPermissionEvent(
        isGpsEnabled: state.isGpsEnabled,
        isGpsPermissionGranted: false,
        istermino: false));
  }

  Future<void> askGpsAccess() async {
    final status = await Permission.location.request();
    switch (status) {
      case PermissionStatus.granted:
        add(GpsPermissionEvent(
            isGpsEnabled: state.isGpsEnabled,
            isGpsPermissionGranted: true,
            istermino: true));
        prefs.access = true;
        prefs.rutaPrincipal = "/login";
        break;
      case PermissionStatus.denied:
      case PermissionStatus.provisional:
      case PermissionStatus.restricted:
      case PermissionStatus.limited:
      case PermissionStatus.permanentlyDenied:
        add(GpsPermissionEvent(
            isGpsEnabled: state.isGpsEnabled,
            isGpsPermissionGranted: false,
            istermino: true));
        prefs.access = false;
        prefs.rutaPrincipal = "/loading";
        openAppSettings();
      // TODO: Handle this case.
    }
  }

  @override
  Future<void> close() {
    //TODO_lim´piar stream service statusStream
    gpsServiceSubscription?.cancel();
    return super.close();
  }
}
