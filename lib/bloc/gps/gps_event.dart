part of 'gps_bloc.dart';

abstract class GpsEvent extends Equatable {
  const GpsEvent();

  @override
  List<Object> get props => [];
}

class GpsPermissionEvent extends GpsEvent {
  final bool isGpsEnabled;
  final bool isGpsPermissionGranted;
  final bool istermino;

  const GpsPermissionEvent(
      {required this.isGpsEnabled,
      required this.isGpsPermissionGranted,
      required this.istermino});
}
