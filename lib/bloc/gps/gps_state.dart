part of 'gps_bloc.dart';

class GpsState extends Equatable {
  final bool isGpsEnabled;
  final bool isGpsPermissionGranted;
  final bool termino;

  bool get isAllGranted => isGpsEnabled && isGpsPermissionGranted;

  const GpsState(
      {required this.isGpsEnabled,
      required this.isGpsPermissionGranted,
      required this.termino});

  GpsState copyWith(
          {bool? isGpsEnabled, bool? isGpsPermissionGranted, bool? termino}) =>
      GpsState(
          isGpsEnabled: isGpsEnabled ?? this.isGpsEnabled,
          isGpsPermissionGranted:
              isGpsPermissionGranted ?? this.isGpsPermissionGranted,
          termino: termino ?? this.termino);

  @override
  List<Object> get props => [isGpsEnabled, isGpsPermissionGranted, termino];
}

//class GpsInitial extends GpsState {}
