import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart'
    show LatLng, Marker, MarkerId;

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  StreamSubscription<Position>? positionStream;
  Timer? debounceTimer;

  LocationBloc() : super(const LocationState()) {
    on<OneNewlocationEvent>((event, emit) {
      emit(state.copyWith(lastKnownLocation: event.newLocation));
    });
    on<DisplayMarker>(
        (event, emit) => emit(state.copyWith(markers: event.markers)));
  }

  void startFollowingUser() {
    positionStream?.cancel();

    positionStream = Geolocator.getPositionStream(
            locationSettings: const LocationSettings(
                accuracy: LocationAccuracy.high, distanceFilter: 5))
        .listen((Position position) async {
      final LatLng newLocation = LatLng(position.latitude, position.longitude);

      if (state.lastKnownLocation == null ||
          _hasMovedSignificantly(state.lastKnownLocation!, newLocation)) {
        // Implementa debounce para evitar llamadas repetitivas
        if (debounceTimer?.isActive ?? false) {
          debounceTimer!.cancel();
        }
        debounceTimer = Timer(const Duration(seconds: 1), () {
          add(OneNewlocationEvent(newLocation));
        });
      }
    });
  }

  bool _hasMovedSignificantly(LatLng oldLocation, LatLng newLocation) {
    const double distanceThreshold = 5.0; // 5 metros
    final double distance = Geolocator.distanceBetween(
      oldLocation.latitude,
      oldLocation.longitude,
      newLocation.latitude,
      newLocation.longitude,
    );
    return distance > distanceThreshold;
  }

  void stopFollowingUser() {
    positionStream?.cancel();
    debounceTimer?.cancel();
  }

  @override
  Future<void> close() {
    stopFollowingUser();
    return super.close();
  }
}
