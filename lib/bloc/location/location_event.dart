part of 'location_bloc.dart';

abstract class LocationEvent extends Equatable {
  const LocationEvent();

  @override
  List<Object> get props => [];
}

class OneNewlocationEvent extends LocationEvent {
  final LatLng newLocation;
  const OneNewlocationEvent(this.newLocation);
}

class DisplayMarker extends LocationEvent {
  final Map<String, Marker> markers;
  const DisplayMarker(this.markers);
}
