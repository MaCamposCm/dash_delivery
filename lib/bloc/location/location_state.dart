part of 'location_bloc.dart';

class LocationState extends Equatable {
  final LatLng? lastKnownLocation;
  final Map<String, Marker> markers;

  const LocationState({
    this.lastKnownLocation,
    myLocHistory,
    Map<String, Marker>? markers,
  }) : markers = markers ?? const {};

  LocationState copyWith({
    bool? followingUser,
    LatLng? lastKnownLocation,
    List<LatLng>? myLocHistory,
    Map<String, Marker>? markers,
  }) =>
      LocationState(
          markers: markers ?? this.markers,
          lastKnownLocation: lastKnownLocation ?? this.lastKnownLocation);

  @override
  List<Object?> get props => [lastKnownLocation, markers];
}

//class LocationInitial extends LocationState {}
