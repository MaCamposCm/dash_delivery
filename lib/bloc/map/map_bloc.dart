import 'dart:async';
import 'dart:convert';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dash_delivery/bloc/location/location_bloc.dart';

part 'map_event.dart';
part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final LocationBloc locationBlock;

  GoogleMapController? _mapController;
  StreamSubscription<LocationState>? locationStateSusbcriptions;

  MapBloc({required this.locationBlock}) : super(const MapState()) {
    on<OnMapInitializedEvent>(_onInitMap);
    on<OnStartFollowinUserEvent>(_onStartFollowinUser);
    on<OnStopFollowinUserEvent>(
        (event, emit) => emit(state.copyWith(followUser: false)));
    on<OnStartIsInBus>((event, emit) => emit(state.copyWith(isInBus: true)));
    on<OnStopIsInBus>((event, emit) => emit(state.copyWith(isInBus: false)));
    on<OnsetZoom>((event, emit) => emit(state.copyWith(zoom: event.zoom)));
    on<OnCahngeOcupationOpen>(
        (event, emit) => emit(state.copyWith(changeOcupation: true)));
    on<OnChangeOcupationClose>((event, emit) => emit(
        state.copyWith(changeOcupation: false, ocupation: event.ocupation)));
    on<OnInRoute>(
        (event, emit) => emit(state.copyWith(inRoute: event.inRoute)));
    on<OnInRouteUp>(
        (event, emit) => emit(state.copyWith(inRouteUp: event.inRouteUp)));
    on<OnInRouteTime>((event, emit) =>
        emit(state.copyWith(inRouteTime: event.inRoute, tiempo: event.tiempo)));
    on<GuardarDatos>((event, emit) =>
        emit(state.copyWith(guardar: event.guardar, tGuardar: event.tGuardar)));
    on<GuardarDatosS>(
        (event, emit) => emit(state.copyWith(guardar: event.guardar)));
    on<UpadateUserPolylineEvent>(_onPolyLineNewPoint);
    on<OnToggleUserRoute>(
        (event, emit) => emit(state.copyWith(showMyRotue: !state.showMyRotue)));
    on<OnEventPolylines>(
        (event, emit) => emit(state.copyWith(points: event.getPoints)));
    on<DisplayPolilynesEvent>((event, emit) => emit(
        state.copyWith(polylines: event.polylines, markers: event.markers)));

    on<DisplayPolilynesEvente>(
        (event, emit) => emit(state.copyWith(polylines: event.polylines)));

    locationStateSusbcriptions = locationBlock.stream.listen((locationState) {
      if (!state.followUser) return;
      if (locationState.lastKnownLocation == null) return;

      moveCamera(locationState.lastKnownLocation!);
    });
  }

  void _onInitMap(OnMapInitializedEvent event, Emitter<MapState> emit) {
    _mapController = event.controller;

    emit(state.copyWith(isMapInitialized: true));
    // Suscribe una sola vez a cambios de ubicación
    locationStateSusbcriptions
        ?.cancel(); // Cancela la suscripción anterior si existe
    locationStateSusbcriptions = locationBlock.stream.listen((locationState) {
      // Aquí manejas los cambios de ubicación, como mover la cámara
      if (state.followUser && locationState.lastKnownLocation != null) {
        moveCamera(locationState.lastKnownLocation!);
      }
    });
  }

  void _onStartFollowinUser(
      OnStartFollowinUserEvent event, Emitter<MapState> emit) {
    emit(state.copyWith(followUser: true));
    if (locationBlock.state.lastKnownLocation == null) return;
    moveCamera(locationBlock.state.lastKnownLocation!);
  }

  void _onPolyLineNewPoint(
      UpadateUserPolylineEvent event, Emitter<MapState> emit) {
    final myRoute = Polyline(
        polylineId: const PolylineId('value'),
        color: Colors.black,
        width: 5,
        points: event.userLocation);
    final currentPolilyne = Map<String, Polyline>.from(state.polylines);
    currentPolilyne['value'] = myRoute;
    emit(state.copyWith(polylines: currentPolilyne));
  }

  List<Color> getGradientColors(
      Color startColor, Color endColor, int segmentCount) {
    List<Color> colors = [];
    for (int i = 0; i < segmentCount; i++) {
      final ratio = i / segmentCount;
      final color = Color.lerp(startColor, endColor, ratio)!;
      colors.add(color);
    }
    return colors;
  }

  Future drawRouteBusPolilyne(List<LatLng> busRoute) async {
    var currentPolylines = Map<String, Polyline>();
    var currentMarkers = Map<String, Marker>();
    // add(DisplayPolilynesEvent(currentPolylines, currentMarkers));

    // Esto asume que tienes un evento que actualiza el estado del mapa en tu bloc, estado o controlador

    final int segmentCount = busRoute.length - 1;
    final List<Color> gradientColors =
        getGradientColors(Colors.blue, Colors.red, segmentCount);
    currentPolylines = Map<String, Polyline>.from(state.polylines);
    currentMarkers = Map<String, Marker>.from(state.markers);

    if (busRoute.isEmpty) {
      final myRoute = Polyline(
          polylineId: const PolylineId('route'),
          color: Colors.black,
          width: 5,
          points: busRoute);
      final currentPolilynes = Map<String, Polyline>.from(state.polylines);
      currentPolilynes['route'] = myRoute;
      final currentMarkers = Map<String, Marker>.from(state.markers);

      add(DisplayPolilynesEvent(currentPolilynes, currentMarkers));
    }
  }

  void moveCamera(LatLng newLocation) async {
    final cameraUpdate = CameraUpdate.newLatLngZoom(newLocation, state.zoom);

    _mapController?.animateCamera(cameraUpdate);
  }

  @override
  Future<void> close() {
    locationStateSusbcriptions?.cancel();
    return super.close();
  }
}
