part of 'map_bloc.dart';

abstract class MapEvent extends Equatable {
  const MapEvent();

  @override
  List<Object> get props => [];
}

class OnMapInitializedEvent extends MapEvent {
  final GoogleMapController controller;

  const OnMapInitializedEvent(this.controller);
}

class OnStopFollowinUserEvent extends MapEvent {}

class OnStartFollowinUserEvent extends MapEvent {}

class OnStartIsInBus extends MapEvent {}

class OnStopIsInBus extends MapEvent {}

class OnInRoute extends MapEvent {
  final bool inRoute;
  const OnInRoute(this.inRoute);
}

class OnInRouteUp extends MapEvent {
  final bool inRouteUp;
  const OnInRouteUp(this.inRouteUp);
}

class OnInRouteTime extends MapEvent {
  final bool inRoute;
  final int tiempo;
  const OnInRouteTime(this.inRoute, this.tiempo);
}

class OnCahngeOcupationOpen extends MapEvent {}

class OnChangeOcupationClose extends MapEvent {
  final int ocupation;

  const OnChangeOcupationClose(this.ocupation);
}

class OnEventPolylines extends MapEvent {
  final List<LatLng> getPoints;

  const OnEventPolylines(this.getPoints);
}

class UpadateUserPolylineEvent extends MapEvent {
  final List<LatLng> userLocation;
  const UpadateUserPolylineEvent(this.userLocation);
}

class OnToggleUserRoute extends MapEvent {}

class DisplayPolilynesEvent extends MapEvent {
  final Map<String, Polyline> polylines;
  final Map<String, Marker> markers;

  const DisplayPolilynesEvent(this.polylines, this.markers);
}

class DisplayPolilynesEvente extends MapEvent {
  final Map<String, Polyline> polylines;
  // final Map<String, Marker> markers;

  const DisplayPolilynesEvente(this.polylines);
}

// class DisplayMarker extends MapEvent{
//   final Map<String,Marker> markers;
//   const DisplayMarker(this.markers);
// }
class GuardarDatosS extends MapEvent {
  final bool guardar;
  const GuardarDatosS(this.guardar);
}

class OnsetZoom extends MapEvent {
  final double zoom;
  const OnsetZoom(this.zoom);
}

class GuardarDatos extends MapEvent {
  final bool guardar;
  final int tGuardar;
  const GuardarDatos(this.guardar, this.tGuardar);
}
