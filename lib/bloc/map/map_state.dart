part of 'map_bloc.dart';

class MapState extends Equatable {
  final bool isMapInitialized;
  final bool followUser;
  final bool showMyRotue;
  final List<LatLng> points;
  final bool isInBus;
  final bool changeOcupation;
  final int ocupation;
  final bool inRoute;
  final bool inRouteUp;
  final bool inRouteTime;
  final int tiempo;
  final bool guardar;
  final int tGuardar;
  final double zoom;

//! polylines

  final Map<String, Polyline> polylines;
  final Map<String, Marker> markers;

  const MapState(
      {this.isMapInitialized = false,
      this.followUser = false,
      this.showMyRotue = false,
      Map<String, Polyline>? polylines,
      Map<String, Marker>? markers,
      this.points = const [],
      this.isInBus = false,
      this.changeOcupation = false,
      this.ocupation = 0,
      this.inRoute = false,
      this.inRouteUp = false,
      this.inRouteTime = false,
      this.tiempo = 0,
      this.guardar = true,
      this.tGuardar = 0,
      this.zoom = 14})
      : polylines = polylines ?? const {},
        markers = markers ?? const {};

  MapState copyWith(
          {bool? isMapInitialized,
          bool? followUser,
          bool? showMyRotue,
          Map<String, Polyline>? polylines,
          Map<String, Marker>? markers,
          List<LatLng>? points,
          bool? isInBus,
          bool? changeOcupation,
          int? ocupation,
          bool? inRoute,
          bool? inRouteUp,
          bool? inRouteTime,
          int? tiempo,
          bool? guardar,
          int? tGuardar,
          double? zoom}) =>
      MapState(
          isMapInitialized: isMapInitialized ?? this.isMapInitialized,
          followUser: followUser ?? this.followUser,
          showMyRotue: showMyRotue ?? this.showMyRotue,
          polylines: polylines ?? this.polylines,
          markers: markers ?? this.markers,
          points: points ?? this.points,
          isInBus: isInBus ?? this.isInBus,
          changeOcupation: changeOcupation ?? this.changeOcupation,
          ocupation: ocupation ?? this.ocupation,
          inRoute: inRoute ?? this.inRoute,
          inRouteUp: inRouteUp ?? this.inRouteUp,
          inRouteTime: inRouteTime ?? this.inRouteTime,
          tiempo: tiempo ?? this.tiempo,
          guardar: guardar ?? this.guardar,
          tGuardar: tGuardar ?? this.tGuardar,
          zoom: zoom ?? this.zoom);

  @override
  List<Object> get props => [
        isMapInitialized,
        followUser,
        polylines,
        showMyRotue,
        markers,
        points,
        isInBus,
        changeOcupation,
        ocupation,
        inRoute,
        inRouteUp,
        inRouteTime,
        tiempo,
        guardar,
        tGuardar,
        zoom
      ];
}

//class MapInitial extends MapState {}
