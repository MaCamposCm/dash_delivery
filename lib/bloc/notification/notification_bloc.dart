import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/services/local_notification.dart';

part 'notification_event.dart';
part 'notification_state.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  var mensaje = message.data;
  var title = mensaje['title'];
  var body = mensaje['body'];

  // Manejo de la notificación según si tiene título o no
  print(WidgetsBinding.instance.lifecycleState == AppLifecycleState.paused);
  print(WidgetsBinding.instance.lifecycleState == AppLifecycleState.detached);
  // Verificar si la aplicación está en segundo plano o terminada
  if (WidgetsBinding.instance.lifecycleState == AppLifecycleState.paused) {
    Random random = Random();
    var id = random.nextInt(100000);
    LocalNotification.showLocalNotification(
      id: id,
      title: title.isNotEmpty
          ? title
          : null, // Solo muestra el título si no está vacío
      body: body,
    );
  }
}

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  static bool _isForegroundListenerRegistered = false;

  NotificationBloc() : super(NotificationInitial()) {
    if (!_isForegroundListenerRegistered) {
      _onForeGroundMessage();
      _isForegroundListenerRegistered = true;
    }
  }

  void requestPermission() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      badge: true,
      sound: true,
    );

    await LocalNotification.requestPermissionLocalNotifications();
    _getToken();
  }

  void _getToken() async {
    final settings = await messaging.getNotificationSettings();
    if (settings.authorizationStatus != AuthorizationStatus.authorized) return;

    final token = await messaging.getToken();
    final prefs = PreferencesUser();
    if (token != null) prefs.tokenTel = token;
  }

  void _onForeGroundMessage() {
    FirebaseMessaging.onMessage.listen(handleRemoteMessage);
  }

  void handleRemoteMessage(RemoteMessage message) {
    var title = message.notification?.title;
    var body = message.notification?.body;

    Random random = Random();
    var id =
        random.nextInt(100000); // Genera un ID único para cada notificación

    // Mostrar notificación con título y cuerpo
    LocalNotification.showLocalNotification(
      id: id,
      title: title,
      body: body,
    );
  }
}
