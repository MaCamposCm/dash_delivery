import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/notServices.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/services/verificacionService.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:dash_delivery/bloc/gps/gps_bloc.dart';
import 'package:dash_delivery/bloc/location/location_bloc.dart';
import 'package:dash_delivery/bloc/map/map_bloc.dart';
import 'package:dash_delivery/firebase_options.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';

import 'package:dash_delivery/bloc/notification/notification_bloc.dart';
import 'package:dash_delivery/services/local_notification.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/services/register_service.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await PreferencesUser.init();
  // await Firebase.initializeApp(
  //   options: DefaultFirebaseOptions.currentPlatform,
  // );
  // Inicializar Firebase
  await Firebase.initializeApp();

  // Inicializar el servicio de notificaciones
  await NotiService.initializeApp();
  await LocalNotification.initializeLocalNotifications();
  print("adadsa");
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider(create: (context) => GpsBloc()),
      BlocProvider(create: (context) => LocationBloc()),
      // BlocProvider(create: (context) => NotificationBloc()),
      ChangeNotifierProvider(create: (_) => LoginService()),
      ChangeNotifierProvider(create: (_) => VerificacionService()),
      ChangeNotifierProvider(create: (_) => PlatilloService()),
      ChangeNotifierProvider(create: (_) => SocketSerDos()),
    ],
    child: DashDeliveryApp(),
  ));
}

Future<void> checkForUpdatesAndClearCache(String rutaPrincipal) async {
  await clearAppCache();
  PreferencesUser().rutaPrincipal = rutaPrincipal;
  await appRouters.pushReplacement(rutaPrincipal);
}

Future<void> clearAppCache() async {
  final cacheDir = await getTemporaryDirectory();
  final appDir = await getApplicationSupportDirectory();

  if (cacheDir.existsSync()) {
    cacheDir.deleteSync(recursive: true);
  }

  if (appDir.existsSync()) {
    appDir.deleteSync(recursive: true);
  }
  await PreferencesUser.init(); // Reinitialize preferences after clearing cache
}

class DashDeliveryApp extends StatefulWidget {
  const DashDeliveryApp({super.key});

  @override
  State<DashDeliveryApp> createState() => _AppStateState();
}

class _AppStateState extends State<DashDeliveryApp> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<ScaffoldMessengerState> scaffoldState =
      GlobalKey<ScaffoldMessengerState>();

  // final GlobalKey<ScaffoldMessengerState> messengerKey =
  //     new GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _initializeApp();
    // PushNotificationService.messagesStream.listen((message) {
    //   // print('MyApp: $message');
    //   navigatorKey.currentState?.pushNamed('message', arguments: message);

    //   final snackBar = SnackBar(content: Text(message));
    //   messengerKey.currentState?.showSnackBar(snackBar);
    // });
    // NotiService.messagesStream.listen((message) {
    // if (Preferences.pantalla == "mapa") {
    // Mueve al usuario a la pantalla de chat cuando se recibe un mensaje
    // navigatorKey.currentState?.pushNamed('/chat');
    // }
    // });
  }

  Future<void> _initializeApp() async {
    final prefs = PreferencesUser();
    String initialRoute =
        prefs.rutaPrincipal.isNotEmpty ? prefs.rutaPrincipal : '/login';

    await checkForUpdatesAndClearCache(initialRoute);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark));
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: 'Dash Delivery',
        routerConfig: appRouters,
        scaffoldMessengerKey: scaffoldState,
        theme: ThemeData(
          useMaterial3: true,
          fontFamily:
              'Futura', // Utiliza la fuente "Futura" como la fuente predeterminada
        ));
  }
}
