import 'package:shared_preferences/shared_preferences.dart';

class PreferencesUser {
  static late SharedPreferences _prefs;

  static Future init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  String get token {
    return _prefs.getString('token') ?? '';
  }

  set token(String val) {
    _prefs.setString('token', val);
  }

  String get tokenTel {
    return _prefs.getString('tokenTel') ?? '';
  }

  set tokenTel(String val) {
    _prefs.setString('tokenTel', val);
  }

  String get rutaPrincipal {
    return _prefs.getString('ruta') ?? '/loading';
  }

  set rutaPrincipal(String val) {
    _prefs.setString('ruta', val);
  }

  bool get access {
    return _prefs.getBool('access') ?? false;
  }

  set access(bool val) {
    _prefs.setBool('access', val);
  }

  String get coordenadas {
    return _prefs.getString('coordenadas') ?? '';
  }

  set coordenadas(String val) {
    _prefs.setString('coordenadas', val);
  }

  String get version {
    return _prefs.getString('version') ?? '';
  }

  set version(String val) {
    _prefs.setString('version', val);
  }
}
