import 'package:dash_delivery/screen/HomeScreen.dart';
import 'package:dash_delivery/screen/UpdateScreen.dart';
import 'package:dash_delivery/screen/administracion_Negocio_screen.dart';
import 'package:dash_delivery/screen/administracion_screen.dart';
import 'package:dash_delivery/screen/bloqueo_screen.dart';
import 'package:dash_delivery/screen/combineAdmin.dart';
import 'package:dash_delivery/screen/combineNeg.dart';
import 'package:dash_delivery/screen/conbine.dart';
import 'package:dash_delivery/screen/gp_access_screen.dart';
import 'package:dash_delivery/screen/loading_gps_screen.dart';
import 'package:dash_delivery/screen/loading_screen.dart';
import 'package:dash_delivery/screen/pendiente_scree.dart';
import 'package:dash_delivery/screen/pendiente_screen_admin.dart';
import 'package:go_router/go_router.dart';
import 'package:dash_delivery/screen/dashboard_eleccion.dart';
import 'package:dash_delivery/screen/dashboard_negocio_screen.dart';
import 'package:dash_delivery/screen/dashboard_repartidor_screen.dart';
import 'package:dash_delivery/screen/estatus_pedido.dart';
import 'package:dash_delivery/screen/login_screen.dart';
import 'package:dash_delivery/screen/mis_pedidos.dart';

final appRouters = GoRouter(routes: [
  GoRoute(
    name: '/',
    path: '/',
    builder: (context, state) => LoginScreen(),
  ),
  GoRoute(
    name: '/login',
    path: '/login',
    builder: (context, state) => LoginScreen(),
  ),
  GoRoute(
    name: '/dashboard_negocio',
    path: '/dashboard_negocio',
    builder: (context, state) => const CombinedScreenNeg(),
  ),
  GoRoute(
    name: '/estatusPedidos',
    path: '/estatusPedidos',
    builder: (context, state) => const EstatusPedido(),
  ),
  GoRoute(
    name: '/dashboard_repartidor',
    path: '/dashboard_repartidor',
    builder: (context, state) => const CombinedScreen(),
  ),
  GoRoute(
    name: '/dashboard_eleccion',
    path: '/dashboard_eleccion',
    builder: (context, state) => DashboardEleccion(),
  ),
  GoRoute(
    name: '/misPedidos',
    path: '/misPedidos',
    builder: (context, state) => const MisPedidos(),
  ),
  GoRoute(
    name: '/accessGPS',
    path: '/accessGPS',
    builder: (context, state) => GpsAccesScreen(),
  ),
  GoRoute(
    name: '/loading',
    path: '/loading',
    builder: (context, state) => LoadongGpsScreen(),
  ),
  GoRoute(
    name: '/admin',
    path: '/admin',
    builder: (context, state) => CombinedScreenAdmin(),
  ),
  GoRoute(
    name: '/adminN',
    path: '/adminN',
    builder: (context, state) => AdminScreenNegocio(),
  ),
  GoRoute(
    name: '/PendienteScreen',
    path: '/PendienteScreen',
    builder: (context, state) => PendienteScreen(),
  ),
  GoRoute(
    name: '/PendienteScreenAdmin',
    path: '/PendienteScreenAdmin',
    builder: (context, state) => PendienteScreenAdmin(),
  ),
  GoRoute(
    name: '/BloqueadoScreen',
    path: '/BloqueadoScreen',
    builder: (context, state) => BloqueoScreen(),
  ),
  GoRoute(
    name: '/updateScreen',
    path: '/updateScreen',
    builder: (context, state) => UpdateScreen(),
  ),
]);
