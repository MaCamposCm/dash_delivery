import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GoogleMapController? _mapController;
  LatLng? _initialPosition;
  final TextEditingController _textController = TextEditingController();

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  Future<void> _handleLink(String link) async {
    final resolvedLink = await _resolveShortLink(link);
    if (resolvedLink != null) {
      _extractCoordinatesFromConsentURL(resolvedLink);
    }
  }

  void _extractCoordinatesFromConsentURL(String consentUrl) {
    final uri = Uri.parse(consentUrl);
    // Extraemos la URL de `continue`
    // final continueUrl = uri.queryParameters['continue'];
    if (consentUrl != null) {
      final finalUri = Uri.parse(consentUrl);
      _extractCoordinates(finalUri);
    } else {}
  }

  void _extractCoordinates(Uri uri) {
    final pathSegments = uri.pathSegments;

    String? coordsString;

    // Buscar coordenadas en la URL, puede ser en diferentes partes
    if (pathSegments.isNotEmpty) {
      final coordsSegment = pathSegments
          .firstWhere((segment) => segment.contains('@'), orElse: () => '');
      if (coordsSegment.isNotEmpty) {
        final parts = coordsSegment.split('@');
        if (parts.length > 1) {
          coordsString = parts[1];
        }
      }
    }

    if (coordsString != null) {
      final coordinates = coordsString.split(',');
      if (coordinates.length >= 2) {
        final lat = double.tryParse(coordinates[0]);
        final lng = double.tryParse(coordinates[1]);

        if (lat != null && lng != null) {
          setState(() {
            _initialPosition = LatLng(lat, lng);
            _mapController
                ?.moveCamera(CameraUpdate.newLatLng(_initialPosition!));
          });
        }
      }
    }
  }

  Future<String?> _resolveShortLink(String link) async {
    try {
      final response = await http.get(
        Uri.parse('https://unshorten.me/s/${Uri.encodeComponent(link)}'),
        headers: {
          'Accept': 'application/json',
        },
      );

      if (response.statusCode == 200) {
        final jsonResponse = response.body;
        // final urlNecesaria = jsonResponse['resolved_url'];

        // Extraer la URL después del parámetro `continue`
        final Uri uri = Uri.parse(jsonResponse);
        final continueUrl = uri.queryParameters['continue'];
        // if (continueUrl != null) {
        return Uri.decodeComponent(continueUrl!);
        // } else {
        // }
      } else {}
    } catch (e) {}
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mapa en Flutter'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                labelText: 'Ingresa el enlace de Google Maps',
                border: OutlineInputBorder(),
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              _handleLink(_textController.text);
            },
            child: Text('Interpretar Enlace'),
          ),
          Expanded(
            child: _initialPosition == null
                ? Center(child: Text('Esperando enlace...'))
                : GoogleMap(
                    onMapCreated: (controller) {
                      _mapController = controller;
                    },
                    initialCameraPosition: CameraPosition(
                      target: _initialPosition ?? LatLng(0, 0),
                      zoom: 15,
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
