import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Actualización disponible'),
      // ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Una nueva versión de la aplicación está disponible. Por favor, actualiza a la última versión para continuar usando la aplicación.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 20),
            // ElevatedButton(
            //   onPressed: _launchURL,
            //   child: Text('Actualizar'),
            // ),
          ],
        ),
      ),
    );
  }

  void _launchURL() async {
    const url =
        'https://play.google.com/store/apps/details?id=com.nabiaa.dash_delivery'; // URL de tu aplicación en la tienda de aplicaciones
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'No se pudo abrir $url';
    }
  }
}
