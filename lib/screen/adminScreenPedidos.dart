import 'package:flutter/material.dart';
import 'package:dash_delivery/screen/detalle_pedido_screen_admin.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:dash_delivery/services/socket.dart';
import 'detalle_pedido_screen.dart';

class AdminScreenPedidos extends StatefulWidget {
  const AdminScreenPedidos({super.key});

  @override
  State<AdminScreenPedidos> createState() => _AdminScreenPedidosState();
}

class _AdminScreenPedidosState extends State<AdminScreenPedidos> {
  List<dynamic> _pedidosList = [];
  bool _isLoading = true;
  late SocketSerDos socket;

  @override
  void initState() {
    super.initState();
    socket = Provider.of<SocketSerDos>(context, listen: false);
    _fetchPedidos();

    socket.socketS().on('pedidoActualizado', (data) {
      _fetchPedidos();
    });
  }

  @override
  void dispose() {
    socket.socketS().off('pedidoActualizado');
    socket.socketS().off('pedidosResponse');
    super.dispose();
  }

  Future<void> _fetchPedidos() async {
    setState(() {
      _isLoading = true;
    });

    socket.emit('verTodosPedidos');
    socket.socketS().on('pedidosResponse', (data) {
      if (mounted) {
        setState(() {
          _pedidosList = data;
          _isLoading = false;
        });
      }
    });
  }

  Future<void> _recargarPedidos() async {
    setState(() {
      _isLoading = true;
    });
    await _fetchPedidos();
  }

  void _eliminarPedidoDeLaLista(int id) {
    setState(() {
      _pedidosList =
          _pedidosList.where((pedido) => pedido['id'] != id).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pedidos pendientes/liberados'),
      ),
      body: Stack(
        children: [
          RefreshIndicator(
            onRefresh: _recargarPedidos,
            child: _isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : _pedidosList.isEmpty
                    ? _buildNoPedidosMessage()
                    : ListView.builder(
                        itemCount: _pedidosList.length,
                        itemBuilder: (context, index) {
                          final pedido = _pedidosList[index];
                          final coordenadas =
                              _parseCoordinates(pedido['coordenadas']);
                          final coordenadasI =
                              _parseCoordinates(pedido['coordenadas_inicio']);
                          final registro = _parseDateTime(pedido['registro']);
                          return _buildPedidoCard(
                            context,
                            pedido['id'],
                            pedido['nombre_local'],
                            pedido['descripcion'],
                            pedido['detalle'],
                            pedido['precio'],
                            pedido['ubicacion'],
                            pedido['direccion'],
                            coordenadas,
                            coordenadasI,
                            registro,
                            pedido['total_pedido'],
                            pedido['ganancia'],
                            pedido['estatus'],
                          );
                        },
                      ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoPedidosMessage() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'No se han encontrado pedidos actualmente.',
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _recargarPedidos,
            child: const Text('Recargar'),
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blueAccent,
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
              textStyle: TextStyle(fontSize: 16),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPedidoCard(
    BuildContext context,
    int id,
    String nombreLocal,
    String descripcion,
    String detalle,
    String precio,
    String ubicacion,
    String direccion,
    LatLng coordenadas,
    LatLng coordenadasI,
    String registro,
    String monto,
    String ganancia,
    String estatus,
  ) {
    // Definir los colores de fondo según el estatus
    Color? backgroundColor;
    if (estatus == 'POR RECOLECTAR') {
      backgroundColor = Colors.white;
    } else if (estatus == 'LIBERADO') {
      backgroundColor = Colors.white;
    }

    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      elevation: 2.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: backgroundColor, // Aplicar el color de fondo según el estatus
      child: Stack(
        children: [
          ListTile(
            title: Text(
              nombreLocal,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              textAlign: TextAlign.center,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('De: $ubicacion'),
                Text('A: $direccion'),
                Text('Monto: \$${precio}'),
                Text('Registrado el: $registro'),
                Text('Estatus: $estatus'),
              ],
            ),
            onTap: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetallePedidoScreenAdmin(
                    id: id,
                    nombreLocal: nombreLocal,
                    descripcion: descripcion,
                    detalle: detalle,
                    direccion: direccion,
                    coordenadas: coordenadas,
                    coordenadasI: coordenadasI,
                    precio: precio,
                    monto: monto,
                    ganancia: ganancia,
                  ),
                ),
              );

              if (result != null && result is int) {
                _eliminarPedidoDeLaLista(result);
                _recargarPedidos(); // Recargar pedidos después de eliminar
              }
            },
          ),
          if (estatus == 'LIBERADO')
            Positioned(
              bottom: 10,
              right: 0,
              child: _buildPulsatingPriorityBadge(),
            ),
        ],
      ),
    );
  }

  Widget _buildPulsatingPriorityBadge() {
    return AnimatedOpacity(
      opacity: 0.5,
      duration: Duration(seconds: 1),
      curve: Curves.easeInOut,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: const BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
            child: Transform.rotate(
              angle: 0,
              child: Text(
                'PRIORITARIO',
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
      onEnd: () {
        setState(() {
          // Reversar la opacidad para crear un efecto de pulsación continuo
          _buildPulsatingPriorityBadge();
        });
      },
    );
  }

  LatLng _parseCoordinates(String coordenadas) {
    final parts = coordenadas.split(',');
    final lat = double.parse(parts[0].trim());
    final lng = double.parse(parts[1].trim());
    return LatLng(lat, lng);
  }

  String _parseDateTime(String dateTimeString) {
    try {
      final dateTime = DateTime.parse(dateTimeString);
      final formatter = DateFormat('dd/MM/yyyy HH:mm');
      return formatter.format(dateTime);
    } catch (e) {
      return 'Fecha no válida';
    }
  }
}
