import 'dart:typed_data';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:dash_delivery/services/socket.dart';

class AdminScreenNegocio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Verificación de Negocios'),
      ),
      body: UserListD(),
    );
  }
}

class UserListD extends StatefulWidget {
  @override
  _UserListStateD createState() => _UserListStateD();
}

class _UserListStateD extends State<UserListD> {
  List<dynamic> users = [];
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _fetchUsers();
    final socketService = Provider.of<SocketSerDos>(context, listen: false);
    socketService
        .socketS()
        .on('estadoNegocioCambiado', _handleEstadoNegocioCambiado);
  }

  void _handleEstadoNegocioCambiado(data) {
    if (mounted) {
      setState(() {
        users.removeWhere((user) => user['id'] == data['id']);
      });
    }
  }

  Future<void> _fetchUsers() async {
    setState(() {
      _isLoading = true;
    });
    final socketService = Provider.of<SocketSerDos>(context, listen: false);
    socketService.emit('traerUsuariosPendientesRepartidor', "restaurante");
    socketService.socketS().on('usuariosPendientesRepartidorResponseR', (data) {
      if (mounted) {
        setState(() {
          users = data;
          _isLoading = false;
          if (users.isEmpty) {
            print("No se han encontrado usuarios actualmente.");
          }
        });
      }
    });
  }

  Future<void> _recargarUsuarios() async {
    setState(() {
      _isLoading = true;
    });
    await _fetchUsers();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _recargarUsuarios,
      child: _isLoading
          ? Center(child: CircularProgressIndicator())
          : users.isEmpty
              ? _buildNoUsersMessage()
              : ListView.builder(
                  itemCount: users.length,
                  itemBuilder: (context, index) {
                    final user = users[index];
                    return _buildUserCard(
                        context,
                        user['correo'],
                        user['id'],
                        user['tipo_usuario'],
                        user['ine'],
                        user['ine_atras'],
                        user['coordenadas'],
                        user['nombre_local'],
                        user['ubicacion']);
                  },
                ),
    );
  }

  Widget _buildNoUsersMessage() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'No se han encontrado usuarios actualmente.',
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _recargarUsuarios,
            child: const Text('Recargar'),
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blueAccent,
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
              textStyle: TextStyle(fontSize: 16),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildUserCard(
      BuildContext context,
      String correo,
      int id,
      String tipoUsuario,
      String ine,
      String ineAtras,
      String coordenadas,
      String nombre_local,
      String? direccion) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      elevation: 2.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: ListTile(
        title: Text(
          'Nombre negocio: $nombre_local',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          textAlign: TextAlign.center,
        ),
        subtitle: Text(
          direccion != null
              ? 'Dirección: $direccion'
              : 'Sin dirección disponible',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14, color: Colors.grey[600]),
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => UserDetailScreen(
                correo: correo,
                id: id,
                tipoUsuario: tipoUsuario,
                ine: ine,
                ineAtras: ineAtras,
                coordenadas: coordenadas,
                nombreNegocio: nombre_local,
                direccion: direccion,
              ),
            ),
          );
        },
      ),
    );
  }
}

class UserDetailScreen extends StatefulWidget {
  final String correo;
  final int id;
  final String tipoUsuario;
  final String ine;
  final String ineAtras;
  final String coordenadas;
  final String nombreNegocio;
  final String? direccion;

  UserDetailScreen({
    required this.correo,
    required this.id,
    required this.tipoUsuario,
    required this.ine,
    required this.ineAtras,
    required this.coordenadas,
    required this.nombreNegocio,
    this.direccion,
  });

  @override
  _UserDetailScreenState createState() => _UserDetailScreenState();
}

class _UserDetailScreenState extends State<UserDetailScreen> {
  Uint8List? ineImage;
  Uint8List? ineAtrasImage;
  bool _isLoading = true;
  LatLng? _latLngCoordinates;

  @override
  void initState() {
    super.initState();
    _loadImages();
    _convertCoordinates();
  }

  Future<void> _convertCoordinates() async {
    final parts = widget.coordenadas.split(',');
    final lat = double.parse(parts[0].trim());
    final lng = double.parse(parts[1].trim());
    if (mounted) {
      setState(() {
        _latLngCoordinates = LatLng(lat, lng);
      });
    }
  }

  Future<void> _loadImages() async {
    final platilloService =
        Provider.of<PlatilloService>(context, listen: false);

    try {
      final ine = await platilloService.obtenerImagenO(widget.ine);
      final ineAtras = await platilloService.obtenerImagenO(widget.ineAtras);

      if (mounted) {
        setState(() {
          ineImage = ine;
          ineAtrasImage = ineAtras;
          _isLoading = false;
        });
      }
    } catch (e) {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  Future<void> _cambiarEstadoNegocio(String estado) async {
    final socketService = Provider.of<SocketSerDos>(context, listen: false);
    socketService.emit('cambiarEstadoNegocio', {
      'id': widget.id,
      'estado': estado,
    });

    socketService.socketS().on('estadoNegocioCambiado', (data) {
      if (mounted) {
        Navigator.pop(context, data);
      }
    });
  }

  void _showExpandedImage(Uint8List imageBytes, String title) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Stack(
          children: [
            InteractiveViewer(
              boundaryMargin: EdgeInsets.all(0),
              minScale: 1.0,
              maxScale: 4.0,
              child: Container(
                width: double.infinity,
                height: 400,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                    image: MemoryImage(imageBytes),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.black54,
                  child: Icon(Icons.close, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalle del Negocio'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Negocio: ${widget.nombreNegocio}',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.blueAccent,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Correo: ${widget.correo}',
                      style: TextStyle(fontSize: 16, color: Colors.grey[600]),
                    ),
                    if (widget.direccion != null) ...[
                      SizedBox(height: 8),
                      Text(
                        'Dirección: ${widget.direccion}',
                        style: TextStyle(fontSize: 16, color: Colors.grey[600]),
                      ),
                    ],
                    SizedBox(height: 16),
                    SizedBox(
                      height: 200,
                      child: _latLngCoordinates != null
                          ? _buildGoogleMap()
                          : Center(child: CircularProgressIndicator()),
                    ),
                    SizedBox(height: 16),
                    _buildImageSection(),
                    SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () => _cambiarEstadoNegocio('aceptado'),
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(255, 129, 180, 131),
                            padding: EdgeInsets.symmetric(
                                horizontal: 32, vertical: 12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: Text(
                            'Aceptar Negocio',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(width: 16),
                        ElevatedButton(
                          onPressed: () => _cambiarEstadoNegocio('bloqueado'),
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(255, 222, 102, 93),
                            padding: EdgeInsets.symmetric(
                                horizontal: 32, vertical: 12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: Text(
                            'Rechazar Negocio',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Widget _buildGoogleMap() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: _latLngCoordinates!,
          zoom: 14,
        ),
        markers: {
          Marker(
            markerId: MarkerId('destination'),
            position: _latLngCoordinates!,
          ),
        },
      ),
    );
  }

  Widget _buildImageSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: _buildSingleImageSection('INE Frente', ineImage),
        ),
        SizedBox(width: 16),
        Expanded(
          child: _buildSingleImageSection('INE Atrás', ineAtrasImage),
        ),
      ],
    );
  }

  Widget _buildSingleImageSection(String title, Uint8List? imageBytes) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$title:',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        SizedBox(height: 8),
        ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: imageBytes != null
              ? GestureDetector(
                  onTap: () => _showExpandedImage(imageBytes, title),
                  child: Image.memory(
                    imageBytes,
                    width: double.infinity,
                    height: 200,
                    fit: BoxFit.cover,
                  ),
                )
              : Container(
                  width: double.infinity,
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey[200],
                  ),
                  child: Center(
                    child: Text(
                      'Imagen no disponible',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey[600],
                      ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }
}
