import 'dart:typed_data';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dash_delivery/services/socket.dart';

class AdminScreen extends StatelessWidget {
  const AdminScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Verificación de repartidores'),
      ),
      // drawer: sidenavAdmin(context),
      body: UserList(),
    );
  }
}

class UserList extends StatefulWidget {
  const UserList({super.key});

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  List<dynamic> users = [];
  bool _isLoading = true;
  late SocketSerDos socketService;

  @override
  void initState() {
    super.initState();
    socketService = Provider.of<SocketSerDos>(context, listen: false);
    _fetchUsers();

    socketService
        .socketS()
        .on('estadoRepartidorCambiado', _handleEstadoRepartidorCambiado);
  }

  void _handleEstadoRepartidorCambiado(data) {
    if (mounted) {
      setState(() {
        users.removeWhere((user) => user['id'] == data['id']);
      });
    }
  }

  Future<void> _fetchUsers() async {
    setState(() {
      _isLoading = true;
    });

    socketService.emit('traerUsuariosPendientesRepartidor', "repartidor");
    socketService.socketS().on('usuariosPendientesRepartidorResponse', (data) {
      if (mounted) {
        setState(() {
          users = data;
          _isLoading = false;
        });
      }
    });
  }

  Future<void> _recargarUsuarios() async {
    setState(() {
      _isLoading = true;
    });
    await _fetchUsers();
  }

  @override
  void dispose() {
    socketService
        .socketS()
        .off('estadoRepartidorCambiado', _handleEstadoRepartidorCambiado);
    socketService.socketS().off('usuariosPendientesRepartidorResponse');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _recargarUsuarios,
      child: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : users.isEmpty
              ? _buildNoUsersMessage()
              : ListView.builder(
                  itemCount: users.length,
                  itemBuilder: (context, index) {
                    final user = users[index];
                    print(user);
                    return _buildUserCard(
                        context,
                        user['correo'],
                        user['id'],
                        user['tipo_usuario'],
                        user['ine'],
                        user['ine_atras'],
                        user['licencia'],
                        user['mochila'],
                        user['marca_modelo_moto'],
                        user['nombre_contacto'],
                        user['parentesco'],
                        user['telefono_contacto'],
                        user['nombre_repartidor'],
                        user['repartidor_photo']);
                  },
                ),
    );
  }

  Widget _buildNoUsersMessage() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'No se han encontrado usuarios actualmente.',
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _recargarUsuarios,
            child: const Text('Recargar'),
          ),
        ],
      ),
    );
  }

  Widget _buildUserCard(
      BuildContext context,
      String correo,
      int id,
      String tipoUsuario,
      String ine,
      String ineAtras,
      String licencia,
      String mochila,
      String marcaModeloMoto,
      String nombreContacto,
      String parentesco,
      String telefonoContacto,
      String nombreRepartidor,
      String fotoRepartidor) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      elevation: 2.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: ListTile(
        title: Text(
          'Nombre: $nombreRepartidor',
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
            color: Colors.blueAccent,
          ),
          textAlign: TextAlign.center,
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 8),
            Text(
              'Teléfono: $telefonoContacto',
              style: TextStyle(fontSize: 14, color: Colors.grey[600]),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 8),
            Text(
              'Correo: $correo',
              style: TextStyle(fontSize: 14, color: Colors.grey[600]),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => UserDetailScreen(
                  correo: correo,
                  id: id,
                  tipoUsuario: tipoUsuario,
                  ine: ine,
                  ineAtras: ineAtras,
                  licencia: licencia,
                  mochila: mochila,
                  marcaModeloMoto: marcaModeloMoto,
                  nombreContacto: nombreContacto,
                  parentesco: parentesco,
                  telefonoContacto: telefonoContacto,
                  nombreRepartidor: nombreRepartidor,
                  fotoRepartidor: fotoRepartidor),
            ),
          );
        },
      ),
    );
  }
}

class UserDetailScreen extends StatefulWidget {
  final String correo;
  final int id;
  final String tipoUsuario;
  final String ine;
  final String ineAtras;
  final String licencia;
  final String mochila;
  final String marcaModeloMoto;
  final String nombreContacto;
  final String parentesco;
  final String telefonoContacto;
  final String nombreRepartidor;
  final String fotoRepartidor;

  UserDetailScreen({
    required this.correo,
    required this.id,
    required this.tipoUsuario,
    required this.ine,
    required this.ineAtras,
    required this.licencia,
    required this.mochila,
    required this.marcaModeloMoto,
    required this.nombreContacto,
    required this.parentesco,
    required this.telefonoContacto,
    required this.nombreRepartidor,
    required this.fotoRepartidor,
  });

  @override
  _UserDetailScreenState createState() => _UserDetailScreenState();
}

class _UserDetailScreenState extends State<UserDetailScreen> {
  Uint8List? ineImage;
  Uint8List? ineAtrasImage;
  Uint8List? licenciaImage;
  Uint8List? mochilaImage;
  Uint8List? repartidorImage;
  bool _isLoading = true;
  late SocketSerDos socketService;

  @override
  void initState() {
    super.initState();
    socketService = Provider.of<SocketSerDos>(context, listen: false);
    _loadImages();
  }

  Future<void> _loadImages() async {
    final platilloService =
        Provider.of<PlatilloService>(context, listen: false);

    try {
      final ine = await platilloService.obtenerImagenO(widget.ine);
      final ineAtras = await platilloService.obtenerImagenO(widget.ineAtras);
      final licencia = await platilloService.obtenerImagenO(widget.licencia);
      final mochila = await platilloService.obtenerImagenO(widget.mochila);
      final fotoRepartidor =
          await platilloService.obtenerImagenO(widget.fotoRepartidor);

      if (mounted) {
        setState(() {
          ineImage = ine;
          ineAtrasImage = ineAtras;
          licenciaImage = licencia;
          mochilaImage = mochila;
          repartidorImage = fotoRepartidor;
          _isLoading = false;
        });
      }
    } catch (e) {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  Future<void> _cambiarEstadoRepartidor(String estado) async {
    socketService.emit('cambiarEstadoRepartidor', {
      'id': widget.id,
      'estado': estado,
    });

    socketService.socketS().on('estadoRepartidorCambiado', (data) {
      if (mounted) {
        Navigator.pop(context, widget.id);
      }
    });
  }

  void _showExpandedImage(Uint8List imageBytes, String title) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Stack(
          children: [
            InteractiveViewer(
              boundaryMargin: EdgeInsets.all(0),
              minScale: 1.0,
              maxScale: 4.0,
              child: Container(
                width: double.infinity,
                height: 400,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                    image: MemoryImage(imageBytes),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.black54,
                  child: Icon(Icons.close, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalle del Repartidor'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: _isLoading
            ? const Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Repartidor: ${widget.nombreRepartidor}',
                      style: const TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.blueAccent,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 8),
                    Text(
                      'Correo: ${widget.correo}',
                      style: TextStyle(fontSize: 16, color: Colors.grey[600]),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      'Teléfono: ${widget.telefonoContacto}',
                      style: TextStyle(fontSize: 16, color: Colors.grey[600]),
                    ),
                    const SizedBox(height: 16),
                    _buildImageSection(),
                    const SizedBox(height: 16),
                    Text(
                      'Marca y Modelo de Moto: ${widget.marcaModeloMoto}',
                      style: const TextStyle(fontSize: 18),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      'Nombre de Contacto: ${widget.nombreContacto}',
                      style: const TextStyle(fontSize: 18),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      'Parentesco: ${widget.parentesco}',
                      style: const TextStyle(fontSize: 18),
                    ),
                    const SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () => _cambiarEstadoRepartidor('aceptado'),
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(255, 129, 180, 131),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 32, vertical: 12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: const Text(
                            'Aceptar Repartidor',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        const SizedBox(width: 16),
                        ElevatedButton(
                          onPressed: () =>
                              _cambiarEstadoRepartidor('bloqueado'),
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(255, 222, 102, 93),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 32, vertical: 12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: const Text(
                            'Rechazar Repartidor',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Widget _buildImageSection() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: _buildSingleImageSection('INE Frente', ineImage),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: _buildSingleImageSection('INE Atrás', ineAtrasImage),
            ),
          ],
        ),
        const SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: _buildSingleImageSection('Licencia', licenciaImage),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: _buildSingleImageSection('Mochila', mochilaImage),
            ),
          ],
        ),
        const SizedBox(height: 16),
        _buildSingleImageSection('Foto Repartidor', repartidorImage),
      ],
    );
  }

  Widget _buildSingleImageSection(String title, Uint8List? imageBytes) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$title:',
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        const SizedBox(height: 8),
        ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: imageBytes != null
              ? GestureDetector(
                  onTap: () => _showExpandedImage(imageBytes, title),
                  child: Image.memory(
                    imageBytes,
                    width: double.infinity,
                    height: 200,
                    fit: BoxFit.cover,
                  ),
                )
              : Container(
                  width: double.infinity,
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey[200],
                  ),
                  child: Center(
                    child: Text(
                      'Imagen no disponible',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey[600],
                      ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }
}
