import 'dart:typed_data';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/widgets/flush_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dash_delivery/services/socket.dart';

class AdminScreenUsuarios extends StatelessWidget {
  const AdminScreenUsuarios({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Usuarios'),
      ),
      body: UserList(),
    );
  }
}

class UserList extends StatefulWidget {
  const UserList({super.key});

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  List<dynamic> users = [];
  List<dynamic> filteredUsers = [];
  bool _isLoading = true;
  late SocketSerDos socketService;
  TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    socketService = Provider.of<SocketSerDos>(context, listen: false);
    _fetchUsers();

    socketService
        .socketS()
        .on('estadoRepartidorCambiado', _handleEstadoRepartidorCambiado);

    _searchController.addListener(_filterUsers);
  }

  void _handleEstadoRepartidorCambiado(data) {
    if (mounted) {
      _fetchUsers();
    }
  }

  Future<void> _fetchUsers() async {
    setState(() {
      _isLoading = true;
    });

    socketService.emit('traerUsuarios');
    socketService.socketS().on('usuariosResponse', (data) {
      if (mounted) {
        setState(() {
          users = data;
          filteredUsers = users;
          _isLoading = false;
        });
      }
    });
  }

  void _filterUsers() {
    String query = _searchController.text.toLowerCase();
    setState(() {
      filteredUsers = users.where((user) {
        final correo = user['correo']?.toLowerCase() ?? '';
        final nombreRepartidor = user['nombre_repartidor']?.toLowerCase() ?? '';
        final nombreLocal = user['nombre_local']?.toLowerCase() ?? '';
        return correo.contains(query) ||
            nombreRepartidor.contains(query) ||
            nombreLocal.contains(query);
      }).toList();
    });
  }

  Future<void> _recargarUsuarios() async {
    setState(() {
      _isLoading = true;
    });
    await _fetchUsers();
  }

  @override
  void dispose() {
    socketService
        .socketS()
        .off('estadoRepartidorCambiado', _handleEstadoRepartidorCambiado);
    socketService.socketS().off('usuariosPendientesRepartidorResponse');
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _recargarUsuarios,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: _searchController,
              decoration: InputDecoration(
                hintText: 'Buscar por correo, nombre o local',
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
          ),
          Expanded(
            child: _isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : filteredUsers.isEmpty
                    ? _buildNoUsersMessage()
                    : ListView.builder(
                        itemCount: filteredUsers.length,
                        itemBuilder: (context, index) {
                          final user = filteredUsers[index];
                          return _buildUserCard(
                              context,
                              user['correo'],
                              user['id'],
                              user['tipo_usuario'] ?? "Sin registro aun",
                              user['ine'] ?? "Sin registrar",
                              user['ine_atras'] ?? "Sin registrar",
                              user['licencia'] ?? "",
                              user['mochila'] ?? "",
                              user['marca_modelo_moto'] ?? "",
                              user['nombre_contacto'] ?? "",
                              user['parentesco'] ?? "",
                              user['telefono_contacto'] ?? "",
                              user['nombre_repartidor'] ?? "",
                              user['repartidor_photo'] ?? "",
                              user['nombre_local'] ?? "",
                              user['estatus'] ?? "");
                        },
                      ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoUsersMessage() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'No se han encontrado usuarios actualmente.',
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _recargarUsuarios,
            child: const Text('Recargar'),
          ),
        ],
      ),
    );
  }

  Widget _buildUserCard(
    BuildContext context,
    String correo,
    int id,
    String tipoUsuario,
    String ine,
    String ineAtras,
    String licencia,
    String mochila,
    String marcaModeloMoto,
    String nombreContacto,
    String parentesco,
    String telefonoContacto,
    String nombreRepartidor,
    String fotoRepartidor,
    String nombreLocal,
    String estatus,
  ) {
    // Determinar el ícono y color del card en función del tipo de usuario
    IconData? leadingIcon;
    Color cardColor = Colors.white; // Default color

    switch (tipoUsuario) {
      case 'repartidor':
        leadingIcon = Icons.delivery_dining;
        break;
      case 'restaurante':
        leadingIcon = Icons.storefront;
        break;
      case 'admin':
        leadingIcon = Icons.admin_panel_settings;
        break;
      default:
        cardColor = Colors.grey[300]!;
        break;
    }

    return Card(
      color: cardColor,
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      elevation: 2.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Column(
        children: [
          ListTile(
            leading: leadingIcon != null
                ? Icon(
                    leadingIcon,
                    color: Colors.blueAccent,
                    size: 50,
                  )
                : null,
            title: Text(
              tipoUsuario == 'repartidor'
                  ? 'Nombre del repartidor: $nombreRepartidor'
                  : tipoUsuario == 'restaurante'
                      ? 'Nombre local: $nombreLocal'
                      : tipoUsuario == 'admin'
                          ? 'Usuario Administrativo'
                          : 'Pendiente de registrar',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.blueAccent,
              ),
              textAlign: TextAlign.center,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 8),
                Text(
                  'Estatus: $estatus',
                  style: TextStyle(fontSize: 14, color: Colors.grey[600]),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 8),
                telefonoContacto != ''
                    ? Text(
                        'Teléfono: $telefonoContacto',
                        style: TextStyle(fontSize: 14, color: Colors.grey[600]),
                        textAlign: TextAlign.center,
                      )
                    : SizedBox(),
                const SizedBox(height: 8),
                estatus == 'pendiente'
                    ? SizedBox()
                    : Text(
                        'Correo: $correo',
                        style: TextStyle(fontSize: 14, color: Colors.grey[600]),
                        textAlign: TextAlign.center,
                      ),
              ],
            ),
            onTap: () {
              // Navegación a la pantalla de detalles del usuario
            },
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, right: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                _buildCircularButton(
                  icon: Icons.lock_open,
                  color: estatus == 'bloqueado' ? Colors.green : Colors.grey,
                  onPressed: estatus == 'bloqueado'
                      ? () {
                          _cambiarEstadoUsuario(context, id, 'desbloquear');
                        }
                      : null,
                ),
                _buildCircularButton(
                  icon: Icons.lock_outline,
                  color: estatus == 'aceptado' ? Colors.red : Colors.grey,
                  onPressed: estatus == 'aceptado'
                      ? () {
                          _cambiarEstadoUsuario(context, id, 'bloquear');
                        }
                      : null,
                ),
                _buildCircularButton(
                  icon: Icons.monetization_on_outlined,
                  color: estatus == 'altaCuota' ? Colors.orange : Colors.grey,
                  onPressed: estatus == 'altaCuota'
                      ? () {
                          _cambiarEstadoUsuario(context, id, 'liberar');
                        }
                      : null,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCircularButton({
    required IconData icon,
    required Color color,
    required VoidCallback? onPressed,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: Material(
        color: Colors.transparent,
        child: Ink(
          decoration: ShapeDecoration(
            color: color,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: Icon(icon),
            color: Colors.white,
            onPressed: onPressed,
          ),
        ),
      ),
    );
  }

  void _cambiarEstadoUsuario(BuildContext context, int id, String accion) {
    switch (accion) {
      case 'desbloquear':
        socketService.emit('desbloquearUser', {'idUsuario': id});
        socketService.socketS().on('desbloquearUserResponse', (data) {
          if (data['status'] == 'success') {
            showErrorFlushbar('Usuario desbloqueado con éxito', false, context);
            _recargarUsuarios();
          }
        });
        break;
      case 'bloquear':
        socketService.emit('bloquearUser', {'idUsuario': id});
        socketService.socketS().on('bloquearUserResponse', (data) {
          if (data['status'] == 'success') {
            showErrorFlushbar(
                'Se ha bloqueado el usuario correctamente.', false, context);
            _recargarUsuarios();
          }
        });
        break;
      case 'liberar':
        socketService.emit('liberarUser', {'idUsuario': id});
        socketService.socketS().on('liberarUserResponse', (data) {
          if (data['status'] == 'success') {
            showErrorFlushbar('Usuario desbloqueado con éxito', false, context);
            _recargarUsuarios();
          }
        });
        break;
    }
  }
}
