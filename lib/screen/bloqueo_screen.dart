import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/widgets/sidenav.dart';
import 'package:dash_delivery/widgets/sidenavB.dart';
import 'package:flutter/material.dart';

class BloqueoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Usuario bloqueado'),
      ),
      drawer: sidenavB(context),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/pickers.png',
              height: 100,
            ),
            SizedBox(height: 20), // Espacio entre la imagen y el texto
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'Se ha bloqueado su usuario, favor de contactar con el administrador.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.red,
                ),
              ),
            ),
            SizedBox(height: 20), // Espacio entre el texto y el botón
            ElevatedButton(
              onPressed: () {
                // Aquí puedes agregar la lógica de verificación
                appRouters
                    .pushReplacementNamed(PreferencesUser().rutaPrincipal);
              },
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                backgroundColor: Colors.yellow, // Color del texto del botón
                minimumSize: Size(150, 50), // Tamaño mínimo del botón
              ),
              child: Text('Verificar'),
            ),
          ],
        ),
      ),
    );
  }
}
