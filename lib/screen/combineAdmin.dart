import 'package:dash_delivery/screen/adminScreenPedidos.dart';
import 'package:dash_delivery/screen/administracion_Negocio_screen.dart';
import 'package:dash_delivery/screen/administracion_screen.dart';
import 'package:dash_delivery/screen/administracion_screen_usuarios.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';

class CombinedScreenAdmin extends StatefulWidget {
  const CombinedScreenAdmin({super.key});

  @override
  State<CombinedScreenAdmin> createState() => _CombinedScreenState();
}

class _CombinedScreenState extends State<CombinedScreenAdmin> {
  int _selectedIndex =
      0; // Para rastrear el botón seleccionado, empieza con "Negocios" activo
  bool _isExpanded = false;

  void _toggleExpand() {
    setState(() {
      _isExpanded = !_isExpanded;
    });
  }

  void _onButtonTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    switch (_selectedIndex) {
      case 0:
        AdminScreenNegocio();
        break;
      case 1:
        AdminScreen();
        break;
      case 2:
        const AdminScreenPedidos();
        break;
      case 3:
        // Acción al presionar el botón de "Usuarios"
        // Implementa la navegación a la pantalla de "Usuarios" aquí
        break;
      case 4:
        _showLogoutConfirmationDialog(context);
        break;
    }
  }

  void _showLogoutConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          contentPadding: const EdgeInsets.all(16.0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                'assets/dash_delivery.png',
                height: 50,
              ),
              const SizedBox(height: 20),
              const Text(
                'Confirmar Cierre de Sesión',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              const Text(
                '¿Estás seguro de que quieres cerrar sesión?',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          actions: <Widget>[
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              icon: const Icon(
                Icons.cancel,
                color: Colors.white,
              ),
              label: const Text(
                'Cancelar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green[400],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              icon: const Icon(
                Icons.check_circle,
                color: Colors.white,
              ),
              label: const Text(
                'Aceptar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                Navigator.of(context).pop();
                await FirebaseAuth.instance.signOut();
                await AuthGoogleU().logoutGoogle();
                final prefs = PreferencesUser();
                prefs.coordenadas = "";
                prefs.rutaPrincipal = '/login';
                appRouters.pushReplacementNamed('/login');
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _selectedIndex == 0
              ? AdminScreenNegocio()
              : _selectedIndex == 1
                  ? const AdminScreen() //repartidores
                  : _selectedIndex == 2
                      ? const AdminScreenPedidos() //repartidores
                      : const AdminScreenUsuarios(),
          Positioned(
            top: 50,
            right: 20,
            child: GestureDetector(
              onTap: _toggleExpand,
              child: CircleAvatar(
                radius: 25,
                backgroundColor: _isExpanded ? Colors.red : Colors.amber,
                child: Icon(
                  _isExpanded ? Icons.close : Icons.menu,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          if (_isExpanded)
            Positioned(
              top: 110,
              right: 20,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  _buildButtonWithLabel(
                    label: "Negocios",
                    icon: Icons.business,
                    color: _selectedIndex == 0
                        ? Colors.amber.shade700
                        : Colors.amber,
                    onPressed: () {
                      _onButtonTapped(0);
                    },
                  ),
                  const SizedBox(height: 10),
                  _buildButtonWithLabel(
                    label: "Repartidores",
                    icon: Icons.motorcycle,
                    color: _selectedIndex == 1
                        ? Colors.amber.shade700
                        : Colors.amber,
                    onPressed: () {
                      _onButtonTapped(1);
                    },
                  ),
                  const SizedBox(height: 10),
                  _buildButtonWithLabel(
                    label: "Pedidos",
                    icon: Icons.receipt,
                    color: _selectedIndex == 2
                        ? Colors.amber.shade700
                        : Colors.amber,
                    onPressed: () {
                      _onButtonTapped(2);
                    },
                  ),
                  const SizedBox(height: 10),
                  _buildButtonWithLabel(
                    label: "Usuarios", // Nuevo botón "Usuarios"
                    icon: Icons.people,
                    color: _selectedIndex == 3
                        ? Colors.amber.shade700
                        : Colors.amber,
                    onPressed: () {
                      _onButtonTapped(3);
                    },
                  ),
                  const SizedBox(height: 10),
                  _buildButtonWithLabel(
                    label: "Cerrar sesión",
                    icon: Icons.logout,
                    color: Colors.red,
                    onPressed: () {
                      _showLogoutConfirmationDialog(context);
                    },
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildButtonWithLabel({
    required String label,
    required IconData icon,
    required Color color,
    required VoidCallback onPressed,
  }) {
    return Container(
      width: 200, // Establece un ancho fijo para los botones
      decoration: BoxDecoration(
        color: color, // Color de fondo
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: TextButton.icon(
        onPressed: onPressed,
        icon: Icon(
          icon,
          color: Colors.white,
        ),
        label: Text(
          label,
          style: const TextStyle(color: Colors.white),
        ),
        style: TextButton.styleFrom(
          backgroundColor: Colors.transparent, // Sin fondo adicional
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
