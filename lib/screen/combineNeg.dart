import 'package:dash_delivery/screen/dashboard_negocio_screen.dart';
import 'package:dash_delivery/screen/dashboard_repartidor_screen.dart';
import 'package:dash_delivery/screen/estatus_pedido.dart';
import 'package:dash_delivery/screen/mis_pedidos.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/screen/detalle_pedido_screen_rep.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/services/socket.dart';

class CombinedScreenNeg extends StatefulWidget {
  const CombinedScreenNeg({super.key});

  @override
  State<CombinedScreenNeg> createState() => _CombinedScreenState();
}

class _CombinedScreenState extends State<CombinedScreenNeg> {
  int _selectedIndex = 0; // Para controlar la selección del BottomNavigationBar

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onItemTapped(int index) {
    setState(() {
      if (index != 2) {
        _selectedIndex = index;
      }

      if (index == 1) {
        // _fetchPedidos(); // Cargar los pedidos al seleccionar "Ver mis pedidos"
      } else if (index == 2) {
        // FirebaseAuth.instance.signOut();
        // AuthGoogleU().logoutGoogle();
        // PreferencesUser().rutaPrincipal = '/login';
        // appRouters.pushReplacementNamed('/login');
        _showLogoutConfirmationDialog(context);
      }
    });
  }

  void _showLogoutConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible:
          false, // Evita que el diálogo se cierre al hacer clic fuera de él
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          contentPadding:
              EdgeInsets.all(16.0), // Agrega padding alrededor del contenido
          content: Column(
            mainAxisSize:
                MainAxisSize.min, // Ajusta el tamaño de la columna al contenido
            children: [
              Image.asset(
                'assets/pickers.png',
                height: 50,
              ),
              SizedBox(height: 20),
              Text(
                'Confirmar Cierre de Sesión',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Text(
                '¿Estás seguro de que quieres cerrar sesión?',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          actions: <Widget>[
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              icon: Icon(
                Icons.cancel,
                color: Colors.white,
              ),
              label: Text(
                'Cancelar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop(); // Cierra el diálogo
              },
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green[400],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              icon: Icon(
                Icons.check_circle,
                color: Colors.white,
              ),
              label: Text(
                'Aceptar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                Navigator.of(context).pop(); // Cierra el diálogo
                await FirebaseAuth.instance.signOut();
                await AuthGoogleU().logoutGoogle();
                final prefs = PreferencesUser();
                prefs.coordenadas = "";
                prefs.rutaPrincipal = '/login';
                appRouters.pushReplacementNamed('/login');
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _selectedIndex == 0 ? DashboardNegocio() : EstatusPedido(),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 0 ? Colors.amber : Colors.transparent,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.receipt_long_outlined,
                color: _selectedIndex == 0 ? Colors.white : Colors.grey,
              ),
            ),
            label: 'Realizar pedido',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 1 ? Colors.amber : Colors.transparent,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.find_in_page_sharp,
                color: _selectedIndex == 1 ? Colors.white : Colors.grey,
              ),
            ),
            label: 'Ver estatus',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 2 ? Colors.amber : Colors.red,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.logout,
                color: _selectedIndex == 2 ? Colors.red : Colors.white,
              ),
            ),
            label: 'Cerrar sesión',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
