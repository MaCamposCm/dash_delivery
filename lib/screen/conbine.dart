import 'package:dash_delivery/screen/dashboard_repartidor_screen.dart';
import 'package:dash_delivery/screen/mis_pedidos.dart';
import 'package:dash_delivery/services/verificacionService.dart';
import 'package:dash_delivery/widgets/CustomUpdateBanner.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/screen/detalle_pedido_screen_rep.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/services/socket.dart';

class CombinedScreen extends StatefulWidget {
  const CombinedScreen({super.key});

  @override
  State<CombinedScreen> createState() => _CombinedScreenState();
}

class _CombinedScreenState extends State<CombinedScreen> {
  final verificacionService = VerificacionService();

  int _selectedIndex = 0; // Para controlar la selección del BottomNavigationBar
  bool _showUpdateBanner = false; // Para controlar la visibilidad del banner
  @override
  void initState() {
    super.initState();
    checkForUpdatesAndShowBottomSheet();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onItemTapped(int index) {
    setState(() {
      if (index != 2) {
        _selectedIndex = index;
      }

      if (index == 1) {
        // _fetchPedidos(); // Cargar los pedidos al seleccionar "Ver mis pedidos"
      } else if (index == 2) {
        // FirebaseAuth.instance.signOut();
        // AuthGoogleU().logoutGoogle();
        // PreferencesUser().rutaPrincipal = '/login';
        // appRouters.pushReplacementNamed('/login');
        _showLogoutConfirmationDialog(context);
      }
    });
  }

  void hideUpdateBanner() {
    setState(() {
      _showUpdateBanner = false;
    });
  }

  Future<void> checkForUpdatesAndShowBottomSheet() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String currentVersion = packageInfo.version;

    final dato = await verificacionService.verificarVersion(context);
    if (currentVersion != dato) {
      setState(() {
        _showUpdateBanner = true; // Mostrar el banner si hay una nueva versión
      });
    }
  }

  void _showLogoutConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible:
          false, // Evita que el diálogo se cierre al hacer clic fuera de él
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          contentPadding:
              EdgeInsets.all(16.0), // Agrega padding alrededor del contenido
          content: Column(
            mainAxisSize:
                MainAxisSize.min, // Ajusta el tamaño de la columna al contenido
            children: [
              Image.asset(
                'assets/pickers.png',
                height: 50,
              ),
              SizedBox(height: 20),
              Text(
                'Confirmar Cierre de Sesión',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Text(
                '¿Estás seguro de que quieres cerrar sesión?',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          actions: <Widget>[
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              icon: Icon(
                Icons.cancel,
                color: Colors.white,
              ),
              label: Text(
                'Cancelar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop(); // Cierra el diálogo
              },
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green[400],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              icon: Icon(
                Icons.check_circle,
                color: Colors.white,
              ),
              label: Text(
                'Aceptar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                Navigator.of(context).pop(); // Cierra el diálogo
                await FirebaseAuth.instance.signOut();
                await AuthGoogleU().logoutGoogle();
                final prefs = PreferencesUser();
                prefs.coordenadas = "";
                prefs.rutaPrincipal = '/login';
                appRouters.pushReplacementNamed('/login');
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard Repartidor'),
      ),
      body: Stack(
        children: [
          _selectedIndex == 0 ? DashboardRepartidor() : MisPedidos(),
          if (_showUpdateBanner)
            CustomUpdateBanner(
              message: 'Se encuentra una nueva versión de esta aplicación',
              buttonText: '¡Obténla!',
              updateUrl:
                  'https://play.google.com/store/apps/details?id=com.nabiaa.dashdelivery',
              onClose: hideUpdateBanner,
            ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 0 ? Colors.amber : Colors.transparent,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.receipt,
                color: _selectedIndex == 0 ? Colors.white : Colors.grey,
              ),
            ),
            label: 'Pedidos pendientes',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: _selectedIndex == 1 ? Colors.amber : Colors.transparent,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.remove_red_eye,
                color: _selectedIndex == 1 ? Colors.white : Colors.grey,
              ),
            ),
            label: 'Ver mis pedidos',
          ),
          BottomNavigationBarItem(
            icon: Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.logout,
                color: Colors.white,
              ),
            ),
            label: 'Cerrar sesión',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
