import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dash_delivery/screen/maps/map_screen.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/widgets/sidenavB.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:another_flushbar/flushbar.dart';
import 'package:dash_delivery/bloc/location/location_bloc.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/register_service.dart';
import 'package:dash_delivery/widgets/flush_bar.dart';
import 'package:provider/provider.dart';

class DashboardEleccion extends StatefulWidget {
  const DashboardEleccion({super.key});

  @override
  State<DashboardEleccion> createState() => _DashboardEleccionState();
}

class _DashboardEleccionState extends State<DashboardEleccion> {
  String selectedRole = 'restaurante';
  File? frontINE;
  File? backINE;
  File? license;
  File? backpack;
  File? repartidorPhoto;

  bool _isLoading = false;
  String? selectedAddress;
  LatLng? selectedLatLng;

  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _repartidorNameController =
      TextEditingController();
  final TextEditingController _vehicleController = TextEditingController();
  final TextEditingController _contactNameController = TextEditingController();
  final TextEditingController _contactPhoneController = TextEditingController();
  final TextEditingController _contactRelationshipController =
      TextEditingController();
  final TextEditingController _localNameController = TextEditingController();
  String? selectedCategory;
  final List<String> categories = ["comida rápida", "farmacia", "abarrotes"];
  final prefs = PreferencesUser();

  late LocationBloc locationBloc;

  void _setSelectedAddress(String address, LatLng coordinates) {
    setState(() {
      selectedAddress = address;
      selectedLatLng = coordinates;
    });
  }

  void _handleRadioValueChange(String? value) {
    setState(() {
      selectedRole = value!;
    });
  }

  void _onImagePicked(bool isFront,
      {bool fromCamera = false, String type = 'INE'}) {
    _pickImage(isFront, fromCamera: fromCamera, type: type).then((_) {
      // Any additional actions after image picking
    }).catchError((error) {});
  }

  Future<void> _pickImage(bool isFront,
      {bool fromCamera = false, String type = 'INE'}) async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(
      source: fromCamera ? ImageSource.camera : ImageSource.gallery,
    );

    if (pickedFile != null) {
      setState(() {
        switch (type) {
          case 'INE':
            if (isFront) {
              frontINE = File(pickedFile.path);
            } else {
              backINE = File(pickedFile.path);
            }
            break;
          case 'License':
            license = File(pickedFile.path);
            break;
          case 'Backpack':
            backpack = File(pickedFile.path);
            break;
          case 'RepartidorPhoto':
            repartidorPhoto = File(pickedFile.path);
            break;
        }
      });
    }
  }

  Future<void> _updateAddressFromLatLng(LatLng location) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(location.latitude, location.longitude);
      if (placemarks.isNotEmpty) {
        final place = placemarks.first;
        setState(() {
          selectedLatLng = location;
          selectedAddress =
              '${place.street}, ${place.locality}, ${place.postalCode}, ${place.country}';
        });
      }
    } catch (e) {
      showErrorFlushbar('Error al obtener la dirección: $e', true, context);
    }
  }

  Future<void> _registerRepartidor() async {
    if (_vehicleController.text.isNotEmpty &&
        frontINE != null &&
        backINE != null &&
        license != null &&
        backpack != null &&
        repartidorPhoto !=
            null && // Verificar que se haya seleccionado la foto del repartidor
        _contactNameController.text.isNotEmpty &&
        _contactPhoneController.text.isNotEmpty &&
        _contactRelationshipController.text.isNotEmpty &&
        _repartidorNameController.text.isNotEmpty) {
      // Verificar que se haya ingresado el nombre del repartidor

      final loginService = LoginService();

      setState(() {
        _isLoading = true;
      });

      try {
        final userAut = AuthGoogleU().dataUser();

        if (PreferencesUser().token.isEmpty) {
          PreferencesUser().token = userAut!.uid;
        }

        final result = await loginService.registroRepartidor(
          marcaModeloVehiculo: _vehicleController.text,
          ineFrente: frontINE!,
          ineAtras: backINE!,
          licencia: license!,
          mochila: backpack!,
          repartidorPhoto: repartidorPhoto!, // Enviar la foto del repartidor
          nombreRepartidor:
              _repartidorNameController.text, // Enviar el nombre del repartidor
          nombreContacto: _contactNameController.text,
          telefonoContacto: _contactPhoneController.text,
          parentesco: _contactRelationshipController.text,
          uid: PreferencesUser().token,
          context: context,
        );
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }

        if (result['estatus'] == "Correcto") {
          showErrorFlushbar("Registro de repartidor exitoso", false, context);

          prefs.rutaPrincipal = '/PendienteScreen';
          appRouters.pushReplacementNamed('/PendienteScreen');
        } else {
          showErrorFlushbar(result['msg'] ?? "Ocurrió un error", true, context);
        }
      } catch (e) {
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
        showErrorFlushbar('Error al registrar: $e', true, context);
      }
    } else {
      showErrorFlushbar(
          'Por favor, complete todos los campos y suba todas las imágenes antes de continuar.',
          true,
          context);
    }
  }

  Future<void> _registroRestaurante() async {
    if (selectedAddress != null &&
        _localNameController.text.isNotEmpty &&
        _phoneController.text.isNotEmpty &&
        selectedCategory != null &&
        frontINE != null &&
        backINE != null) {
      try {
        final loginService = LoginService();

        setState(() {
          _isLoading = true;
        });

        final userAut = AuthGoogleU().dataUser();
        if (PreferencesUser().token.isEmpty) {
          PreferencesUser().token = userAut!.uid;
        }

        final result = await loginService.registroRestaurante(
          nombreRestaurante: _localNameController.text,
          direccion: '${selectedAddress}',
          coordenadas:
              '${selectedLatLng!.latitude}, ${selectedLatLng!.longitude}',
          telefono: '${_phoneController.text}',
          categoria: '$selectedCategory',
          uid: PreferencesUser().token,
          ineFrente: frontINE!,
          ineAtras: backINE!,
          context: context,
        );
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }

        if (result['estatus'] == "Correcto") {
          showErrorFlushbar("Iniciando sesión", false, context);
          prefs.rutaPrincipal = '/PendienteScreen';
          appRouters.pushReplacementNamed('/PendienteScreen');
        } else {
          showErrorFlushbar(result['msg'] ?? "Ocurrió un error", true, context);
        }
      } catch (e) {
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
        showErrorFlushbar('Error al registrar: $e', true, context);
      }
    } else {
      showErrorFlushbar(
          'Por favor, complete todos los campos y suba todas las imágenes antes de continuar.',
          true,
          context);
    }
  }

  @override
  void initState() {
    super.initState();
    locationBloc = BlocProvider.of<LocationBloc>(context);
    locationBloc.startFollowingUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        title: Text('Registro'),
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.red[700]),
        actions: [
          IconButton(
            icon: Container(
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 99, 156, 255),
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: const Row(
                children: [
                  Text(
                    'Solicitar Acceso Administrativo',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                  Icon(
                    Icons.admin_panel_settings_outlined,
                    color: Colors.black,
                    size:
                        18, // Puedes ajustar el tamaño del icono si es necesario
                  ),
                ],
              ),
            ),
            onPressed: () async {
              _showAdminRequestDialog(context);
            },
          ),
          IconButton(
            icon: Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: const EdgeInsets.all(8),
              child: Icon(
                Icons.logout,
                color: Colors.white,
              ),
            ),
            onPressed: () async {
              _showLogoutConfirmationDialog(context);
            },
          ),
        ],
      ),
      body: Stack(
        children: [
          Container(),
          BlocBuilder<LocationBloc, LocationState>(
            builder: (context, state) {
              if (state.lastKnownLocation == null) {
                return const Center(child: Text('Espere por favor...'));
              }
              if (selectedLatLng == null) {
                selectedLatLng = state.lastKnownLocation!;
                _updateAddressFromLatLng(selectedLatLng!);
              }

              return SingleChildScrollView(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: AnimatedContainer(
                            duration: Duration(milliseconds: 300),
                            margin: selectedRole == 'restaurante'
                                ? EdgeInsets.only(right: 10)
                                : EdgeInsets.only(left: 120),
                            decoration: BoxDecoration(
                              color: selectedRole == 'restaurante'
                                  ? Colors.red[700]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  selectedRole = 'restaurante';
                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.store, color: Colors.white),
                                    if (selectedRole == 'restaurante')
                                      SizedBox(width: 8),
                                    if (selectedRole == 'restaurante')
                                      Flexible(
                                        // Añadir un Flexible aquí
                                        child: Text(
                                          'Restaurante/local',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: AnimatedContainer(
                            duration: Duration(milliseconds: 300),
                            margin: selectedRole == 'repartidor'
                                ? EdgeInsets.only(left: 10)
                                : EdgeInsets.only(right: 120),
                            decoration: BoxDecoration(
                              color: selectedRole == 'repartidor'
                                  ? Colors.red[700]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  selectedRole = 'repartidor';
                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.motorcycle, color: Colors.white),
                                    if (selectedRole == 'repartidor')
                                      SizedBox(width: 8),
                                    if (selectedRole == 'repartidor')
                                      Flexible(
                                        // Añadir un Flexible aquí
                                        child: Text(
                                          'Repartidor',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    const SizedBox(height: 20),
                    selectedRole == 'restaurante'
                        ? RestauranteForm(
                            onLocationSelected: _setSelectedAddress,
                            onImagePicked: _onImagePicked,
                            selectedAddress: selectedAddress,
                            selectedLatLng: selectedLatLng,
                            frontINE: frontINE,
                            backINE: backINE,
                            categories: categories,
                            selectedCategory: selectedCategory,
                            onCategoryChanged: (value) {
                              setState(() {
                                selectedCategory = value;
                              });
                            },
                            localNameController: _localNameController,
                            phoneController: _phoneController,
                            onSubmit: _registroRestaurante,
                          )
                        : RepartidorForm(
                            onImagePicked: _onImagePicked,
                            frontINE: frontINE,
                            backINE: backINE,
                            license: license,
                            backpack: backpack,
                            repartidorPhoto: repartidorPhoto,
                            vehicleController: _vehicleController,
                            contactNameController: _contactNameController,
                            contactPhoneController: _contactPhoneController,
                            repartidorNameController: _repartidorNameController,
                            contactRelationshipController:
                                _contactRelationshipController,
                            onSubmit: _registerRepartidor,
                          ),
                  ],
                ),
              );
            },
          ),
          if (_isLoading)
            ModalBarrier(
              dismissible: false,
              color: Colors.black.withOpacity(0.5),
            ),
          if (_isLoading)
            const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            ),
        ],
      ),
    );
  }
}

void _showAdminRequestDialog(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible:
        false, // Evita que el diálogo se cierre al hacer clic fuera de él
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        contentPadding:
            EdgeInsets.all(16.0), // Agrega padding alrededor del contenido
        content: Column(
          mainAxisSize:
              MainAxisSize.min, // Ajusta el tamaño de la columna al contenido
          children: [
            Image.asset(
              'assets/pickers.png',
              height: 100,
            ),
            SizedBox(height: 20),
            Text(
              'Solicitar Acceso Administrativo',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              '¿Deseas solicitar acceso con privilegios de administrador? \nSe enviará un correo para procesar tu solicitud.',
              style: TextStyle(color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        actions: <Widget>[
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            icon: Icon(
              Icons.cancel,
              color: Colors.white,
            ),
            label: Text(
              'Cancelar',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Navigator.of(context).pop(); // Cierra el diálogo
            },
          ),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              backgroundColor: const Color.fromARGB(255, 99, 156, 255),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            icon: Icon(
              Icons.check_circle,
              color: Colors.white,
            ),
            label: Text(
              'Enviar Solicitud',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () async {
              Navigator.of(context).pop(); // Cierra el diálogo

              // Obtener la información del usuario autenticado
              final userAut = AuthGoogleU().dataUser();

              late SocketSerDos socket;

              socket = Provider.of<SocketSerDos>(context, listen: false);

              // Emitir el evento por socket con la solicitud
              socket.socketS().emit('mandarSolicitud', {
                'email': userAut!.email, // Enviar el correo del usuario
                'uid': userAut.uid, // Enviar el UID del usuario
              });

              // Escuchar el evento de confirmación
              socket.socketS().on('solicitudEnviada', (data) {
                if (data['status'] == 'success') {
                  appRouters.pushReplacementNamed('/PendienteScreenAdmin');
                } else {
                  showErrorFlushbar(data['msg'], true, context);
                }
              });
            },
          ),
        ],
      );
    },
  );
}

void _showLogoutConfirmationDialog(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible:
        false, // Evita que el diálogo se cierre al hacer clic fuera de él
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        contentPadding:
            EdgeInsets.all(16.0), // Agrega padding alrededor del contenido
        content: Column(
          mainAxisSize:
              MainAxisSize.min, // Ajusta el tamaño de la columna al contenido
          children: [
            Image.asset(
              'assets/pickers.png',
              height: 50,
            ),
            SizedBox(height: 20),
            Text(
              'Confirmar Cierre de Sesión',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              '¿Estás seguro de que quieres cerrar sesión?',
              style: TextStyle(color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        actions: <Widget>[
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            icon: Icon(
              Icons.cancel,
              color: Colors.white,
            ),
            label: Text(
              'Cancelar',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Navigator.of(context).pop(); // Cierra el diálogo
            },
          ),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.green[400],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            icon: Icon(
              Icons.check_circle,
              color: Colors.white,
            ),
            label: Text(
              'Aceptar',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () async {
              Navigator.of(context).pop(); // Cierra el diálogo
              await FirebaseAuth.instance.signOut();
              await AuthGoogleU().logoutGoogle();
              final prefs = PreferencesUser();

              prefs.rutaPrincipal = '/login';
              appRouters.pushReplacementNamed('/login');
            },
          ),
        ],
      );
    },
  );
}

class RestauranteForm extends StatelessWidget {
  final Function(String, LatLng) onLocationSelected;
  final Function(bool, {bool fromCamera, String type}) onImagePicked;
  final String? selectedAddress;
  final LatLng? selectedLatLng;
  final File? frontINE;
  final File? backINE;
  final List<String> categories;
  final String? selectedCategory;
  final Function(String?) onCategoryChanged;
  final TextEditingController localNameController;
  final TextEditingController phoneController;
  final Function onSubmit;

  RestauranteForm({
    required this.onLocationSelected,
    required this.onImagePicked,
    this.selectedAddress,
    this.selectedLatLng,
    this.frontINE,
    this.backINE,
    required this.categories,
    this.selectedCategory,
    required this.onCategoryChanged,
    required this.localNameController,
    required this.phoneController,
    required this.onSubmit,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton.icon(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MapaScreen(
                  onLocationSelected: onLocationSelected,
                  initialAddress: selectedAddress,
                ),
              ),
            );
          },
          icon: Icon(Icons.location_on),
          label: Text('Seleccionar dirección del local'),
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Colors.red[700],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
        if (selectedAddress != null)
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Dirección seleccionada: $selectedAddress',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black),
            ),
          ),
        if (selectedLatLng != null)
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Coordenadas: ${selectedLatLng!.latitude}, ${selectedLatLng!.longitude}',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black),
            ),
          ),
        SizedBox(height: 20),
        TextField(
          controller: localNameController,
          decoration: InputDecoration(
            labelText: 'Nombre del local',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.store, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        TextField(
          controller: phoneController,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
            labelText: 'Teléfono',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.phone, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        DropdownButtonFormField<String>(
          value: selectedCategory,
          items: categories
              .map((category) => DropdownMenuItem(
                    value: category,
                    child: Text(category),
                  ))
              .toList(),
          onChanged: onCategoryChanged,
          decoration: InputDecoration(
            labelText: 'Categoría',
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          dropdownColor: Colors.red[50],
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(
              child: ElevatedButton.icon(
                onPressed: () =>
                    onImagePicked(true, fromCamera: true, type: 'INE'),
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto INE (Frente)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Flexible(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(true, type: 'INE'),
                icon: Icon(Icons.photo_library),
                label: Text('Subir INE (Frente)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (frontINE != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              frontINE!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(
              child: ElevatedButton.icon(
                onPressed: () =>
                    onImagePicked(false, fromCamera: true, type: 'INE'),
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto INE (Atrás)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Flexible(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(false, type: 'INE'),
                icon: Icon(Icons.photo_library),
                label: Text('Subir INE (Atrás)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (backINE != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              backINE!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        ElevatedButton(
          onPressed: () {
            onSubmit();
          },
          child: Text('Registrarme'),
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Color.fromARGB(255, 99, 156, 255),
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
            textStyle: TextStyle(fontSize: 18),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
      ],
    );
  }
}

class RepartidorForm extends StatelessWidget {
  final Function(bool, {bool fromCamera, String type}) onImagePicked;
  final File? frontINE;
  final File? backINE;
  final File? license;
  final File? backpack;
  final File? repartidorPhoto; // Agregado para la foto del repartidor

  final TextEditingController vehicleController;
  final TextEditingController contactNameController;
  final TextEditingController contactPhoneController;
  final TextEditingController contactRelationshipController;
  final TextEditingController
      repartidorNameController; // Agregado para el nombre del repartidor
  final Function onSubmit;

  RepartidorForm(
      {required this.onImagePicked,
      this.frontINE,
      this.backINE,
      this.license,
      this.backpack,
      this.repartidorPhoto, // Agregado para la foto del repartidor
      required this.vehicleController,
      required this.contactNameController,
      required this.contactPhoneController,
      required this.contactRelationshipController,
      required this.repartidorNameController, // Agregado para el nombre del repartidor
      required this.onSubmit});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 20),
        TextField(
          controller:
              repartidorNameController, // Campo para el nombre del repartidor
          decoration: InputDecoration(
            labelText: 'Nombre del Repartidor',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.person, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(true,
                    fromCamera: true,
                    type:
                        'RepartidorPhoto'), // Botón para tomar la foto del repartidor
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto del Repartidor'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (repartidorPhoto != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              repartidorPhoto!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        TextField(
          controller: vehicleController,
          decoration: InputDecoration(
            labelText: 'Marca/Modelo vehículo de transporte',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.directions_car, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () =>
                    onImagePicked(true, fromCamera: true, type: 'INE'),
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto INE (Frente)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(true, type: 'INE'),
                icon: Icon(Icons.photo_library),
                label: Text('Subir INE (Frente)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (frontINE != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              frontINE!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () =>
                    onImagePicked(false, fromCamera: true, type: 'INE'),
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto INE (Atrás)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(false, type: 'INE'),
                icon: Icon(Icons.photo_library),
                label: Text('Subir INE (Atrás)'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (backINE != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              backINE!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () =>
                    onImagePicked(true, fromCamera: true, type: 'License'),
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto Licencia'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(true, type: 'License'),
                icon: Icon(Icons.photo_library),
                label: Text('Subir Licencia'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (license != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              license!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () =>
                    onImagePicked(true, fromCamera: true, type: 'Backpack'),
                icon: Icon(Icons.camera_alt),
                label: Text('Tomar foto Mochila'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton.icon(
                onPressed: () => onImagePicked(true, type: 'Backpack'),
                icon: Icon(Icons.photo_library),
                label: Text('Subir Mochila'),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.red[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
        if (backpack != null)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(
              backpack!,
              height: 150,
            ),
          ),
        SizedBox(height: 20),
        TextField(
          controller: contactNameController,
          decoration: InputDecoration(
            labelText: 'Nombre persona de contacto',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.person, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        TextField(
          controller: contactPhoneController,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
            labelText: 'Teléfono de contacto',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.phone, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        TextField(
          controller: contactRelationshipController,
          decoration: InputDecoration(
            labelText: 'Parentesco',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.family_restroom, color: Colors.black),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red[700]!),
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelStyle: TextStyle(color: Colors.black),
          ),
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(height: 20),
        ElevatedButton(
          onPressed: () => onSubmit(),
          child: Text('Registrarme'),
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Color.fromARGB(255, 99, 156, 255),
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
            textStyle: TextStyle(fontSize: 18),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
      ],
    );
  }
}
