import 'dart:async';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/bloc/location/location_bloc.dart';
import 'package:dash_delivery/screen/realizar_pedido_screen.dart';
import 'package:dash_delivery/widgets/sidenav.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_webservice/places.dart' as loc;
import 'package:receive_sharing_intent/receive_sharing_intent.dart';

class DashboardNegocio extends StatefulWidget {
  const DashboardNegocio({super.key});

  @override
  _DashboardNegocioState createState() => _DashboardNegocioState();
}

class _DashboardNegocioState extends State<DashboardNegocio> {
  late LocationBloc locationBloc;
  late SocketSerDos _socket; // Instancia del servicio de socket

  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  final Completer<GoogleMapController> _controller = Completer();
  StreamSubscription? _intentDataStreamSubscription;
  // String? _sharedText;

  LatLng? _clienteCoordinates;
  String _clienteAddress = 'Seleccionar dirección';
  bool _isSubmitting = false;
  bool _isLink = false;
  bool _isHandlingLink = false;
  List<Map<String, String>> suggestions = [];
  Marker? clienteMarker;
  BitmapDescriptor? _motoIcon;
  BitmapDescriptor? _selectedIcon;

  LatLng _restaurantCoordinates = LatLng(0, 0); // Coordenadas del restaurante
  LatLng? initialPosition;
  LatLng? selectedLatLng;
  final String googleApiKey =
      'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0'; // Reemplaza con tu API key
  final _sharedFiles = <SharedMediaFile>[];

  @override
  void initState() {
    super.initState();
    _socket = Provider.of<SocketSerDos>(context, listen: false);
    _loadCustomIcon();
    _loadCustomIcons();
    // _getInitialRestaurantCoordinates();
    _initializeCoordinates();

    // Inicializar el stream para manejar archivos y textos compartidos
    _intentDataStreamSubscription =
        ReceiveSharingIntent.instance.getMediaStream().listen((value) {
      setState(() {
        _sharedFiles.clear();
        _sharedFiles.addAll(value);

        for (var i = 0; i < _sharedFiles.length; i++) {
          var match = urlPattern.firstMatch(_sharedFiles[i].path);
          if (match != null) {
            _searchController.text = "${match.group(0)}";
            _handleLink("${match.group(0)}");
          }
        }
      });
    }, onError: (err) {});

    ReceiveSharingIntent.instance.getInitialMedia().then((value) {
      setState(() {
        _sharedFiles.clear();
        _sharedFiles.addAll(value);

        for (var i = 0; i < _sharedFiles.length; i++) {
          var match = urlPattern.firstMatch(_sharedFiles[i].path);
          if (match != null) {
            _searchController.text = "${match.group(0)}";
            _handleLink("${match.group(0)}");
          }
        }
        // Tell the library that we are done processing the intent.
        ReceiveSharingIntent.instance.reset();
        // _sharedText = value;
        // if (value != null && _isValidLink(value)) {
        //   _searchController.text = value;
        //   _handleLink(value);
        // }
      });
    });
  }

  Future<void> _loadCustomIcons() async {
    final BitmapDescriptor motoIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pin.png',
    );

    final BitmapDescriptor selectedIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pinD.png',
    );

    if (mounted) {
      setState(() {
        _motoIcon = motoIcon;
        _selectedIcon = selectedIcon;
      });
    }
  }

  RegExp urlPattern = RegExp(
    r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
    caseSensitive: false,
  );

  Future<void> _loadCustomIcon() async {
    final BitmapDescriptor motoIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pin.png',
    );

    if (mounted) {
      setState(() {
        _motoIcon = motoIcon;
      });
    }
  }

  Future<void> _initializeCoordinates() async {
    var coordinates = PreferencesUser().coordenadas;

    if (coordinates.isEmpty) {
      final socket = Provider.of<SocketSerDos>(context, listen: false);
      final userAut = AuthGoogleU().dataUser();

      socket.emit('obtenerCoordenadasRestaurante', {'uid': userAut!.uid});
      socket.socketS().on('respuestaCoordenadasRestaurante', (data) {
        // if (mounted) {
        final lat = data['latitude'];
        final lng = data['longitude'];
        PreferencesUser().coordenadas = '$lat , $lng';
        _restaurantCoordinates = stringToLatLng(PreferencesUser().coordenadas);
        if (mounted) {
          setState(() {
            initialPosition = _restaurantCoordinates;
          });
        }
      });
    }

    if (coordinates.isNotEmpty) {
      _restaurantCoordinates = stringToLatLng(coordinates);
      if (mounted) {
        setState(() {
          initialPosition = _restaurantCoordinates;
        });
      }
    }
  }

  void _getInitialRestaurantCoordinates() {
    final coordinates = PreferencesUser().coordenadas;

    if (coordinates != null) {
      stringToLatLng(coordinates);
      if (mounted) {
        setState(() {
          initialPosition = _restaurantCoordinates;
        });
      }
    }
  }

  LatLng stringToLatLng(String coordinates) {
    List<String> parts =
        coordinates.split(',').map((part) => part.trim()).toList();
    double latitude = double.parse(parts[0]);
    double longitude = double.parse(parts[1]);
    return LatLng(latitude, longitude);
  }

  Future<void> _getCurrentLocation({LatLng? newLocation}) async {
    try {
      LatLng currentLatLng;
      if (newLocation != null) {
        currentLatLng = newLocation;
      } else {
        Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        currentLatLng = LatLng(position.latitude, position.longitude);
      }
      setState(() {
        _clienteCoordinates = currentLatLng;
      });
      _updateAddressFromLatLng(currentLatLng);
    } catch (e) {}
  }

  Future<void> _updateAddressFromLatLng(LatLng location) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(location.latitude, location.longitude);
      if (placemarks.isNotEmpty) {
        final place = placemarks.first;
        setState(() {
          _clienteCoordinates = location;
          _clienteAddress =
              '${place.street}, ${place.locality}, ${place.postalCode}, ${place.country}';
          _addressController.text = _clienteAddress;
        });
      }
    } catch (e) {}
  }

  Future<void> _handleLink(String link) async {
    if (_isHandlingLink) return;
    _isHandlingLink = true;

    _socket.emit('expandUrl', link);

    _socket.socketS().on("expandedUrl", (data) async {
      final Uri uri = Uri.parse(data['url']);
      final pathSegments = uri.pathSegments;

      String? coordsString;

      if (pathSegments.isNotEmpty) {
        final coordsSegment = pathSegments
            .firstWhere((segment) => segment.contains('@'), orElse: () => '');
        if (coordsSegment.isNotEmpty) {
          final parts = coordsSegment.split('@');
          if (parts.length > 1) {
            coordsString = parts[1];
          }
        }
      }

      if (coordsString != null) {
        final coordinates = coordsString.split(',');
        if (coordinates.length >= 2) {
          final lat = double.tryParse(coordinates[0]);
          final lng = double.tryParse(coordinates[1]);

          if (lat != null && lng != null) {
            LatLng newLocation = LatLng(lat, lng);
            await _updateClienteLocation(newLocation);
          }
        }
      } else {
        final placeIndex = pathSegments.indexOf('place');
        if (placeIndex != -1 && placeIndex + 1 < pathSegments.length) {
          coordsString = pathSegments[placeIndex + 1].replaceAll('+', ' ');
        }

        if (coordsString != null) {
          _addressController.text = coordsString;
          await _getCoordinatesFromAddress(coordsString);
        }

        final queryParams = uri.queryParameters;
        if (queryParams.containsKey('q')) {
          final qParam = queryParams['q'];
          if (qParam != null) {
            coordsString = qParam.replaceAll('+', ' ');
            _addressController.text = coordsString;
            await _getCoordinatesFromAddress(coordsString);
          }
        }
      }

      _socket.socketS().off("expandedUrl"); // Desregistrar el evento
      _isHandlingLink = false;
    });
  }

  Future<void> _getCoordinatesFromAddress(String address) async {
    try {
      List<Location> locations = await locationFromAddress(address);
      if (locations.isNotEmpty) {
        LatLng newLocation =
            LatLng(locations.first.latitude, locations.first.longitude);
        await _updateClienteLocation(newLocation);
      }
    } catch (e) {}
  }

  void _onMapCreated(GoogleMapController controller) {
    if (!_controller.isCompleted) {
      _controller.complete(controller);
    }
    if (initialPosition != null) {
      controller.animateCamera(CameraUpdate.newLatLng(initialPosition!));
    }
  }

  Future<void> _searchPlace(String placeId) async {
    try {
      final places = loc.GoogleMapsPlaces(apiKey: googleApiKey);
      final response = await places.getDetailsByPlaceId(placeId);

      if (response.status == "OK") {
        final result = response.result;
        final location = result.geometry!.location;
        final latLng = LatLng(location.lat, location.lng);
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newLatLng(latLng));
        _updateClienteLocation(latLng);
      } else {}
    } catch (e) {}
  }

  Future<void> _getSuggestions(String input) async {
    try {
      final places = loc.GoogleMapsPlaces(apiKey: googleApiKey);
      final response = await places.autocomplete(input);

      print("response estatus");
      print(response.errorMessage);
      if (response.status == "OK") {
        setState(() {
          suggestions = response.predictions
              .map<Map<String, String>>((p) => {
                    'description': '${p.description}',
                    'place_id': '${p.placeId}'
                  })
              .toList();
        });
      } else {}
    } catch (e) {}
  }

  Future<void> _updateClienteLocation(LatLng latLng) async {
    try {
      final GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newLatLng(latLng));
      // Actualiza _clienteCoordinates antes de usarlo
      setState(() {
        _clienteCoordinates = latLng;
      });

      setState(() {
        clienteMarker = Marker(
          markerId: MarkerId('clienteLocation'),
          position: latLng,
          icon: _selectedIcon ?? BitmapDescriptor.defaultMarker,
          infoWindow: InfoWindow(
            title:
                '${_clienteCoordinates!.latitude.toStringAsFixed(6)}, ${_clienteCoordinates!.longitude.toStringAsFixed(6)}',
          ),
        );
        _clienteCoordinates = latLng;
        suggestions = [];
      });

      Future.delayed(Duration(milliseconds: 100), () {
        controller.showMarkerInfoWindow(MarkerId('clienteLocation'));
      });

      List<Placemark> placemarks =
          await placemarkFromCoordinates(latLng.latitude, latLng.longitude);
      if (placemarks.isNotEmpty) {
        Placemark place = placemarks.first;
        setState(() {
          _clienteAddress =
              '${place.street}, ${place.locality}, ${place.subLocality}, ${place.administrativeArea}';
          _searchController.text = _clienteAddress!;
        });
      }
      _updateCameraBounds();
    } catch (e) {
      print(e);
    }
  }

  Future<void> _updateCameraBounds() async {
    final GoogleMapController controller = await _controller.future;
    LatLngBounds bounds;

    if (_clienteCoordinates != null) {
      bounds = LatLngBounds(
        southwest: LatLng(
          _clienteCoordinates!.latitude < _restaurantCoordinates.latitude
              ? _clienteCoordinates!.latitude
              : _restaurantCoordinates.latitude,
          _clienteCoordinates!.longitude < _restaurantCoordinates.longitude
              ? _clienteCoordinates!.longitude
              : _restaurantCoordinates.longitude,
        ),
        northeast: LatLng(
          _clienteCoordinates!.latitude > _restaurantCoordinates.latitude
              ? _clienteCoordinates!.latitude
              : _restaurantCoordinates.latitude,
          _clienteCoordinates!.longitude > _restaurantCoordinates.longitude
              ? _clienteCoordinates!.longitude
              : _restaurantCoordinates.longitude,
        ),
      );
    } else {
      bounds = LatLngBounds(
        southwest: _restaurantCoordinates,
        northeast: _restaurantCoordinates,
      );
    }

    final cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
    controller.animateCamera(cameraUpdate);
  }

  bool _isValidLink(String input) {
    final Uri? uri = Uri.tryParse(input);
    return uri != null && (uri.isScheme('http') || uri.isScheme('https'));
  }

  @override
  void dispose() {
    _intentDataStreamSubscription?.cancel();

    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          // Mapa que cubre la pantalla
          Positioned.fill(
            child: Column(
              children: [
                const Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(top: 40),
                    child: Text(
                      'Bienvenido',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Center(
                  child: Column(
                    children: const [
                      Text(
                        'Por favor selecciona a donde enviarás tu pedido',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: EdgeInsets.all(30.0),
                        child: Text(
                          'Ingresa de forma manual o copia y pega la dirección del Cliente',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      LayoutBuilder(
                        builder:
                            (BuildContext context, BoxConstraints constraints) {
                          // constraints.maxHeight y constraints.maxWidth te dan el tamaño disponible
                          return Stack(
                            children: [
                              GoogleMap(
                                onMapCreated: _onMapCreated,
                                initialCameraPosition: initialPosition != null
                                    ? CameraPosition(
                                        target: initialPosition!, zoom: 15)
                                    : const CameraPosition(
                                        target: LatLng(0, 0), zoom: 15),
                                markers: {
                                  if (clienteMarker != null) clienteMarker!,
                                  Marker(
                                    markerId: MarkerId('restaurantLocation'),
                                    position: _restaurantCoordinates,
                                    icon: _motoIcon ??
                                        BitmapDescriptor.defaultMarker,
                                  ),
                                },
                                onTap: (latLng) {
                                  _updateClienteLocation(latLng);
                                },
                                myLocationEnabled: true,
                              ),
                              Positioned(
                                // Ajusta la posición basada en el tamaño del contenedor
                                bottom: constraints.maxHeight > 400 ? 150 : 10,
                                right: constraints.maxWidth > 300 ? 10 : 5,
                                height: 40,
                                width: 40,
                                child: FloatingActionButton(
                                  onPressed: _goToCurrentLocation,
                                  child: Icon(Icons.my_location),
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                      Positioned(
                        top: 10,
                        left: 10,
                        right: 10,
                        child: Column(
                          children: [
                            TextField(
                              controller: _searchController,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                hintText: 'Buscar lugar...',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                suffixIcon: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    if (_isLink)
                                      IconButton(
                                        icon: Icon(Icons.search),
                                        onPressed: () async {
                                          if (_isValidLink(
                                              _searchController.text)) {
                                            await _handleLink(
                                                _searchController.text);
                                          }
                                        },
                                      ),
                                    IconButton(
                                      icon: Icon(Icons.clear),
                                      onPressed: () {
                                        _searchController.clear();
                                        setState(() {
                                          suggestions = [];
                                          clienteMarker = null;
                                          _isLink = false;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              onChanged: (value) {
                                if (_isValidLink(value)) {
                                  setState(() {
                                    _isLink = true;
                                    suggestions = [];
                                  });
                                } else {
                                  setState(() {
                                    _isLink = false;
                                    if (value.isNotEmpty) {
                                      _getSuggestions(value);
                                    } else {
                                      suggestions = [];
                                    }
                                  });
                                }
                              },
                            ),
                            if (suggestions.isNotEmpty)
                              Container(
                                height: 200,
                                color: Colors.white,
                                child: ListView.builder(
                                  itemCount: suggestions.length,
                                  itemBuilder: (context, index) {
                                    return ListTile(
                                      title: Text(
                                          suggestions[index]['description']!),
                                      onTap: () {
                                        _searchController.text =
                                            suggestions[index]['description']!;
                                        _searchPlace(
                                            suggestions[index]['place_id']!);
                                      },
                                    );
                                  },
                                ),
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          // Container con el botón y el texto, sobre el mapa
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding: const EdgeInsets.all(16.0),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ElevatedButton(
                    onPressed: _clienteCoordinates != null
                        ? () async {
                            final orderCompleted = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => RealizarPedidoScreen(
                                  address: _clienteAddress,
                                  location: _clienteCoordinates!,
                                ),
                              ),
                            );

                            if (orderCompleted == true) {
                              _getCurrentLocation();
                              _addressController.clear();
                              _clienteCoordinates = null;
                              _clienteAddress = 'Seleccionar dirección';
                            }
                          }
                        : null,
                    child: const Text('Realizar pedido'),
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Ahora da click en realizar pedido y agrega la información del pedido y cliente',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),

          // Indicador de carga si está enviando
          if (_isSubmitting)
            ModalBarrier(
              dismissible: false,
              color: Colors.black.withOpacity(0.5),
            ),
          if (_isSubmitting)
            const Center(
              child: CircularProgressIndicator(),
            ),
        ],
      ),
    );
  }

  Future<void> _goToCurrentLocation() async {
    final GoogleMapController controller = await _controller.future;
    if (_restaurantCoordinates != null) {
      controller.animateCamera(
        CameraUpdate.newLatLngZoom(_restaurantCoordinates, 15),
      );
    }
  }
}
