import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/verificacionService.dart';
import 'package:dash_delivery/widgets/CustomUpdateBanner.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:dash_delivery/screen/detalle_pedido_screen.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:dash_delivery/services/socket.dart';

class DashboardRepartidor extends StatefulWidget {
  const DashboardRepartidor({super.key});

  @override
  State<DashboardRepartidor> createState() => _DashboardRepartidorState();
}

class _DashboardRepartidorState extends State<DashboardRepartidor> {
  List<dynamic> _pedidosList = []; // Lista para almacenar los pedidos
  late SocketSerDos socket;
  bool _isLoading = true; // Variable para manejar el estado de carga

  @override
  void initState() {
    super.initState();
    _fetchPedidos();

    // Obtener la instancia de Socket
    socket = Provider.of<SocketSerDos>(context, listen: false);

    // Conectar el socket y escuchar el evento 'pedidoActualizado'
    socket.socketS().on('pedidoActualizado', (data) {
      // Recargar la lista de pedidos cuando se recibe el evento 'pedidoActualizado'
      _fetchPedidos();
    });
  }

  @override
  void dispose() {
    // Limpiar la suscripción al evento 'pedidoActualizado'
    socket.socketS().off('pedidoActualizado');
    super.dispose();
  }

  Future<void> _fetchPedidos() async {
    try {
      final pedidos = await Provider.of<PlatilloService>(context, listen: false)
          .obtenerPedidosRepa();
      setState(() {
        _pedidosList = pedidos; // Almacena los pedidos en la lista de estado
        _isLoading = false; // Dejar de mostrar el indicador de carga
        if (_pedidosList.isEmpty) {
          print("No se han encontrado pedidos.");
        }
      });
    } catch (error) {
      // Manejar error si es necesario
      setState(() {
        _isLoading =
            false; // Dejar de mostrar el indicador de carga incluso en caso de error
        print("Error al obtener los pedidos: $error");
      });
    }
  }

  Future<void> _recargarPedidos() async {
    setState(() {
      _isLoading =
          true; // Mostrar el indicador de carga mientras se recargan los pedidos
    });
    await _fetchPedidos(); // Recarga los pedidos
  }

  void _eliminarPedidoDeLaLista(int id) {
    setState(() {
      _pedidosList =
          _pedidosList.where((pedido) => pedido['id'] != id).toList();
    });
  }

  void _handleYellowButtonPressed() {
    // Acción para el botón amarillo
  }

  void _handleRedButtonPressed() {
    // Acción para el botón rojo
  }

  void _onItemTapped(int index) {
    setState(() {});
    // Acciones según el índice seleccionado
    switch (index) {
      case 0:
        _recargarPedidos();
        break;
      case 1:
        appRouters.pushReplacementNamed('/misPedidos');
        break;
      case 2:
        FirebaseAuth.instance.signOut();
        AuthGoogleU().logoutGoogle();
        PreferencesUser().rutaPrincipal = '/login';
        Navigator.pushReplacementNamed(context, '/login');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          RefreshIndicator(
            onRefresh: _recargarPedidos,
            child: _isLoading
                ? const Center(
                    child:
                        CircularProgressIndicator()) // Mostrar un indicador mientras se carga
                : _pedidosList.isEmpty
                    ? _buildNoPedidosMessage()
                    : ListView.builder(
                        itemCount: _pedidosList.length,
                        itemBuilder: (context, index) {
                          final pedido = _pedidosList[index];
                          final coordenadas =
                              _parseCoordinates(pedido['coordenadas']);
                          final coordenadasI =
                              _parseCoordinates(pedido['coordenadas_inicio']);
                          final registro = _parseDateTime(pedido['registro']);
                          return _buildPedidoCard(
                              context,
                              pedido['id'],
                              pedido['nombre_local'],
                              pedido['descripcion'],
                              pedido['detalle'],
                              pedido['precio'],
                              pedido['ubicacion'],
                              pedido['direccion'],
                              coordenadas,
                              coordenadasI,
                              registro,
                              pedido['total_pedido'],
                              pedido['ganancia'],
                              pedido['estatus']);
                        },
                      ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoPedidosMessage() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'No se han encontrado pedidos actualmente.',
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _recargarPedidos,
            child: const Text('Recargar'),
          ),
        ],
      ),
    );
  }

  Widget _buildPedidoCard(
    BuildContext context,
    int id,
    String nombreLocal,
    String descripcion,
    String detalle,
    String precio,
    String ubicacion,
    String direccion,
    LatLng coordenadas,
    LatLng coordenadasI,
    String registro,
    String monto,
    String ganancia,
    String estatus,
  ) {
    return Stack(
      children: [
        Card(
          margin: const EdgeInsets.all(8.0),
          child: ListTile(
            title: Text(
              nombreLocal,
              textAlign: TextAlign.center,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('De: $ubicacion'),
                Text('A: $direccion'),
                Text('Monto: \$${precio}'),
                Text('Registrado el: $registro'),
                Text('Estatus: $estatus'),
              ],
            ),
            onTap: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetallePedidoScreen(
                    id: id,
                    nombreLocal: nombreLocal,
                    descripcion: descripcion,
                    detalle: detalle,
                    direccion: direccion,
                    coordenadas: coordenadas,
                    coordenadasI: coordenadasI,
                    precio: precio,
                    monto: monto,
                    ganancia: ganancia,
                  ),
                ),
              );

              if (result != null && result is int) {
                _eliminarPedidoDeLaLista(result);
              }
            },
          ),
        ),
        if (estatus == 'LIBERADO')
          Positioned(
            bottom: 10,
            right: 0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 4.0),
                  decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  child: Transform.rotate(
                    angle: 0, // Rotación en radianes (45 grados en negativo)
                    child: Text(
                      'LIBERADO',
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                // Espacio entre las etiquetas
                Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 4.0),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  child: Transform.rotate(
                    angle: 0, // Rotación en radianes (45 grados en negativo)
                    child: Text(
                      'PRIORITARIO',
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
      ],
    );
  }

  LatLng _parseCoordinates(String coordenadas) {
    final parts = coordenadas.split(',');
    final lat = double.parse(parts[0].trim());
    final lng = double.parse(parts[1].trim());
    return LatLng(lat, lng);
  }

  String _parseDateTime(String dateTimeString) {
    try {
      final dateTime = DateTime.parse(dateTimeString);
      final formatter = DateFormat('dd/MM/yyyy HH:mm');
      return formatter.format(dateTime);
    } catch (e) {
      return 'Fecha no válida';
    }
  }
}
