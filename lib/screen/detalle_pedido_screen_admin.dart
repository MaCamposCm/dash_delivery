import 'dart:typed_data';
import 'package:another_flushbar/flushbar.dart';
import 'package:dash_delivery/bloc/location/location_bloc.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/widgets/cuota.dart';
import 'package:dash_delivery/widgets/flush_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/material.dart' as a;

class DetallePedidoScreenAdmin extends StatefulWidget {
  final int id;
  final String nombreLocal;
  final String descripcion;
  final String detalle;
  final String direccion;
  final LatLng coordenadas;
  final LatLng coordenadasI;
  final String precio;
  final String monto;
  final String ganancia;

  const DetallePedidoScreenAdmin({
    Key? key,
    required this.id,
    required this.nombreLocal,
    required this.descripcion,
    required this.detalle,
    required this.direccion,
    required this.coordenadas,
    required this.coordenadasI,
    required this.precio,
    required this.monto,
    required this.ganancia,
  }) : super(key: key);

  @override
  _DetallePedidoScreenState createState() => _DetallePedidoScreenState();
}

class _DetallePedidoScreenState extends State<DetallePedidoScreenAdmin> {
  Uint8List? imagenBytes;
  bool isLoading = true;
  bool isLoadingMap = true; // Indicador de carga del mapa

  late SocketSerDos socket;
  List<LatLng> _polylineCoordinates = []; // Coordenadas de la ruta
  GoogleMapController? _mapController;
  BitmapDescriptor? _selectedIcon; // Icono para el marcador del cliente
  BitmapDescriptor? _originIcon; // Icono para el marcador del cliente
  bool isImageExpanded = false;

  @override
  void initState() {
    super.initState();
    _loadImage();
    socket = Provider.of<SocketSerDos>(context, listen: false);
    _loadCustomIcon();
    _fetchRoute(); // Obtener la ruta
  }

  Future<void> _loadCustomIcon() async {
    final BitmapDescriptor selectedIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pinD.png',
    );
    final BitmapDescriptor originIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pin.png',
    );

    if (mounted) {
      setState(() {
        _originIcon = originIcon;
        _selectedIcon = selectedIcon;
      });
    }
  }

  Future<void> _fetchRoute() async {
    const String apiKey = 'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0';
    final String origin =
        '${widget.coordenadasI.latitude},${widget.coordenadasI.longitude}';
    final String destination =
        '${widget.coordenadas.latitude},${widget.coordenadas.longitude}';

    final url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$apiKey';

    final response = await http.get(Uri.parse(url));
    final data = json.decode(response.body);

    if (data['status'] == 'OK') {
      final points = data['routes'][0]['overview_polyline']['points'];
      setState(() {
        _polylineCoordinates = _decodePolyline(points);
        isLoadingMap = false; // Mapa listo para mostrarse
      });
    } else {
      print('Error fetching route: ${data['status']}');
    }
  }

  List<LatLng> _decodePolyline(String encoded) {
    List<LatLng> polyline = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      final latitude = lat / 1E5;
      final longitude = lng / 1E5;
      polyline.add(LatLng(latitude, longitude));
    }
    return polyline;
  }

  @override
  void dispose() {
    // Limpiar las suscripciones al socket
    socket.socketS().off('verificacionPedidosPendientes');
    socket.socketS().off('pedidoAceptado');
    super.dispose();
  }

  void _loadImage() async {
    final platilloService =
        Provider.of<PlatilloService>(context, listen: false);
    final imagen = await platilloService.obtenerImagenO(widget.id.toString());
    if (mounted) {
      setState(() {
        imagenBytes = imagen;
        isLoading = false;
      });
    }
  }

  void _asignarRepartidor() async {
    // Remover cualquier oyente previo del evento 'repartidoresDisponibles' para evitar múltiples llamadas
    socket.socketS().off('repartidoresDisponibles');

    socket.emit('verRepartidoresDisponibles');

    socket.socketS().on('repartidoresDisponibles', (data) {
      if (!mounted) return;

      List repartidores = data['repartidores'];
      _showRepartidoresModal(repartidores);
    });
  }

  void _confirmarAsignacion(int repartidorId) async {
    Navigator.of(context).pop(); // Cerrar el modal de repartidores

    final DateTime now = DateTime.now();
    final String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

    socket.emit('asignarRepartidor', {
      'idPedido': widget.id,
      'idRepartidor': repartidorId,
      'fecha': formattedDate, // Enviar la fecha actual
    });

    socket.socketS().on('repartidorAsignado', (data) async {
      if (data['status'] == 'success') {
        if (mounted)
          await _showErrorFlushbar('Repartidor asignado con éxito.', false);
        Navigator.pop(context, widget.id); // Cerrar la pantalla de detalles
      } else {
        if (mounted)
          await _showErrorFlushbar('Error al asignar repartidor.', true);
      }
    });
  }

  Future<void> _showErrorFlushbar(String message, bool isError) async {
    return await Flushbar(
      message: message,
      duration: const Duration(seconds: 3),
      backgroundColor: isError
          ? const Color.fromRGBO(205, 63, 63, 1)
          : const Color.fromRGBO(51, 96, 137, 1),
      messageColor: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      margin: const EdgeInsets.all(16.0),
      flushbarPosition: FlushbarPosition.TOP,
      icon: const Icon(
        Icons.error,
        color: Colors.white,
      ),
    ).show(context);
  }

  void _showRepartidoresModal(List repartidores) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Asignar Repartidor'),
          content: Container(
            width: double.maxFinite,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: repartidores.length,
              itemBuilder: (context, index) {
                final repartidor = repartidores[index];
                return Column(
                  children: [
                    ListTile(
                      title: Text(repartidor['nombre_repartidor']),
                      subtitle: Text(
                        'Correo: ${repartidor['correo']}\nPedidos recolectados: ${repartidor['recolectado_count']}',
                      ),
                      trailing: ElevatedButton(
                        onPressed: () {
                          _confirmarAsignacion(repartidor['id']);
                        },
                        child: const Text('Asignar'),
                      ),
                    ),
                    const Divider(), // Línea de separación
                  ],
                );
              },
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Text('Cancelar'),
            ),
          ],
        );
      },
    );
  }

  void _aceptarPedido() async {
    final userAut = await AuthGoogleU().dataUser();
    final DateTime now = DateTime.now();
    final String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

    if (userAut != null) {
      final uid = userAut.uid;

      socket.emit('verificarPedidosPendientes', {
        'uid': uid,
      });

      socket.socketS().on('verificacionPedidosPendientes', (data) {
        if (!mounted) return;
        if (data['pendientes'] < 2) {
          socket.emit('aceptarPedido', {
            'idPedido': widget.id,
            'uid': uid,
            'recolectado': formattedDate,
          });

          socket.socketS().off('pedidoAceptado');
          socket.socketS().on('pedidoAceptado', (data) {
            if (!mounted) return;
            if (data['status'] == 'success') {
              showErrorFlushbar('Pedido aceptado con éxito.', false, context);

              Future.delayed(const Duration(seconds: 1), () {
                if (mounted) {
                  Navigator.pop(context);
                  Navigator.pop(context, widget.id);
                }
              });
            } else {
              showErrorFlushbar(data['msg'], true, context);
            }
          });
        } else {
          showErrorFlushbar(
              'No se pueden aceptar más pedidos hasta que completes alguno pendiente.',
              true,
              context);
        }
      });
    } else {
      if (!mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Usuario no autenticado')),
      );
    }
  }

  void showErrorFlushbar(String message, bool isError, BuildContext context) {
    if (!mounted) return;
    Flushbar(
      message: message,
      duration: const Duration(seconds: 3),
      backgroundColor: isError
          ? const Color.fromRGBO(205, 63, 63, 1)
          : const Color.fromRGBO(51, 96, 137, 1),
      messageColor: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      margin: const EdgeInsets.all(16.0),
      flushbarPosition: FlushbarPosition.TOP,
      icon: const Icon(
        Icons.error,
        color: Colors.white,
      ),
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text('Detalles del pedido.'),
      ),
      body: Stack(
        children: [
          if (isLoadingMap)
            Container(
              color: Colors.white,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            ),
          if (!isLoadingMap)
            Positioned.fill(
              child: _buildGoogleMap(),
            ),
          Positioned(
            top: 16.0,
            left: 16.0,
            right: 16.0,
            child: Center(
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
                elevation: 8.0,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Negocio: ${widget.nombreLocal}',
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(height: 8.0),
                            Text(
                              'Descripción:',
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                            _buildDetailTextWithIndicator(widget.descripcion),
                            const SizedBox(height: 8.0),
                            Text(
                              'Monto Total: \$${widget.precio}',
                              style: const TextStyle(fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(height: 4.0),
                            Text(
                              'Monto: \$${widget.monto}',
                              style: const TextStyle(fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(height: 4.0),
                            Text(
                              'Ganancia: \$${widget.ganancia}',
                              style: const TextStyle(fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Detalle:',
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                            _buildDetailTextWithIndicator(widget.detalle),
                            const SizedBox(height: 8.0),
                            if (imagenBytes != null)
                              GestureDetector(
                                onTap: _expandImage,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(16.0),
                                  child: Image.memory(
                                    imagenBytes!,
                                    width: double.infinity,
                                    height: 100,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (isLoading)
            const Center(
              child: CircularProgressIndicator(),
            ),
          if (isImageExpanded) _buildExpandedImage(),
          Positioned(
            bottom: 64.0,
            left: 16.0,
            right: 16.0,
            child: Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _buildDistanceIndicator(
                    color: Colors.blue,
                    text:
                        'Distancia: ${_calculateDistance(widget.coordenadasI, widget.coordenadas)} km',
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 16.0,
            left: 16.0,
            right: 16.0,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ElevatedButton(
                    onPressed: _asignarRepartidor,
                    child: const Text('Asignar Repartidor'),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.red,
                      minimumSize: const Size(double.infinity, 50),
                    ),
                    child: const Text('Cerrar'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDistanceIndicator({required Color color, required String text}) {
    return Row(
      children: [
        Container(
          width: 10,
          height: 10,
          color: color,
        ),
        const SizedBox(width: 8),
        Text(
          text,
          style: TextStyle(color: color, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  Widget _buildDetailTextWithIndicator(String detail) {
    bool isOverflowing = false;

    return LayoutBuilder(
      builder: (context, constraints) {
        final textSpan = TextSpan(
          text: detail,
          style: const TextStyle(fontSize: 16),
        );

        final textPainter = TextPainter(
          text: textSpan,
          maxLines: 2,
          textDirection: a.TextDirection.ltr,
        );

        textPainter.layout(maxWidth: constraints.maxWidth);

        if (textPainter.didExceedMaxLines) {
          isOverflowing = true;
        }

        return GestureDetector(
          onTap: () {
            if (isOverflowing) {
              _showExpandedText('Detalle', detail);
            }
          },
          child: Row(
            children: [
              Flexible(
                child: Text(
                  detail,
                  style: const TextStyle(fontSize: 16),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  String _calculateDistance(LatLng? start, LatLng end) {
    if (start == null) return '0.0';

    final double distance = Geolocator.distanceBetween(
        start.latitude, start.longitude, end.latitude, end.longitude);

    return (distance / 1000).toStringAsFixed(2);
  }

  Widget _buildGoogleMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: widget.coordenadas,
        zoom: 14,
      ),
      polylines: {
        Polyline(
          polylineId: const PolylineId('route'),
          points: _polylineCoordinates,
          color: Colors.blue,
          width: 5,
        ),
      },
      markers: {
        Marker(
          markerId: const MarkerId('origin'),
          position: widget.coordenadasI,
          icon: _originIcon ?? BitmapDescriptor.defaultMarker,
        ),
        Marker(
          markerId: const MarkerId('destination'),
          position: widget.coordenadas,
          icon: _selectedIcon ?? BitmapDescriptor.defaultMarker,
        ),
      },
      onMapCreated: (GoogleMapController controller) {
        _mapController = controller;
        _updateMapCamera();
      },
    );
  }

  void _updateMapCamera() {
    if (_mapController == null) return;

    LatLngBounds bounds;

    final allPoints = [widget.coordenadas, widget.coordenadasI];

    double southWestLat = allPoints[0].latitude;
    double southWestLng = allPoints[0].longitude;
    double northEastLat = allPoints[0].latitude;
    double northEastLng = allPoints[0].longitude;

    for (LatLng point in allPoints) {
      if (point.latitude < southWestLat) southWestLat = point.latitude;
      if (point.longitude < southWestLng) southWestLng = point.longitude;
      if (point.latitude > northEastLat) northEastLat = point.latitude;
      if (point.longitude > northEastLng) northEastLng = point.longitude;
    }

    bounds = LatLngBounds(
      southwest: LatLng(southWestLat, southWestLng),
      northeast: LatLng(northEastLat, northEastLng),
    );

    _mapController!.animateCamera(
      CameraUpdate.newLatLngBounds(bounds, 250),
    );
  }

  void _expandImage() {
    setState(() {
      isImageExpanded = true;
    });
  }

  void _closeExpandedImage() {
    setState(() {
      isImageExpanded = false;
    });
  }

  void _showExpandedText(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            elevation: 8.0,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        title,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 16.0),
                  SingleChildScrollView(
                    child: Text(
                      content,
                      style: const TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildExpandedImage() {
    return Center(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        elevation: 8.0,
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Image.memory(
                  imagenBytes!,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              top: 8.0,
              right: 8.0,
              child: IconButton(
                icon: const Icon(Icons.close, color: Colors.white),
                onPressed: _closeExpandedImage,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
