import 'dart:typed_data';
import 'package:another_flushbar/flushbar.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/screen/estatus_pedido.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/widgets/cuota.dart';
import 'package:dash_delivery/widgets/flush_bar.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetallePedidoScreenNegocio extends StatefulWidget {
  final int id;
  final String nombreLocal;
  final String descripcion;
  final String detalle;
  final String direccion;
  final LatLng coordenadas;
  final LatLng coordenadasI;
  final String precio;
  final String estatus;

  const DetallePedidoScreenNegocio({
    Key? key,
    required this.id,
    required this.nombreLocal,
    required this.descripcion,
    required this.detalle,
    required this.direccion,
    required this.coordenadas,
    required this.coordenadasI,
    required this.precio,
    required this.estatus,
  }) : super(key: key);

  @override
  _DetallePedidoScreenState createState() => _DetallePedidoScreenState();
}

class _DetallePedidoScreenState extends State<DetallePedidoScreenNegocio> {
  Uint8List? imagenBytes;
  bool isLoading = true;
  late SocketSerDos socket;
  List<LatLng> _polylineCoordinates = []; // Coordenadas de la ruta
  GoogleMapController? _mapController;
  BitmapDescriptor? _motoIcon; // Icono personalizado

  @override
  void initState() {
    super.initState();
    _loadImage();
    socket = Provider.of<SocketSerDos>(context, listen: false);
    _loadCustomIcon(); // Cargar el icono personalizado
    _fetchRoute(); // Obtener la ruta
  }

  Future<void> _loadCustomIcon() async {
    final BitmapDescriptor motoIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)), // Tamaño del icono
      'assets/pin.png', // Asegúrate de tener la imagen de la moto en la carpeta de assets
    );

    setState(() {
      _motoIcon = motoIcon;
    });
  }

  Future<void> _fetchRoute() async {
    final String apiKey = 'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0';
    final String origin =
        '${widget.coordenadasI.latitude},${widget.coordenadasI.longitude}';
    final String destination =
        '${widget.coordenadas.latitude},${widget.coordenadas.longitude}';

    final url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$apiKey';

    final response = await http.get(Uri.parse(url));
    final data = json.decode(response.body);

    if (data['status'] == 'OK') {
      final points = data['routes'][0]['overview_polyline']['points'];
      setState(() {
        _polylineCoordinates = _decodePolyline(points);
      });
    } else {}
  }

  List<LatLng> _decodePolyline(String encoded) {
    List<LatLng> polyline = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      final latitude = lat / 1E5;
      final longitude = lng / 1E5;
      polyline.add(LatLng(latitude, longitude));
    }
    return polyline;
  }

  @override
  void dispose() {
    // Limpiar las suscripciones al socket
    socket.socketS().off('verificacionPedidosPendientes');
    socket.socketS().off('pedidoAceptado');
    socket.socketS().off('pedidoCancelado');
    super.dispose();
  }

  void _loadImage() async {
    final platilloService =
        Provider.of<PlatilloService>(context, listen: false);
    final imagen = await platilloService.obtenerImagena(widget.id);
    if (mounted) {
      setState(() {
        imagenBytes = imagen;
        isLoading = false;
      });
    }
  }

  void _cancelarPedido() async {
    socket.emit('cancelarPedido', {'idPedido': widget.id});

    socket.socketS().on('pedidoCancelado', (data) {
      if (data['status'] == 'success') {
        if (mounted) {
          Navigator.pop(context);
          appRouters.pushReplacement("/estatusPedidos");
        }
      } else {
        showErrorFlushbar(data['msg'], true, context);
      }
    });
  }

  void showErrorFlushbar(String message, bool isError, BuildContext context) {
    if (!mounted) return;
    Flushbar(
      message: message,
      duration: const Duration(seconds: 3),
      backgroundColor: isError
          ? const Color.fromRGBO(205, 63, 63, 1)
          : const Color.fromRGBO(51, 96, 137, 1),
      messageColor: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      margin: const EdgeInsets.all(16.0),
      flushbarPosition: FlushbarPosition.TOP,
      icon: const Icon(
        Icons.error,
        color: Colors.white,
      ),
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('Detalles de pedidos'),
        actions: [
          CuotaUser(), // Uso del widget CuotaUser
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              '${widget.nombreLocal}',
              style: TextStyle(fontSize: 30),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 16.0),
                    const Text('Monto:', style: TextStyle(fontSize: 18.0)),
                    const SizedBox(height: 8.0),
                    Text("\$${widget.precio}",
                        style: const TextStyle(fontSize: 16.0)),
                  ],
                ),
                Column(
                  children: [
                    const SizedBox(height: 16.0),
                    const Text('Descripción:',
                        style: TextStyle(fontSize: 18.0)),
                    const SizedBox(height: 8.0),
                    Text(widget.descripcion,
                        style: const TextStyle(fontSize: 16.0)),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(widget.detalle != "" ? " Detalle: ${widget.detalle}" : "",
                    style: const TextStyle(fontSize: 14.0, color: Colors.grey)),
              ],
            ),
            const SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (isLoading)
                  const Center(
                      child:
                          CircularProgressIndicator()) // Mostrar un indicador de carga mientras se carga la imagen
                else if (imagenBytes != null)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Image.memory(
                      imagenBytes!,
                      width: 200,
                    ),
                  ),
              ],
            ),
            const Text('Ubicación:', style: TextStyle(fontSize: 18.0)),
            const SizedBox(height: 8.0),
            SizedBox(
              height: 200,
              child: _buildGoogleMap(), // Construir el mapa con la ruta
            ),
            const SizedBox(height: 16.0),
            Row(
              children: <Widget>[
                const SizedBox(width: 10),
                widget.estatus == 'POR RECOLECTAR'
                    ? Expanded(
                        child: ElevatedButton(
                          onPressed: _cancelarPedido,
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.red,
                            minimumSize: const Size(double.infinity, 50),
                          ),
                          child: const Text('Cancelar este pedido'),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGoogleMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: widget.coordenadas,
        zoom: 14,
      ),
      polylines: {
        Polyline(
          polylineId: PolylineId('route'),
          points: _polylineCoordinates,
          color: Colors.blue,
          width: 5,
        ),
      },
      markers: {
        Marker(
          markerId: MarkerId('origin'),
          position: widget.coordenadasI,
          icon: _motoIcon ??
              BitmapDescriptor.defaultMarker, // Usar icono personalizado
        ),
        Marker(
          markerId: MarkerId('destination'),
          position: widget.coordenadas,
        ),
      },
      onMapCreated: (GoogleMapController controller) {
        _mapController = controller;
        _mapController?.animateCamera(
          CameraUpdate.newLatLngBounds(
            LatLngBounds(
              southwest: LatLng(
                widget.coordenadasI.latitude < widget.coordenadas.latitude
                    ? widget.coordenadasI.latitude
                    : widget.coordenadas.latitude,
                widget.coordenadasI.longitude < widget.coordenadas.longitude
                    ? widget.coordenadasI.longitude
                    : widget.coordenadas.longitude,
              ),
              northeast: LatLng(
                widget.coordenadasI.latitude > widget.coordenadas.latitude
                    ? widget.coordenadasI.latitude
                    : widget.coordenadas.latitude,
                widget.coordenadasI.longitude > widget.coordenadas.longitude
                    ? widget.coordenadasI.longitude
                    : widget.coordenadas.longitude,
              ),
            ),
            50,
          ),
        );
      },
    );
  }
}
