import 'dart:typed_data';
import 'package:another_flushbar/flushbar.dart';
import 'package:dash_delivery/bloc/location/location_bloc.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/widgets/cuota.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:http/http.dart' as http;
import 'package:maps_launcher/maps_launcher.dart';
import 'dart:convert';
import 'package:flutter/material.dart' as a;

class DetallePedidoScreenRep extends StatefulWidget {
  final int id;
  final String nombreLocal;
  final String descripcion;
  final String detalle;
  final String direccion;
  final LatLng coordenadas;
  final LatLng coordenadasI;
  final String precio;
  final String terminado;

  const DetallePedidoScreenRep({
    Key? key,
    required this.id,
    required this.nombreLocal,
    required this.descripcion,
    required this.detalle,
    required this.direccion,
    required this.coordenadas,
    required this.coordenadasI,
    required this.precio,
    required this.terminado,
  }) : super(key: key);

  @override
  _DetallePedidoScreenState createState() => _DetallePedidoScreenState();
}

class _DetallePedidoScreenState extends State<DetallePedidoScreenRep>
    with SingleTickerProviderStateMixin {
  Uint8List? imagenBytes;
  bool isLoading = true;
  bool isLoadingMap = true; // Indicador de carga del mapa
  late SocketSerDos socket;
  List<LatLng> _polylineCoordinates = []; // Coordenadas de la ruta
  GoogleMapController? _mapController;
  BitmapDescriptor? _motoIcon; // Icono personalizado
  BitmapDescriptor? _currentLocationIcon; // Icono para la ubicación actual
  LatLng? _currentLocation; // Coordenadas de la ubicación actual
  BitmapDescriptor? _selectedIcon; // Nuevo icono para el marcador del cliente
  bool isImageExpanded = false;
  AnimationController? _animationController;

  @override
  void initState() {
    super.initState();
    _loadImage();
    socket = Provider.of<SocketSerDos>(context, listen: false);
    _loadCustomIcons(); // Cargar los iconos personalizados
    _fetchRoute(); // Obtener la ruta
    BlocProvider.of<LocationBloc>(context)
        .startFollowingUser(); // Usar LocationBloc para obtener la ubicación actual

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    )..repeat(reverse: true);
  }

  Future<void> _loadCustomIcons() async {
    final motoIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pin.png',
    );

    final currentLocationIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pinM.png',
    );

    final selectedIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pinD.png',
    );

    setState(() {
      _motoIcon = motoIcon;
      _currentLocationIcon = currentLocationIcon;
      _selectedIcon = selectedIcon;
    });
  }

  Future<void> _fetchRoute() async {
    final String apiKey = 'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0';
    final String origin =
        '${widget.coordenadasI.latitude},${widget.coordenadasI.longitude}';
    final String destination =
        '${widget.coordenadas.latitude},${widget.coordenadas.longitude}';

    final url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$apiKey';

    final response = await http.get(Uri.parse(url));
    final data = json.decode(response.body);

    if (data['status'] == 'OK') {
      final points = data['routes'][0]['overview_polyline']['points'];
      setState(() {
        _polylineCoordinates = _decodePolyline(points);
        isLoadingMap = false; // Mapa listo para mostrarse
      });
    }
  }

  List<LatLng> _decodePolyline(String encoded) {
    List<LatLng> polyline = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      final latitude = lat / 1E5;
      final longitude = lng / 1E5;
      polyline.add(LatLng(latitude, longitude));
    }
    return polyline;
  }

  @override
  void dispose() {
    socket.socketS().off('verificacionPedidosPendientes');
    socket.socketS().off('pedidoTerminado');

    _animationController?.dispose();
    super.dispose();
  }

  void _loadImage() async {
    final platilloService =
        Provider.of<PlatilloService>(context, listen: false);
    final imagen = await platilloService.obtenerImagena(widget.id);
    if (mounted) {
      setState(() {
        imagenBytes = imagen;
        isLoading = false;
      });
    }
  }

  void _terminarPedido() async {
    final userAut = await AuthGoogleU().dataUser();

    if (userAut != null) {
      final uid = userAut.uid;

      socket.emit('terminarPedido', {
        'idPedido': widget.id,
        'uid': uid,
        'terminado': "terminado",
      });

      socket.socketS().off('pedidoTerminado');
      socket.socketS().on('pedidoTerminado', (data) {
        if (!mounted) return;
        if (data['status'] == 'success') {
          showErrorFlushbar('Pedido terminado con éxito.', false, context);
          if (data['ruta'] == 'BloqueadoScreen') {
            PreferencesUser().rutaPrincipal = '/BloqueadoScreen';
            appRouters.pushReplacementNamed(PreferencesUser().rutaPrincipal);
          } else {
            Future.delayed(const Duration(seconds: 1), () {
              if (mounted) {
                Navigator.pop(context, widget.id);
                Navigator.pop(
                    context, widget.id); // Devuelve el ID del pedido terminado
              }
            });
          }
        } else {
          showErrorFlushbar('Error al terminar el pedido.', true, context);
        }
      });
    } else {
      if (!mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Usuario no autenticado')),
      );
    }
  }

  void showErrorFlushbar(String message, bool isError, BuildContext context) {
    if (!mounted) return;
    Flushbar(
      message: message,
      duration: const Duration(seconds: 3),
      backgroundColor: isError
          ? const Color.fromRGBO(205, 63, 63, 1)
          : const Color.fromRGBO(51, 96, 137, 1),
      messageColor: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      margin: const EdgeInsets.all(16.0),
      flushbarPosition: FlushbarPosition.TOP,
      icon: const Icon(
        Icons.error,
        color: Colors.white,
      ),
    ).show(context);
  }

  void _openMapsOptions() {
    final latitude = widget.coordenadas.latitude;
    final longitude = widget.coordenadas.longitude;
    MapsLauncher.launchCoordinates(latitude, longitude, 'Seguir pedido');
  }

  void _openMapsOptionsI() {
    final latitude = widget.coordenadasI.latitude;
    final longitude = widget.coordenadasI.longitude;
    MapsLauncher.launchCoordinates(latitude, longitude, 'Ver restaurante');
  }

  void _expandImage() {
    setState(() {
      isImageExpanded = true; // Expandir la imagen
    });
  }

  void _closeExpandedImage() {
    setState(() {
      isImageExpanded = false; // Cerrar la imagen expandida
    });
  }

  void _showExpandedText(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            elevation: 8.0,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        title,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 16.0),
                  SingleChildScrollView(
                    child: Text(
                      content,
                      style: const TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildDetailTextWithIndicator(String detail) {
    bool isOverflowing = false;

    return LayoutBuilder(
      builder: (context, constraints) {
        final textSpan = TextSpan(
          text: detail,
          style: const TextStyle(fontSize: 16),
        );

        final textPainter = TextPainter(
          text: textSpan,
          maxLines: 2,
          textDirection: a.TextDirection.ltr,
        );

        textPainter.layout(maxWidth: constraints.maxWidth);

        if (textPainter.didExceedMaxLines) {
          isOverflowing = true;
        }

        return GestureDetector(
          onTap: () {
            if (isOverflowing) {
              _showExpandedText(
                  'Detalle', detail); // Mostrar el texto expandido
            }
          },
          child: Row(
            children: [
              Flexible(
                child: Text(
                  detail,
                  style: const TextStyle(fontSize: 16),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  List<LatLng> _polylineCoordinatesCurrentToDestination =
      []; // Coordenadas de la ruta entre currentLocation y destination

  Future<void> _fetchRouteForCurrentLocationToDestination(
      LatLng currentLocation) async {
    const String apiKey =
        'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0'; // Reemplaza con tu API Key
    final String origin =
        '${currentLocation.latitude},${currentLocation.longitude}';
    final String destination =
        '${widget.coordenadasI.latitude},${widget.coordenadasI.longitude}';

    final url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$apiKey&mode=driving';

    final response = await http.get(Uri.parse(url));
    final data = json.decode(response.body);

    if (data['status'] == 'OK') {
      final points = data['routes'][0]['overview_polyline']['points'];
      if (mounted) {
        setState(() {
          _polylineCoordinatesCurrentToDestination = _decodePolyline(points);
          isLoadingMap = false; // Mapa listo para mostrarse
        });
      }
      // setState(() {
      // _currentLocation = currentLocation;
      // });
      //     });
      //     _updateMapCamera();
      // }
    } else {
      print('Error fetching route: ${data['status']}');
    }
  }

  Future<void> _showHelpModal() async {
    TextEditingController _textController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                const Text(
                  'Selecciona una opción:',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16.0),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildHelpButton('Problema con mi moto', _textController),
                    const SizedBox(height: 8.0),
                    _buildHelpButton('Accidente', _textController),
                    const SizedBox(height: 8.0),
                    _buildHelpButton('Otra causa', _textController),
                  ],
                ),
                const SizedBox(height: 16.0),
                TextField(
                  controller: _textController,
                  maxLines: 4,
                  decoration: InputDecoration(
                    hintText: 'Dar más información',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 8.0),
                const Text(
                  'Al seleccionar una opción se liberará tu pedido y se enviará una alerta. Es posible que se comuniquen contigo para más información.',
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 16.0),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHelpButton(String label, TextEditingController controller) {
    return ElevatedButton(
      onPressed: () {
        socket.emit('mandarAlerta', {
          'idPedido': widget.id, // Agrega el id del pedido
          'motivo': label,
          'detalle': controller.text,
        });
        Navigator.of(context).pop(); // Cierra el modal
        // showErrorFlushbar('Alerta enviada con éxito.', false, context);

        Future.delayed(const Duration(seconds: 1), () {
          Navigator.pop(context); // Cerrar la pantalla de detalles
        });
      },
      child: Text(label),
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('Pedido tomado'),
        actions: [
          CuotaUser(),
        ],
      ),
      body: BlocBuilder<LocationBloc, LocationState>(
        builder: (context, state) {
          final currentLocation = state.lastKnownLocation;
          if (currentLocation != null) {
            _fetchRouteForCurrentLocationToDestination(currentLocation);
            _currentLocation = currentLocation;
          }
          return Stack(
            children: [
              if (isLoadingMap)
                Container(
                  color: Colors.white,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              if (!isLoadingMap)
                Positioned.fill(
                  child: _buildGoogleMap(),
                ),
              Positioned(
                top: 16.0,
                left: 16.0,
                right: 16.0,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  elevation: 8.0,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 1, // Ocupa la mitad izquierda
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Negocio: ${widget.nombreLocal}',
                                style: const TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 8.0),
                              Text('Descripción:',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                              _buildDetailTextWithIndicator(widget.descripcion),
                              const SizedBox(height: 8.0),
                              Text('Monto total: \$${widget.precio}',
                                  style: const TextStyle(fontSize: 16)),
                            ],
                          ),
                        ),
                        const SizedBox(
                            width: 16.0), // Espacio entre las columnas
                        Expanded(
                          flex: 1, // Ocupa la mitad derecha
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Detalle:',
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              _buildDetailTextWithIndicator(widget.detalle),
                              const SizedBox(height: 16.0),
                              if (imagenBytes != null)
                                GestureDetector(
                                  onTap: _expandImage,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(16.0),
                                    child: Image.memory(
                                      imagenBytes!,
                                      width: double.infinity,
                                      height: 100,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              if (isLoading)
                const Center(
                  child: CircularProgressIndicator(),
                ),
              if (isImageExpanded) _buildExpandedImage(),
              Positioned(
                bottom: 16.0,
                left: 16.0,
                right: 16.0,
                child: Column(
                  children: [
                    if (widget.terminado != 'ENTREGADO')
                      ElevatedButton.icon(
                        onPressed: _openMapsOptionsI,
                        icon: Icon(Icons.restaurant),
                        label: const Text('Ver ubicación del restaurante'),
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white,
                          backgroundColor: Colors.amber[700],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                    const SizedBox(height: 8.0),
                    if (widget.terminado != 'ENTREGADO')
                      ElevatedButton.icon(
                        onPressed: _openMapsOptions,
                        icon: Icon(Icons.map),
                        label: const Text('Ir a domicilio del cliente'),
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white,
                          backgroundColor: Colors.amber[700],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                    const SizedBox(height: 8.0),
                    if (widget.terminado != 'ENTREGADO')
                      ElevatedButton.icon(
                        onPressed: _terminarPedido,
                        icon: Icon(Icons.done_all),
                        label: const Text('Finalizar pedido'),
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white,
                          backgroundColor: Colors.red[700],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
              // Añadir el botón circular debajo del card
              if (widget.terminado != 'ENTREGADO')
                Positioned(
                  bottom: 10.0,
                  right: 16.0,
                  child: ScaleTransition(
                    scale: Tween(begin: 0.9, end: 1.1)
                        .animate(_animationController!),
                    child: FloatingActionButton(
                      onPressed: _showHelpModal,
                      backgroundColor: Colors.yellow,
                      child: Icon(
                        Icons.warning_amber_rounded,
                        size: 40,
                      ),
                    ),
                  ),
                ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildGoogleMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: widget.coordenadas,
        zoom: 14,
      ),
      polylines: {
        Polyline(
          polylineId: const PolylineId('route'),
          points: _polylineCoordinates,
          color: Colors.blue,
          width: 5,
        ),
        Polyline(
          polylineId: const PolylineId('route_current_to_destination'),
          points:
              _polylineCoordinatesCurrentToDestination, // Ruta entre currentLocation y destination
          color: Colors.orange, // Color naranja para la nueva ruta
          width: 5,
        ),
      },
      markers: {
        if (_currentLocation != null)
          Marker(
            markerId: const MarkerId('currentLocation'),
            position: _currentLocation!,
            icon: _currentLocationIcon ?? BitmapDescriptor.defaultMarker,
          ),
        Marker(
          markerId: const MarkerId('origin'),
          position: widget.coordenadasI,
          icon: _motoIcon ?? BitmapDescriptor.defaultMarker,
        ),
        Marker(
          markerId: const MarkerId('destination'),
          position: widget.coordenadas,
          icon: _selectedIcon ?? BitmapDescriptor.defaultMarker,
        ),
      },
      onMapCreated: (GoogleMapController controller) {
        _mapController = controller;
        _updateMapCamera();
      },
    );
  }

  void _updateMapCamera() {
    if (_mapController == null || _currentLocation == null) return;

    LatLngBounds bounds;

    final allPoints = [
      _currentLocation!,
      widget.coordenadas,
      widget.coordenadasI
    ];

    double southWestLat = allPoints[0].latitude;
    double southWestLng = allPoints[0].longitude;
    double northEastLat = allPoints[0].latitude;
    double northEastLng = allPoints[0].longitude;

    for (LatLng point in allPoints) {
      if (point.latitude < southWestLat) southWestLat = point.latitude;
      if (point.longitude < southWestLng) southWestLng = point.longitude;
      if (point.latitude > northEastLat) northEastLat = point.latitude;
      if (point.longitude > northEastLng) northEastLng = point.longitude;
    }

    bounds = LatLngBounds(
      southwest: LatLng(southWestLat, southWestLng),
      northeast: LatLng(northEastLat, northEastLng),
    );

    _mapController!.animateCamera(
      CameraUpdate.newLatLngBounds(bounds, 50),
    );
  }

  Widget _buildExpandedImage() {
    return Center(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        elevation: 8.0,
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Image.memory(
                  imagenBytes!,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              top: 8.0,
              right: 8.0,
              child: IconButton(
                icon: const Icon(Icons.close, color: Colors.white),
                onPressed: _closeExpandedImage,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
