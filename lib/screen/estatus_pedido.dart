import 'dart:convert';
import 'package:dash_delivery/screen/detalle_pedido_screen_negocio.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/widgets/sidenav.dart';
import 'package:provider/provider.dart';

class EstatusPedido extends StatefulWidget {
  const EstatusPedido({super.key});

  @override
  _EstatusPedidoState createState() => _EstatusPedidoState();
}

class _EstatusPedidoState extends State<EstatusPedido> {
  late Future<List<dynamic>> _pedidosFuture;

  @override
  void initState() {
    super.initState();
    _pedidosFuture =
        Provider.of<PlatilloService>(context, listen: false).obtenerPedidos();
  }

  Future<void> _recargarPedidos() async {
    setState(() {
      _pedidosFuture =
          Provider.of<PlatilloService>(context, listen: false).obtenerPedidos();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Ver estatus de pedidos'),
      // ),
      // drawer: sidenav(context),
      body: RefreshIndicator(
        onRefresh: _recargarPedidos,
        child: FutureBuilder<List<dynamic>>(
          future: _pedidosFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
              return Center(child: Text('No se encontraron pedidos'));
            }

            final pedidos = snapshot.data!;

            return ListView.builder(
              itemCount: pedidos.length,
              itemBuilder: (context, index) {
                final pedido = pedidos[index];
                final coordenadas = _parseCoordinates(pedido['coordenadas']);
                final coordenadasI =
                    _parseCoordinates(pedido['coordenadas_inicio']);

                return _buildPedidoCard(
                  context,
                  pedido['id'],
                  pedido['nombre_local'],
                  pedido['descripcion'],
                  pedido['detalle'],
                  pedido['precio'],
                  pedido['ubicacion'],
                  pedido['direccion'],
                  pedido['estatus'],
                  coordenadas,
                  coordenadasI,
                );
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildPedidoCard(
    BuildContext context,
    int id,
    String nombreLocal,
    String descripcion,
    String detalle,
    String precio,
    String ubicacion,
    String direccion,
    String estatus,
    LatLng coordenadas,
    LatLng coordenadasI,
  ) {
    return Card(
      margin: const EdgeInsets.all(8.0),
      child: ListTile(
        title: Text(
          nombreLocal,
          textAlign: TextAlign.center,
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('De: $ubicacion'),
            Text('A: $direccion'),
            Text('Monto: \$${precio}'),
            Text('Estatus: $estatus'),
          ],
        ),
        onTap: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DetallePedidoScreenNegocio(
                id: id,
                nombreLocal: nombreLocal,
                descripcion: descripcion,
                detalle: detalle,
                direccion: direccion,
                coordenadas: coordenadas,
                coordenadasI: coordenadasI,
                precio: precio,
                estatus: estatus,
              ),
            ),
          );

          if (result != null) {
            _recargarPedidos();
          }
        },
      ),
    );
  }

  LatLng _parseCoordinates(String coordenadas) {
    final parts = coordenadas.split(',');
    final lat = double.parse(parts[0].trim());
    final lng = double.parse(parts[1].trim());
    return LatLng(lat, lng);
  }
}
