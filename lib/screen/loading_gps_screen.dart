import 'package:dash_delivery/bloc/gps/gps_bloc.dart';
import 'package:dash_delivery/screen/gp_access_screen.dart';
import 'package:dash_delivery/screen/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoadongGpsScreen extends StatelessWidget {
  const LoadongGpsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<GpsBloc, GpsState>(
        builder: (context, state) {
          return state.isAllGranted
              ? const LoginScreen()
              : const GpsAccesScreen();
        },
      ),
    );
  }
}
