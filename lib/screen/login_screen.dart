import 'package:dash_delivery/screen/loading_screen.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/bloc/notification/notification_bloc.dart';
import 'package:dash_delivery/services/register_service.dart';
import 'package:dash_delivery/widgets/flush_bar.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isLoading = false;
  final loginService = LoginService();

  @override
  Widget build(BuildContext context) {
    // context.read<NotificationBloc>().requestPermission();

    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 200,
                  ),
                  Image.asset(
                    'assets/pickers.png',
                    height: 300,
                  ),
                  const SizedBox(height: 40),
                  ElevatedButton.icon(
                    onPressed:
                        _isLoading ? null : () => _handleGoogleSignIn(context),
                    icon: const Icon(
                      Icons.g_mobiledata,
                      color: Colors.white,
                    ),
                    label: const Text(
                      'Iniciar sesión con Google',
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      minimumSize: const Size(double.infinity, 50),
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (_isLoading)
            Container(
              color: Colors.black.withOpacity(0.5),
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }

  Future<void> _handleGoogleSignIn(BuildContext context) async {
    // Navegar a la pantalla de carga
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const LoadingScreen()));

    try {
      final user = await AuthGoogleU().loginGoogle();
      if (user != null) {
        final userAut = AuthGoogleU().dataUser();
        final result = await loginService.login(
            userAut!.email ?? '', userAut.uid, context);

        if (result == "validar") {
          showErrorFlushbar("Iniciando sesión", false, context);
          if (!PreferencesUser().access) {
            PreferencesUser().rutaPrincipal = '/accessGPS';
            appRouters.pushReplacementNamed('/accessGPS');
          } else {
            PreferencesUser().rutaPrincipal = '/dashboard_eleccion';
            appRouters.pushReplacementNamed('/dashboard_eleccion');
          }
        } else if (result == "restaurante") {
          if (!PreferencesUser().access) {
            PreferencesUser().rutaPrincipal = '/accessGPS';
            appRouters.pushReplacementNamed('/accessGPS');
          } else {
            PreferencesUser().rutaPrincipal = '/dashboard_negocio';
            appRouters.pushReplacementNamed('/dashboard_negocio');
          }
        } else if (result == "repartidor") {
          if (!PreferencesUser().access) {
            PreferencesUser().rutaPrincipal = '/accessGPS';
            appRouters.pushReplacementNamed('/accessGPS');
          } else {
            PreferencesUser().rutaPrincipal = '/dashboard_repartidor';
            appRouters.pushReplacementNamed('/dashboard_repartidor');
          }
        } else if (result == "admin") {
          if (!PreferencesUser().access) {
            PreferencesUser().rutaPrincipal = '/accessGPS';
            appRouters.pushReplacementNamed('/accessGPS');
          } else {
            PreferencesUser().rutaPrincipal = '/admin';
            appRouters.pushReplacementNamed('/admin');
          }
        } else if (result == "PendienteScreen") {
          PreferencesUser().rutaPrincipal = '/PendienteScreen';

          appRouters.pushReplacementNamed('/PendienteScreen');
        } else if (result == "BloqueadoScreen") {
          PreferencesUser().rutaPrincipal = '/BloqueadoScreen';
          appRouters.pushReplacementNamed('/BloqueadoScreen');
        } else {
          // Si no cumple con ninguna de las validaciones anteriores
          await FirebaseAuth.instance.signOut();
          await AuthGoogleU().logoutGoogle();
          Future.delayed(const Duration(milliseconds: 500), () {
            // Regresar a la pantalla anterior (quitar la pantalla de carga)
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
            showErrorFlushbar(result ?? "Ocurrió un error", true, context);
          });
        }
      } else {
        // Si el usuario cancela la selección de la cuenta de Google
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop(); // Regresar a la pantalla de login
        }
      }
    } on FirebaseAuthException catch (error) {
      await FirebaseAuth.instance.signOut();
      await AuthGoogleU().logoutGoogle();
      Future.delayed(const Duration(milliseconds: 500), () {
        // Regresar a la pantalla anterior (quitar la pantalla de carga)
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        showErrorFlushbar('${error.message}', false, context);
      });
    } catch (e) {
      print(
          "******************************************************************");
      print(e);
      await FirebaseAuth.instance.signOut();
      await AuthGoogleU().logoutGoogle();
      Future.delayed(const Duration(milliseconds: 500), () {
        // Regresar a la pantalla anterior (quitar la pantalla de carga)
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
        showErrorFlushbar('Fallo al iniciar sesión', true, context);
      });
    }
  }
}
