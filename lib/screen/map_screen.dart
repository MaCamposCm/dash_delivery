import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoding/geocoding.dart';

class MapScreen extends StatefulWidget {
  final LatLng? initialLocation;

  MapScreen({required this.initialLocation});

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  late LatLng _selectedLocation;
  GoogleMapController? _controller;

  @override
  void initState() {
    super.initState();
    _selectedLocation = widget.initialLocation ??
        LatLng(37.7749, -122.4194); // Coordenadas por defecto
  }

  Future<String> _getAddressFromLatLng(LatLng location) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(
      location.latitude,
      location.longitude,
    );

    Placemark place = placemarks[0];
    return '${place.street}, ${place.locality}, ${place.postalCode}, ${place.country}';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Seleccionar dirección en el mapa'),
        actions: <Widget>[
          TextButton(
            onPressed: () async {
              final address = await _getAddressFromLatLng(_selectedLocation);
              Navigator.pop(
                  context, {'location': _selectedLocation, 'address': address});
            },
            child: const Text(
              'Finalizar',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: GoogleMap(
              initialCameraPosition: CameraPosition(
                target: _selectedLocation,
                zoom: 14,
              ),
              onMapCreated: (GoogleMapController controller) {
                _controller = controller;
                if (_selectedLocation != widget.initialLocation) {
                  _controller?.animateCamera(
                      CameraUpdate.newLatLng(_selectedLocation));
                }
              },
              onTap: (LatLng location) {
                setState(() {
                  _selectedLocation = location;
                });
              },
              markers: <Marker>{
                Marker(
                  markerId: MarkerId('selectedLocation'),
                  position: _selectedLocation,
                ),
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ElevatedButton(
              onPressed: () async {
                final address = await _getAddressFromLatLng(_selectedLocation);
                Navigator.pop(context,
                    {'location': _selectedLocation, 'address': address});
              },
              style: ElevatedButton.styleFrom(
                minimumSize:
                    const Size(double.infinity, 50), // Tamaño del botón
              ),
              child: const Text('Confirmar selección'),
            ),
          ),
        ],
      ),
    );
  }
}
