import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_webservice/places.dart' as loc;
import 'package:geolocator/geolocator.dart';

class MapaScreenNegocio extends StatefulWidget {
  final Function(String, LatLng) onLocationSelected;
  final String? initialAddress;

  const MapaScreenNegocio({
    super.key,
    required this.onLocationSelected,
    this.initialAddress,
  });

  @override
  MapaScreenState createState() => MapaScreenState();
}

class MapaScreenState extends State<MapaScreenNegocio> {
  final Completer<GoogleMapController> _controller = Completer();
  final TextEditingController _searchController = TextEditingController();
  Marker? selectedMarker;
  final String googleApiKey =
      'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0'; // Reemplaza con tu API key
  List<Map<String, String>> suggestions = [];
  String? selectedAddress;
  LatLng? initialPosition;
  LatLng? selectedLatLng;

  @override
  void initState() {
    super.initState();
    if (widget.initialAddress != null) {
      _searchController.text = widget.initialAddress!;
      _getCoordinatesFromAddress(widget.initialAddress!);
    } else {
      _getCurrentLocation();
    }
  }

  Future<void> _getCoordinatesFromAddress(String address) async {
    try {
      List<Location> locations = await locationFromAddress(address);
      if (locations.isNotEmpty) {
        LatLng latLng =
            LatLng(locations.first.latitude, locations.first.longitude);
        setState(() {
          initialPosition = latLng;
          selectedLatLng = latLng;
          selectedAddress = address;
          selectedMarker = Marker(
            markerId: MarkerId('selectedLocation'),
            position: latLng,
          );
        });
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newLatLng(latLng));
      }
    } catch (e) {}
  }

  Future<void> _getCurrentLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      LatLng currentLatLng = LatLng(position.latitude, position.longitude);
      setState(() {
        initialPosition = currentLatLng;
      });
      _updateSelectedLocation(currentLatLng);
    } catch (e) {}
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    if (initialPosition != null) {
      controller.animateCamera(CameraUpdate.newLatLng(initialPosition!));
    }
  }

  Future<void> _searchPlace(String placeId) async {
    try {
      final places = loc.GoogleMapsPlaces(apiKey: googleApiKey);
      final response = await places.getDetailsByPlaceId(placeId);

      if (response.status == "OK") {
        final result = response.result;
        final location = result.geometry!.location;
        final latLng = LatLng(location.lat, location.lng);
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newLatLng(latLng));
        _updateSelectedLocation(latLng);
      } else {}
    } catch (e) {}
  }

  Future<void> _getSuggestions(String input) async {
    try {
      final places = loc.GoogleMapsPlaces(apiKey: googleApiKey);
      final response = await places.autocomplete(input);

      if (response.status == "OK") {
        setState(() {
          suggestions = response.predictions
              .map<Map<String, String>>((p) => {
                    'description': '${p.description}',
                    'place_id': '${p.placeId}'
                  })
              .toList();
        });
      } else {}
    } catch (e) {}
  }

  Future<void> _updateSelectedLocation(LatLng latLng) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(latLng.latitude, latLng.longitude);
      if (mounted) {
        setState(() {
          selectedMarker = Marker(
            markerId: MarkerId('selectedLocation'),
            position: latLng,
          );
          selectedLatLng = latLng;
          suggestions = [];
        });
        if (placemarks.isNotEmpty) {
          Placemark place = placemarks.first;
          selectedAddress =
              '${place.street}, ${place.locality}, ${place.subLocality}, ${place.administrativeArea} ';
          _searchController.text = selectedAddress!;
        }
      }
    } catch (e) {}
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Seleccionar Dirección'),
      ),
      body: initialPosition == null
          ? Center(child: CircularProgressIndicator())
          : Stack(
              children: [
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: initialPosition!,
                    zoom: 15,
                  ),
                  markers: selectedMarker != null ? {selectedMarker!} : {},
                  onTap: (latLng) {
                    _updateSelectedLocation(latLng);
                  },
                  myLocationEnabled: true,
                ),
                Positioned(
                  top: 10,
                  left: 10,
                  right: 10,
                  child: Column(
                    children: [
                      TextField(
                        controller: _searchController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Buscar lugar...',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () {
                              _searchController.clear();
                              setState(() {
                                suggestions = [];
                                selectedMarker = null;
                              });
                            },
                          ),
                        ),
                        onChanged: (value) {
                          if (value.isNotEmpty) {
                            _getSuggestions(value);
                          } else {
                            setState(() {
                              suggestions = [];
                            });
                          }
                        },
                      ),
                      if (suggestions.isNotEmpty)
                        Container(
                          height: 200,
                          color: Colors.white,
                          child: ListView.builder(
                            itemCount: suggestions.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                title: Text(suggestions[index]['description']!),
                                onTap: () {
                                  _searchController.text =
                                      suggestions[index]['description']!;
                                  _searchPlace(suggestions[index]['place_id']!);
                                },
                              );
                            },
                          ),
                        ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 20,
                  left: 20,
                  right: 20,
                  child: ElevatedButton(
                    onPressed: () {
                      if (selectedMarker != null && selectedLatLng != null) {
                        widget.onLocationSelected(
                          selectedAddress!,
                          selectedLatLng!,
                        );
                        Navigator.pop(context);
                      }
                    },
                    child: Text('Guardar dirección'),
                  ),
                ),
                if (selectedLatLng != null)
                  Positioned(
                    bottom: 80,
                    left: 20,
                    right: 20,
                    child: Container(
                      padding: const EdgeInsets.all(12.0),
                      // Aquí podrías mostrar las coordenadas o cualquier otro detalle
                      // sobre la ubicación seleccionada si es necesario.
                    ),
                  ),
              ],
            ),
    );
  }
}
