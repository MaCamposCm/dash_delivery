import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/screen/detalle_pedido_screen_rep.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/widgets/cuota.dart';
import 'package:dash_delivery/widgets/datosExtra.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MisPedidos extends StatefulWidget {
  const MisPedidos({super.key});

  @override
  State<MisPedidos> createState() => _MisPedidosState();
}

class _MisPedidosState extends State<MisPedidos> {
  List<dynamic> _pedidosList = []; // Lista para almacenar los pedidos
  late SocketSerDos socket;
  bool _isLoading = true; // Variable para manejar el estado de carga
  int _selectedIndex = 1; // Para controlar la selección del BottomNavigationBar

  @override
  void initState() {
    super.initState();
    _fetchPedidos();
    socket = Provider.of<SocketSerDos>(context, listen: false);

    socket.socketS().on('alertaEnviada', (data) {
      if (data['status'] == 'success') {
        // Future.delayed(const Duration(seconds: 2), () {
        // Navigator.pop(context); // Cerrar la pantalla de detalles
        _fetchPedidos();
        // });
      }
    });

    // Obtener la instancia de Socket
  }

  @override
  void dispose() {
    // Limpiar la suscripción al evento 'pedidoActualizado'
    super.dispose();
    socket.socketS().off(
        'alertaEnviada'); // Desactivar el listener cuando se deshace la pantalla
  }

  Future<void> _fetchPedidos() async {
    try {
      final pedidos = await Provider.of<PlatilloService>(context, listen: false)
          .obtenerPedidosRealizados(PreferencesUser().token);
      setState(() {
        _pedidosList = pedidos; // Almacena los pedidos en la lista de estado
        _isLoading = false; // Dejar de mostrar el indicador de carga
        if (_pedidosList.isEmpty) {
          print("No se han encontrado pedidos.");
        }
      });
    } catch (error) {
      // Manejar error si es necesario
      setState(() {
        _isLoading =
            false; // Dejar de mostrar el indicador de carga incluso en caso de error
      });
    }
  }

  Future<void> _recargarPedidos() async {
    if (mounted) {
      setState(() {
        _isLoading =
            true; // Mostrar el indicador de carga mientras se recargan los pedidos
      });
    }
    await _fetchPedidos(); // Recarga los pedidos
  }

  void _eliminarPedidoDeLaLista(int id) {
    setState(() {
      _pedidosList =
          _pedidosList.where((pedido) => pedido['id'] != id).toList();
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    // Acciones según el índice seleccionado
    switch (index) {
      case 0:
        appRouters.pushReplacementNamed('/dashboard_repartidor');
        break;
      case 1:
        _recargarPedidos();
        break;
      case 2:
        FirebaseAuth.instance.signOut();
        AuthGoogleU().logoutGoogle();
        PreferencesUser().rutaPrincipal = '/login';
        appRouters.pushReplacementNamed('/dashboard_repartidor');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          RefreshIndicator(
            onRefresh: _recargarPedidos,
            child: _isLoading
                ? const Center(
                    child:
                        CircularProgressIndicator()) // Mostrar un indicador mientras se carga
                : _pedidosList.isEmpty
                    ? _buildNoPedidosMessage()
                    : ListView.builder(
                        itemCount: _pedidosList.length,
                        itemBuilder: (context, index) {
                          final pedido = _pedidosList[index];
                          final coordenadas =
                              _parseCoordinates(pedido['coordenadas']);
                          final coordenadasI =
                              _parseCoordinates(pedido['coordenadas_inicio']);
                          final registro = _parseDateTime(pedido['registro']);
                          return _buildPedidoCard(
                              context,
                              pedido['id'],
                              pedido['nombre_local'],
                              pedido['descripcion'],
                              pedido['detalle'],
                              pedido['precio'],
                              pedido['ubicacion'],
                              pedido['direccion'],
                              coordenadas,
                              coordenadasI,
                              registro,
                              pedido['terminado']);
                        },
                      ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoPedidosMessage() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'No se han encontrado pedidos actualmente.',
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: _recargarPedidos,
            child: const Text('Recargar'),
          ),
        ],
      ),
    );
  }

  Widget _buildPedidoCard(
      BuildContext context,
      int id,
      String nombreLocal,
      String descripcion,
      String detalle,
      String precio,
      String ubicacion,
      String direccion,
      LatLng coordenadas,
      LatLng coordenadasI,
      String registro,
      String terminado) {
    Color borderColor;

    // Asignar color del borde basado en el estado del pedido
    if (terminado == "ENTREGADO") {
      borderColor = Colors.green.shade200; // Verde oscuro para borde
    } else if (terminado == "RECOLECTADO") {
      borderColor = Colors.amber.shade200; // Ámbar oscuro para borde
    } else {
      borderColor = Colors.grey.shade300; // Gris para estado por defecto
    }
    return Card(
      margin: const EdgeInsets.all(8.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0), // Bordes redondeados
        side: BorderSide(color: borderColor, width: 5.0), // Color del borde
      ),
      child: ListTile(
        title: Text(
          nombreLocal,
          textAlign: TextAlign.center,
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('De: $ubicacion'),
            Text('A: $direccion'),
            Text('Monto: \$${precio}'),
            Text('Registrado el: $registro'),
            Text('Estatus: $terminado'),
          ],
        ),
        onTap: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DetallePedidoScreenRep(
                  id: id,
                  nombreLocal: nombreLocal,
                  descripcion: descripcion,
                  detalle: detalle,
                  direccion: direccion,
                  coordenadas: coordenadas,
                  coordenadasI: coordenadasI,
                  precio: precio,
                  terminado: terminado),
            ),
          );

          if (result != null && result is int) {
            _eliminarPedidoDeLaLista(result);
          }
        },
      ),
    );
  }

  LatLng _parseCoordinates(String coordenadas) {
    final parts = coordenadas.split(',');
    final lat = double.parse(parts[0].trim());
    final lng = double.parse(parts[1].trim());
    return LatLng(lat, lng);
  }

  String _parseDateTime(String dateTimeString) {
    try {
      final dateTime = DateTime.parse(dateTimeString);
      final formatter = DateFormat('dd/MM/yyyy HH:mm');
      return formatter.format(dateTime);
    } catch (e) {
      return 'Fecha no válida';
    }
  }
}
