import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/verificacionService.dart';
import 'package:dash_delivery/widgets/sidenav.dart';
import 'package:dash_delivery/widgets/sidenavB.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class PendienteScreen extends StatefulWidget {
  @override
  _PendienteScreenState createState() => _PendienteScreenState();
}

class _PendienteScreenState extends State<PendienteScreen> {
  final verificacionService = VerificacionService();

  @override
  void initState() {
    super.initState();
    checkForUpdatesAndShowBottomSheet();
  }

  Future<void> checkForUpdatesAndShowBottomSheet() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String currentVersion = packageInfo.version;

    // final response = await http.get(Uri.parse('http://62.72.3.235:3002/ping'));
    // if (response.statusCode == 200) {
    //   String latestVersion = json.decode(response.body);

    //   if (currentVersion != latestVersion) {
    //     showUpdateBanner();
    //   }
    // }
  }

  void showUpdateBanner() {
    final materialBanner = MaterialBanner(
      content: Text('Se encuentra una nueva versión de esta aplicación',
          style: TextStyle(
            color: Colors.white,
          )),
      actions: [
        TextButton(
          onPressed: () async {
            const url =
                'https://play.google.com/store/apps/details?id=com.nabiaa.pickers';
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              throw 'Could not launch $url';
            }
          },
          child: Text(
            '¡Obténla!',
            style: TextStyle(color: Colors.white),
          ),
        ),
        TextButton(
          onPressed: () {
            ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
          },
          child: Text(
            'Recordar más tarde',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
      backgroundColor: Colors.blue,
    );

    ScaffoldMessenger.of(context).showMaterialBanner(materialBanner);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pendiente de verificación'),
      ),
      drawer: sidenavB(context),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/pickers.png',
              height: 200,
            ),
            SizedBox(height: 50), // Espacio entre la imagen y el texto
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'Pendiente de verificación, favor de esperar',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey,
                ),
              ),
            ),
            SizedBox(height: 20), // Espacio entre el texto y los botones
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: ElevatedButton(
                      onPressed: () async {
                        // Aquí puedes agregar la lógica de verificación
                        final userAut = AuthGoogleU().dataUser();
                        final dato = await verificacionService.verificarUsuario(
                            userAut!.uid, context);
                        print(dato);

                        if (dato == 'aceptado') {
                          appRouters.pushReplacementNamed(
                              PreferencesUser().rutaPrincipal);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.black,
                        backgroundColor:
                            Colors.yellow, // Color del texto del botón
                        minimumSize: Size(150, 50), // Tamaño mínimo del botón
                      ),
                      child: Text('Verificar acceso'),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: ElevatedButton(
                      onPressed: () {
                        // Aquí puedes agregar la lógica de verificación
                        final userAut = AuthGoogleU().dataUser();
                        verificacionService.verificar(context, userAut!.uid);
                      },
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.black,
                        backgroundColor: Color.fromARGB(
                            255, 59, 186, 255), // Color del texto del botón
                        minimumSize: Size(150, 50), // Tamaño mínimo del botón
                      ),
                      child: Text('Solicitar verificación'),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
                height:
                    20), // Espacio entre los botones y el final de la pantalla
          ],
        ),
      ),
    );
  }
}
