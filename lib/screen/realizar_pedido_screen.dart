import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/widgets/flush_bar.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/platillo_service.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class RealizarPedidoScreen extends StatefulWidget {
  final String address;
  final LatLng location;

  const RealizarPedidoScreen({
    super.key,
    required this.address,
    required this.location,
  });

  @override
  RealizarPedidoScreenState createState() => RealizarPedidoScreenState();
}

class RealizarPedidoScreenState extends State<RealizarPedidoScreen> {
  String _distanceInKm = '';

  bool _isSubmitting = false;
  File? _selectedImage;
  late TextEditingController _generalDescriptionController;
  late TextEditingController _detailedDescriptionController;
  late TextEditingController _priceController;
  Future<File?>? _imageFuture;
  String _calculatedPrice = ''; // Nuevo precio calculado basado en la distancia

  late SocketSerDos _socket; // Instancia del servicio de socket
  LatLng? _restaurantCoordinates; // Coordenadas del restaurante
  List<LatLng> _polylineCoordinates = []; // Coordenadas de la ruta
  GoogleMapController? _mapController;
  BitmapDescriptor? _motoIcon; // Icono personalizado
  BitmapDescriptor? _selectedIcon; // Nuevo icono para el marcador del cliente

  Map<String, dynamic>? _routeData; // Almacenar los datos de la ruta

  @override
  void initState() {
    super.initState();
    _generalDescriptionController = TextEditingController();
    _detailedDescriptionController = TextEditingController();
    _priceController = TextEditingController();
    _priceController.addListener(_calculateFinalPrice);

    _socket = Provider.of<SocketSerDos>(context, listen: false);
    _loadCustomIcon(); // Cargar el icono personalizado
    _getRestaurantCoordinatesAndRoute(); // Obtén las coordenadas del restaurante y la ruta
  }

  @override
  void dispose() {
    // Cancelar el listener del socket
    _socket.socketS().off('respuestaCoordenadasRestaurante');
    _socket.socketS().off('pedidoCreado');

    // Liberar los controladores de texto
    _generalDescriptionController.dispose();
    _detailedDescriptionController.dispose();
    _priceController.dispose();

    super.dispose();
  }

  Future<void> _loadCustomIcon() async {
    final BitmapDescriptor motoIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)), // Tamaño del icono
      'assets/pin.png', // Asegúrate de tener la imagen de la moto en la carpeta de assets
    );
    final BitmapDescriptor selectedIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(48, 48)),
      'assets/pinD.png',
    );

    if (mounted) {
      setState(() {
        _motoIcon = motoIcon;
        _selectedIcon = selectedIcon;
      });
    }
  }

  Future<void> _getRestaurantCoordinatesAndRoute() async {
    _restaurantCoordinates = stringToLatLng(PreferencesUser().coordenadas);

    await _fetchRoute(); // Obtén la ruta inmediatamente
    _updateMapCamera(); // Actualiza la cámara del mapa
  }

  LatLng stringToLatLng(String coordinates) {
    // Dividir la cadena por la coma y eliminar los espacios en blanco
    List<String> parts =
        coordinates.split(',').map((part) => part.trim()).toList();

    // Convertir las partes en valores de tipo double
    double latitude = double.parse(parts[0]);
    double longitude = double.parse(parts[1]);

    // Crear y retornar el objeto LatLng
    return LatLng(latitude, longitude);
  }

  Future<void> _fetchRoute() async {
    if (_restaurantCoordinates == null) return;

    const String apiKey = 'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0';
    final String origin =
        '${_restaurantCoordinates!.latitude},${_restaurantCoordinates!.longitude}';
    final String destination =
        '${widget.location.latitude},${widget.location.longitude}';
    final url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$apiKey';

    final response = await http.get(Uri.parse(url));
    final data = json.decode(response.body);

    if (data['status'] == 'OK') {
      _routeData = data; // Almacenar los datos de la ruta
      final points = data['routes'][0]['overview_polyline']['points'];
      if (mounted) {
        setState(() {
          _polylineCoordinates = _decodePolyline(points);
          _distanceInKm =
              '${_calculateDistanceInKmFromStoredData().toStringAsFixed(2)} km'; // Actualizar distancia
        });
      }
    } else {}
  }

  List<LatLng> _decodePolyline(String encoded) {
    List<LatLng> polyline = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      final latitude = lat / 1E5;
      final longitude = lng / 1E5;
      polyline.add(LatLng(latitude, longitude));
    }
    return polyline;
  }

  Future<void> _calculateFinalPrice() async {
    if (_priceController.text.isEmpty || _restaurantCoordinates == null) {
      return;
    }

    double basePrice = double.tryParse(_priceController.text) ?? 0.0;
    double distanceInKm = _calculateDistanceInKmFromStoredData();
    double additionalCharge = _getAdditionalCharge(distanceInKm);
    double finalPrice = basePrice + additionalCharge;

    if (mounted) {
      setState(() {
        _calculatedPrice = '${finalPrice.toStringAsFixed(2)}';
      });
    }
  }

  double _calculateDistanceInKmFromStoredData() {
    if (_routeData == null) {
      return 0.0;
    }

    final route = _routeData!['routes'][0];
    final distance = route['legs'][0]['distance']['value'];
    return distance / 1000; // Convertir metros a kilómetros
  }

  double _getAdditionalCharge(double distanceInKm) {
    print(distanceInKm);
    if (distanceInKm <= 3.0) {
      return 55;
    } else if (distanceInKm <= 4.5) {
      return 65;
    } else if (distanceInKm <= 6.0) {
      return 75;
    } else if (distanceInKm <= 8.0) {
      return 85;
    } else if (distanceInKm <= 10.0) {
      return 95;
    } else if (distanceInKm <= 12.0) {
      return 105;
    } else {
      return 110 + (((distanceInKm - 12.0) / 2).ceil() * 10);
    }
  }

  Future<void> _pickImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.camera);

    if (pickedFile != null) {
      setState(() {
        _imageFuture = _resizeImage(File(pickedFile.path));
      });
    }
  }

  String _generateFileName(String address) {
    final DateTime now = DateTime.now();
    final String formattedDate = now.toIso8601String();
    final dateSanitized =
        formattedDate.replaceAll(RegExp(r'[^\w\s]'), '').replaceAll(' ', '_');
    return '$dateSanitized.png';
  }

  Future<File?> _resizeImage(File imageFile) async {
    final bytes = await imageFile.readAsBytes();
    final image = img.decodeImage(bytes);
    if (image != null) {
      const maxWidth = 800;
      const maxHeight = 800;
      final resizedImage =
          img.copyResize(image, width: maxWidth, height: maxHeight);
      final fileName = _generateFileName(widget.address);
      final tempDir = Directory.systemTemp;
      final resizedFile = File('${tempDir.path}/$fileName')
        ..writeAsBytesSync(img.encodePng(resizedImage));
      return resizedFile;
    }
    return imageFile;
  }

  // Future<void> _finalizeOrder() async {
  //   if (_generalDescriptionController.text.isEmpty) {
  //     showErrorFlushbar("Descripción general es obligatoria", true, context);

  //     // _showAlert('Descripción general es obligatoria.');
  //     return;
  //   }

  //   if (_priceController.text.isEmpty) {
  //     showErrorFlushbar("El precio es obligatorio.", true, context);
  //     // _showAlert('El precio es obligatorio.');
  //     return;
  //   }

  //   setState(() {
  //     _isSubmitting = true;
  //   });

  //   final platilloService = PlatilloService();
  //   final userAut = await AuthGoogleU().dataUser();
  //   final fileName =
  //       _selectedImage != null ? _generateFileName(widget.address) : '';

  //   final result = await platilloService.registroPedidos(
  //     ubicacion: widget.address,
  //     coordenadas: '${widget.location.latitude}, ${widget.location.longitude}',
  //     descripcion: _generalDescriptionController.text,
  //     detalle: _detailedDescriptionController.text,
  //     precio: _calculatedPrice, // Usar el precio calculado
  //     platillo: _selectedImage,
  //     uid: userAut!.uid,
  //     nombreArchivo: fileName,
  //     coordenadasRestaurante:
  //         '${_restaurantCoordinates!.latitude}, ${_restaurantCoordinates!.longitude}',
  //   );

  //   if (mounted) {
  //     setState(() {
  //       _isSubmitting = false;
  //     });

  //     if (result['estatus'] == "Correcto") {
  //       _socket.emit('pedidoCreado');
  //       showErrorFlushbar("Se ha creado el pedido con éxito", false, context);

  //       Future.delayed(const Duration(milliseconds: 1000), () {
  //         Navigator.of(context).pop(true);
  //         Navigator.of(context).pop(true);
  //       });
  //     } else {
  //       showErrorFlushbar(result['msg'] ?? "Ocurrió un error", true, context);
  //     }
  //   }
  // }
  Future<void> _finalizeOrder() async {
    if (_generalDescriptionController.text.isEmpty) {
      showErrorFlushbar("Descripción general es obligatoria", true, context);
      return;
    }

    if (_priceController.text.isEmpty) {
      showErrorFlushbar("El precio es obligatorio.", true, context);
      return;
    }

    setState(() {
      _isSubmitting = true;
    });

    final platilloService = PlatilloService();
    final userAut = await AuthGoogleU().dataUser();
    final fileName =
        _selectedImage != null ? _generateFileName(widget.address) : '';

    // Calcula los valores de basePrice y additionalCharge
    double basePrice = double.tryParse(_priceController.text) ?? 0.0;
    double distanceInKm = _calculateDistanceInKmFromStoredData();
    double additionalCharge = _getAdditionalCharge(distanceInKm);

    final result = await platilloService.registroPedidos(
      ubicacion: widget.address,
      coordenadas: '${widget.location.latitude}, ${widget.location.longitude}',
      descripcion: _generalDescriptionController.text,
      detalle: _detailedDescriptionController.text,
      precio: _calculatedPrice, // Usar el precio calculado
      basePrice: basePrice.toString(), // Enviar basePrice como string
      additionalCharge:
          additionalCharge.toString(), // Enviar additionalCharge como string
      platillo: _selectedImage,
      uid: userAut!.uid,
      nombreArchivo: fileName,
      coordenadasRestaurante:
          '${_restaurantCoordinates!.latitude}, ${_restaurantCoordinates!.longitude}',
    );

    if (mounted) {
      setState(() {
        _isSubmitting = false;
      });

      if (result['estatus'] == "Correcto") {
        print("*********************************");
        print(result['idP']);
        _socket.emit('pedidoCreado', result['idP']);
        showErrorFlushbar("Se ha creado el pedido con éxito", false, context);

        Future.delayed(const Duration(milliseconds: 1000), () {
          Navigator.of(context).pop(true);
          Navigator.of(context).pop(true);
        });
      } else {
        showErrorFlushbar(result['msg'] ?? "Ocurrió un error", true, context);
      }
    }
  }

  void _showAlert(String message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Advertencia'),
        content: Text(message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('OK'),
          ),
        ],
      ),
    );
  }

  void _showImagePreview() {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        child: Container(
          child: _selectedImage != null
              ? Image.file(
                  _selectedImage!,
                )
              : const Text('No hay imagen seleccionada'),
        ),
      ),
    );
  }

  void _updateMapCamera() async {
    if (_mapController != null && _restaurantCoordinates != null) {
      await Future.delayed(Duration(seconds: 1)); // Esperar un segundo
      _mapController!.animateCamera(
        CameraUpdate.newLatLngBounds(
          LatLngBounds(
            southwest: LatLng(
              _restaurantCoordinates!.latitude < widget.location.latitude
                  ? _restaurantCoordinates!.latitude
                  : widget.location.latitude,
              _restaurantCoordinates!.longitude < widget.location.longitude
                  ? _restaurantCoordinates!.longitude
                  : widget.location.longitude,
            ),
            northeast: LatLng(
              _restaurantCoordinates!.latitude > widget.location.latitude
                  ? _restaurantCoordinates!.latitude
                  : widget.location.latitude,
              _restaurantCoordinates!.longitude > widget.location.longitude
                  ? _restaurantCoordinates!.longitude
                  : widget.location.longitude,
            ),
          ),
          50,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Realizar Pedido'),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 16.0),
                const SizedBox(width: 8.0),
                const Text(
                  "Envío a:",
                  style: TextStyle(fontSize: 16.0),
                ),
                Row(
                  children: [
                    const Icon(Icons.location_on),
                    const SizedBox(width: 8.0),
                    Expanded(
                      child: Text(
                        widget.address,
                        style: const TextStyle(fontSize: 16.0),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                // Añadimos el mapa aquí
                SizedBox(
                  height: 200,
                  child: _buildGoogleMap(),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  controller: _generalDescriptionController,
                  decoration: const InputDecoration(
                    labelText: 'Descripción general',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  controller: _detailedDescriptionController,
                  maxLines: 3,
                  decoration: const InputDecoration(
                    labelText: 'Datos del cliente',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'Distancia: $_distanceInKm',
                  style: const TextStyle(
                    fontSize: 16.0,
                    color: Colors.black54,
                  ),
                ),
                const SizedBox(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: TextField(
                        controller: _priceController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          labelText: 'Precio',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.circular(8.0), // Bordes redondeados
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 2), // Sombra en x y y
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Text(
                            'Total: ',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.black, // Color de texto blanco
                            ),
                          ),
                          Text(
                            '$_calculatedPrice',
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.black, // Color de texto blanco
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                ElevatedButton.icon(
                  onPressed: _isSubmitting ? null : _pickImage,
                  icon: const Icon(Icons.camera_alt_outlined),
                  label: const Text('Tomar fotografía del platillo'),
                ),
                if (_imageFuture != null)
                  FutureBuilder<File?>(
                    future: _imageFuture,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Padding(
                          padding: EdgeInsets.only(top: 16.0),
                          child: CircularProgressIndicator(),
                        );
                      } else if (snapshot.hasError) {
                        return const Padding(
                          padding: EdgeInsets.only(top: 16.0),
                          child: Text('Error al cargar la imagen'),
                        );
                      } else if (snapshot.hasData) {
                        _selectedImage = snapshot.data;
                        return GestureDetector(
                          onTap: _showImagePreview,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 16.0),
                            child: Image.file(
                              _selectedImage!,
                              height: 150,
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                const SizedBox(height: 16.0),
              ],
            ),
          ),
          if (_isSubmitting)
            ModalBarrier(
              dismissible: false,
              color: Colors.black.withOpacity(0.5),
            ),
          if (_isSubmitting)
            const Center(
              child: CircularProgressIndicator(),
            ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ElevatedButton(
              onPressed: _isSubmitting ? null : _finalizeOrder,
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(double.infinity, 50),
              ),
              child: _isSubmitting
                  ? const CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    )
                  : const Text('Solicitar repartidor'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGoogleMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: _restaurantCoordinates ?? widget.location,
        zoom: 14,
      ),
      polylines: {
        if (_polylineCoordinates.isNotEmpty)
          Polyline(
            polylineId: PolylineId('route'),
            points: _polylineCoordinates,
            color: Colors.blue,
            width: 5,
          ),
      },
      markers: {
        if (_restaurantCoordinates != null)
          Marker(
            markerId: MarkerId('origin'),
            position: _restaurantCoordinates!,
            icon: _motoIcon ??
                BitmapDescriptor.defaultMarker, // Usar icono personalizado
          ),
        Marker(
            markerId: MarkerId('destination'),
            position: widget.location,
            icon: _selectedIcon ??
                BitmapDescriptor.defaultMarker, // Usar el nuevo icono
            infoWindow: (InfoWindow(
                title:
                    '${widget.location.latitude.toStringAsFixed(6)},${widget.location.longitude.toStringAsFixed(6)}'))),
      },
      onMapCreated: (GoogleMapController controller) {
        _mapController = controller;
        _updateMapCamera();
        Future.delayed(Duration(milliseconds: 100), () {
          controller.showMarkerInfoWindow(MarkerId('destination'));
        });
      },
    );
  }
}
