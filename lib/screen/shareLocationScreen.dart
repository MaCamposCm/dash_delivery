import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_webservice/places.dart' as loc;

class ShareLocationScreen extends StatefulWidget {
  final Function(String, LatLng) onLocationSelected;

  const ShareLocationScreen({
    super.key,
    required this.onLocationSelected,
  });

  @override
  ShareLocationScreenState createState() => ShareLocationScreenState();
}

class ShareLocationScreenState extends State<ShareLocationScreen> {
  final Completer<GoogleMapController> _controller = Completer();
  final TextEditingController _linkController = TextEditingController();
  Marker? selectedMarker;
  final String googleApiKey =
      'AIzaSyAYOJw3A5LEe6qmMlaiSbIEVGhuBQ9iui0'; // Reemplaza con tu API key
  LatLng? selectedLatLng;

  Future<void> _parseLinkAndMoveCamera(String link) async {
    try {
      final uri = Uri.parse(link);
      if (uri.path == '/maps' && uri.queryParameters.containsKey('q')) {
        final query = uri.queryParameters['q'];
        if (query != null) {
          final coordinates = query.split(',');
          if (coordinates.length == 2) {
            final lat = double.tryParse(coordinates[0]);
            final lng = double.tryParse(coordinates[1]);
            if (lat != null && lng != null) {
              final latLng = LatLng(lat, lng);
              final GoogleMapController controller = await _controller.future;
              controller.animateCamera(CameraUpdate.newLatLng(latLng));
              _updateSelectedLocation(latLng);
            }
          }
        }
      }
    } catch (e) {}
  }

  Future<void> _updateSelectedLocation(LatLng latLng) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(latLng.latitude, latLng.longitude);
      if (mounted) {
        setState(() {
          selectedMarker = Marker(
            markerId: MarkerId('selectedLocation'),
            position: latLng,
          );
          selectedLatLng = latLng;
        });
      }
    } catch (e) {}
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void dispose() {
    _linkController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ingresar Enlace de Dirección'),
      ),
      body: Stack(
        children: [
          GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: LatLng(0, 0),
              zoom: 2,
            ),
            markers: selectedMarker != null ? {selectedMarker!} : {},
          ),
          Positioned(
            top: 10,
            left: 10,
            right: 10,
            child: Column(
              children: [
                TextField(
                  controller: _linkController,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Ingresar enlace de Google Maps...',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.paste),
                      onPressed: () {
                        if (_linkController.text.isNotEmpty) {
                          _parseLinkAndMoveCamera(_linkController.text);
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 20,
            left: 20,
            right: 20,
            child: ElevatedButton(
              onPressed: () {
                if (selectedLatLng != null) {
                  widget.onLocationSelected(
                    _linkController.text,
                    selectedLatLng!,
                  );
                  Navigator.pop(context);
                }
              },
              child: Text('Guardar dirección'),
            ),
          ),
        ],
      ),
    );
  }
}
