import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dash_delivery/data/baseUrl.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';

class LoginService extends ChangeNotifier {
  final String baseUrl = Enviroment.baseUrlDev;

  Future<String?> login(String correo, String uid) async {
    return _makePostRequest(
      path: '/login',
      queryParams: {
        'correo': correo,
        'uid': uid,
        'token': PreferencesUser().token
      },
    ).then((response) {
      if (response.containsKey('token')) {
        return response['token'] == 'okczc'
            ? response['tipo'] ?? "validar"
            : 'Ocurrió un error: ${response['msg'] ?? 'Error desconocido'}';
      } else {
        return 'Ocurrió un error: ${response['error'] ?? 'Error desconocido'}';
      }
    });
  }

  Future<Map<String, dynamic>> registroPedidos({
    required String ubicacion,
    required String coordenadas,
    required String telefono,
    required String categoria,
    required String inefrente,
    required String ineatras,
    required String uid,
    required String nombre,
  }) async {
    return _makeJsonPostRequest(
      path: '/registroPedido',
      body: {
        'ubicacion': ubicacion,
        'coordenadas': coordenadas,
        'telefono': telefono,
        'categoria': categoria,
        'inefrente': inefrente,
        'ineatras': ineatras,
        'uid': uid,
        'local': nombre
      },
    );
  }

  Future<Map<String, dynamic>> registroRepartidorr({
    required String marcaModeloVehiculo,
    required String ineFrente,
    required String ineAtras,
    required String licencia,
    required String mochila,
    required String nombreContacto,
    required String telefonoContacto,
    required String parentesco,
    required String uid,
  }) async {
    return _makeJsonPostRequest(
      path: '/registroRepartidor',
      body: {
        'marcaModeloVehiculo': marcaModeloVehiculo,
        'ineFrente': ineFrente,
        'ineAtras': ineAtras,
        'licencia': licencia,
        'mochila': mochila,
        'nombreContacto': nombreContacto,
        'telefonoContacto': telefonoContacto,
        'parentesco': parentesco,
        'uid': uid,
      },
    );
  }

  Future<Map<String, dynamic>> registroRepartidor({
    required String marcaModeloVehiculo,
    required File ineFrente,
    required File ineAtras,
    required File licencia,
    required File mochila,
    required String nombreContacto,
    required String telefonoContacto,
    required String parentesco,
    required String uid,
  }) async {
    return _makeMultipartPostRequest(
      path: '/registroRepartidor',
      fields: {
        'marcaModeloVehiculo': marcaModeloVehiculo,
        'nombreContacto': nombreContacto,
        'telefonoContacto': telefonoContacto,
        'parentesco': parentesco,
        'uid': uid,
      },
      files: [
        {'name': 'ineFrente', 'file': ineFrente},
        {'name': 'ineAtras', 'file': ineAtras},
        {'name': 'licencia', 'file': licencia},
        {'name': 'mochila', 'file': mochila},
      ],
    );
  }

  Future<Map<String, dynamic>> registroRestaurante({
    required String nombreRestaurante,
    required String direccion,
    required String coordenadas,
    required String telefono,
    required String categoria,
    required String uid,
    required File ineFrente,
    required File ineAtras,
  }) async {
    return _makeMultipartPostRequest(
      path: '/registroRestaurante',
      fields: {
        'nombreRestaurante': nombreRestaurante,
        'direccion': direccion,
        'coordenadas': coordenadas,
        'telefono': telefono,
        'categoria': categoria,
        'uid': uid,
      },
      files: [
        {'name': 'ineFrente', 'file': ineFrente},
        {'name': 'ineAtras', 'file': ineAtras},
      ],
    );
  }

  // Método para realizar solicitudes POST con contenido JSON
  Future<Map<String, dynamic>> _makeJsonPostRequest({
    required String path,
    required Map<String, dynamic> body,
  }) async {
    try {
      final url = Uri.http(baseUrl, path);
      final resp = await http.post(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(body),
      );

      return _handleResponse(resp);
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'No se pudo conectar con el servidor: $e'
      };
    }
  }

  // Método para realizar solicitudes POST multipart
  Future<Map<String, dynamic>> _makeMultipartPostRequest({
    required String path,
    required Map<String, String> fields,
    required List<Map<String, dynamic>> files,
  }) async {
    try {
      final url = Uri.http(baseUrl, path);
      var request = http.MultipartRequest('POST', url);

      request.fields.addAll(fields);

      for (var file in files) {
        request.files.add(
            await http.MultipartFile.fromPath(file['name'], file['file'].path));
      }

      var response = await request.send();
      var responseBody = await response.stream.bytesToString();
      return _handleResponse(http.Response(responseBody, response.statusCode));
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'No se pudo conectar con el servidor: $e'
      };
    }
  }

  // Método para manejar la respuesta de las solicitudes
  Map<String, dynamic> _handleResponse(http.Response response) {
    if (response.statusCode != 200) {
      return {
        'estatus': 'Error',
        'msg': 'Error en la solicitud: ${response.statusCode}'
      };
    }

    try {
      final Map<String, dynamic> decodeResp =
          json.decode(response.body.toString());
      return decodeResp;
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'Error al decodificar la respuesta: $e'
      };
    }
  }

  // Método para realizar solicitudes POST simples
  Future<Map<String, dynamic>> _makePostRequest({
    required String path,
    Map<String, String>? queryParams,
  }) async {
    try {
      final url = Uri.http(baseUrl, path, queryParams);
      final resp = await http.post(url);

      return _handleResponse(resp);
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'No se pudo conectar con el servidor: $e'
      };
    }
  }
}
