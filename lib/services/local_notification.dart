import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class LocalNotification {
  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  // Solicitar permiso para mostrar notificaciones locales
  static Future<void> requestPermissionLocalNotifications() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.requestNotificationsPermission();
  }

  // Inicialización de notificaciones locales
  static Future<void> initializeLocalNotifications() async {
    const initializationSettingsAndroid =
        AndroidInitializationSettings('launcher_icon');
    const initializationSettingsDarwin = DarwinInitializationSettings(
        onDidReceiveLocalNotification: iosShowNotification);

    const initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsDarwin);

    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  // Mostrar notificaciones en iOS
  static void iosShowNotification(
      int id, String? title, String? body, String? data) {
    showLocalNotification(id: id, title: title, body: body, data: data);
  }

  // Mostrar notificaciones en Android y iOS
  static void showLocalNotification({
    required int id,
    String? title,
    String? body,
    String? data,
  }) {
    final androidDetails = AndroidNotificationDetails(
      'channelId',
      'channelName',
      playSound: true,
      importance: Importance.max,
      priority: Priority.high,
      styleInformation: BigTextStyleInformation(
        body ?? '',
        contentTitle: title?.isNotEmpty == true ? title : null,
        summaryText: body,
      ),
    );

    final notificationDetails = NotificationDetails(
      android: androidDetails,
      iOS: const DarwinNotificationDetails(
        presentSound: true,
        presentAlert: true,
      ),
    );

    flutterLocalNotificationsPlugin.show(
      id,
      title?.isNotEmpty == true ? title : null,
      body,
      notificationDetails,
    );
  }
}
