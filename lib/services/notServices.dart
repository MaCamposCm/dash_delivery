import 'dart:async';
import 'dart:math';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/services/local_notification.dart';

class NotiService {
  static FirebaseMessaging messaging = FirebaseMessaging.instance;
  static String? token;
  static final StreamController<String> _msgstreamController =
      StreamController.broadcast();

  static Stream<String> get messagesStream => _msgstreamController.stream;

  // Handler para notificaciones en segundo plano
  static Future _backgroundHandler(RemoteMessage message) async {
    _msgstreamController.add(message.notification?.title ?? '');

    var title = message.notification?.title ?? message.data['title'];
    var body = message.notification?.body ?? message.data['body'];

    if (title != null && body != null) {
      Random random = Random();
      var id = random.nextInt(100000);
      LocalNotification.showLocalNotification(
        id: id,
        title: title,
        body: body,
      );
    }
  }

  // Handler para notificaciones en primer plano
  static Future _onMessageHandler(RemoteMessage message) async {
    var title = message.notification?.title ?? message.data['title'];
    var body = message.notification?.body ?? message.data['body'];

    if (title != null && body != null) {
      Random random = Random();
      var id = random.nextInt(100000);
      LocalNotification.showLocalNotification(
        id: id,
        title: title,
        body: body,
      );
    }
  }

  // Handler para cuando se abre la app desde una notificación
  static Future _onMessageOpenApp(RemoteMessage message) async {
    var title = message.notification?.title ?? message.data['title'];
    var body = message.notification?.body ?? message.data['body'];

    if (title != null && body != null) {
      Random random = Random();
      var id = random.nextInt(100000);
      LocalNotification.showLocalNotification(
        id: id,
        title: title,
        body: body,
      );
    }
  }

  // Inicialización del servicio de Firebase para notificaciones
  static Future initializeApp() async {
    print("aa");
    final settings = await messaging.getNotificationSettings();
    if (settings.authorizationStatus != AuthorizationStatus.authorized) return;

    token = await messaging.getToken();
    print(token);
    final prefs = PreferencesUser();
    if (token != null) prefs.tokenTel = token!;

    FirebaseMessaging.onBackgroundMessage(_backgroundHandler);
    FirebaseMessaging.onMessage.listen(_onMessageHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenApp);
  }

  static closeStreams() {
    _msgstreamController.close();
  }
}
