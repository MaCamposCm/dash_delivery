import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart' as places;

class PlacesSearch extends SearchDelegate<places.Prediction?> {
  final Future<List<places.Prediction>> Function(String) searchFunction;
  final void Function(places.Prediction) onPlaceSelected;

  PlacesSearch({required this.searchFunction, required this.onPlaceSelected});

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder<List<places.Prediction>>(
      future: searchFunction(query),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          }
          if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Center(child: Text('No se encontraron resultados'));
          }
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              final prediction = snapshot.data![index];
              return ListTile(
                title: Text(prediction.description!),
                onTap: () {
                  onPlaceSelected(prediction);
                  close(context, prediction);
                },
              );
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return buildResults(context);
  }
}
