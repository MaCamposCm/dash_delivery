import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dash_delivery/data/baseUrl.dart';

class PlatilloService extends ChangeNotifier {
  final String baseUrl = Enviroment.baseUrlDev;
  Future<Map<String, dynamic>> registroPedidos({
    required String ubicacion,
    required String coordenadas,
    required String descripcion,
    required String detalle,
    required String precio,
    required String basePrice, // Nuevo parámetro
    required String additionalCharge, // Nuevo parámetro

    File? platillo,
    required String uid,
    required String nombreArchivo,
    required String coordenadasRestaurante,
  }) async {
    http.Response? resp;

    final DateTime now = DateTime.now();
    final String formattedDate = now.toIso8601String(); // Formato ISO 8601

    final fields = {
      'ubicacion': ubicacion,
      'coordenadas': coordenadas,
      'descripcion': descripcion,
      'detalle': detalle,
      'precio': precio,
      'basePrice': basePrice, // Incluye el basePrice en la solicitud
      'additionalCharge':
          additionalCharge, // Incluye el additionalCharge en la solicitud

      'uid': uid,
      'fechahora': formattedDate,
      'nombreArchivo': nombreArchivo,
      'coordenadasRestaurante': coordenadasRestaurante,
    };

    final files = platillo != null
        ? [
            {
              'name': 'platillo',
              'file': platillo,
            }
          ]
        : <Map<String, dynamic>>[];
    ;

    return _makeMultipartPostRequest(
        path: '/realizarPedido', fields: fields, files: files);
  }

  // Future<Map<String, dynamic>> registroPedidos(
  //     {required String ubicacion,
  //     required String coordenadas,
  //     required String descripcion,
  //     required String detalle,
  //     required String precio,
  //     required File platillo,
  //     required String uid,
  //     required String nombreArchivo,
  //     required String coordenadasRestaurante}) async {
  //   http.Response? resp;

  //   final DateTime now = DateTime.now();
  //   final String formattedDate = now.toIso8601String(); // Formato ISO 8601

  //   return _makeMultipartPostRequest(path: '/realizarPedido', fields: {
  //     'ubicacion': ubicacion,
  //     'coordenadas': coordenadas,
  //     'descripcion': descripcion,
  //     'detalle': detalle,
  //     'precio': precio,
  //     'uid': uid,
  //     'fechahora': formattedDate,
  //     'nombreArchivo': nombreArchivo,
  //     'coordenadasRestaurante': coordenadasRestaurante
  //   }, files: [
  //     {
  //       'name': 'platillo',
  //       'file': platillo
  //     } // Usa el nombre del archivo formateado
  //   ]);

  // }

  Future<List<dynamic>> obtenerPedidosRepa() async {
    http.Response? resp;

    String token = PreferencesUser().token;
    try {
      final url = Uri.http(baseUrl, '/obtenerPedidosRep', {'token': token});

      resp = await http.get(url);

      if (resp.statusCode != 200) {
        throw Exception('Error en la solicitud: ${resp.statusCode}');
      }
    } on http.ClientException catch (e) {
      throw Exception("ClientException: $e");
    } on SocketException catch (e) {
      throw Exception("SocketException: $e");
    } catch (e) {
      throw Exception("Error general: $e");
    }

    final List<dynamic> decodeResp;
    try {
      decodeResp = json.decode(resp.body);
    } catch (e) {
      throw Exception("Eren ror al decodificar la respuesta: $e");
    }

    return decodeResp;
  }

  Future<List<dynamic>> obtenerPedidos() async {
    http.Response? resp;

    String token = PreferencesUser().token;
    try {
      final url = Uri.http(baseUrl, '/obtenerPedidos', {'token': token});

      resp = await http.get(url);

      if (resp.statusCode != 200) {
        throw Exception('Error en la solicitud: ${resp.statusCode}');
      }
    } on http.ClientException catch (e) {
      throw Exception("ClientException: $e");
    } on SocketException catch (e) {
      throw Exception("SocketException: $e");
    } catch (e) {
      throw Exception("Error general: $e");
    }

    final List<dynamic> decodeResp;
    try {
      decodeResp = json.decode(resp.body);
    } catch (e) {
      throw Exception("Eren ror al decodificar la respuesta: $e");
    }

    return decodeResp;
  }

  Future<String?> obtenerImagensa(int id) async {
    final url = Uri.http(baseUrl, '/getImg', {'id': id.toString()});
    try {
      final resp = await http.get(url);
      if (resp.statusCode == 200 && resp.body.isNotEmpty) {
        return resp.body;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  // Método para realizar solicitudes POST multipart
  Future<Map<String, dynamic>> _makeMultipartPostRequest({
    required String path,
    required Map<String, String> fields,
    required List<Map<String, dynamic>> files,
  }) async {
    try {
      final url = Uri.http(baseUrl, path);
      var request = http.MultipartRequest('POST', url);

      request.fields.addAll(fields);

      for (var file in files) {
        request.files.add(
            await http.MultipartFile.fromPath(file['name'], file['file'].path));
      }

      var response = await request.send();
      var responseBody = await response.stream.bytesToString();
      return _handleResponse(http.Response(responseBody, response.statusCode));
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'No se pudo conectar con el servidor: $e'
      };
    }
  }

  // Método para manejar la respuesta de las solicitudes
  Map<String, dynamic> _handleResponse(http.Response response) {
    if (response.statusCode != 200) {
      return {
        'estatus': 'Error',
        'msg': 'Error en la solicitud: ${response.statusCode}'
      };
    }

    try {
      final Map<String, dynamic> decodeResp =
          json.decode(response.body.toString());
      return decodeResp;
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'Error al decodificar la respuesta: $e'
      };
    }
  }

//nuevos

  // Método para obtener la imagen del servidor
  Future<Uint8List?> obtenerImagena(int id) async {
    final url = Uri.http(baseUrl, '/getImg', {'id': id.toString()});
    try {
      final resp = await http.get(url);
      if (resp.statusCode == 200 && resp.bodyBytes.isNotEmpty) {
        return resp.bodyBytes; // Devuelve los bytes de la imagen
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<Uint8List?> obtenerImagenO(String id) async {
    final url = Uri.http(baseUrl, '/getImgO', {'id': id.toString()});
    try {
      final resp = await http.get(url);
      if (resp.statusCode == 200 && resp.bodyBytes.isNotEmpty) {
        return resp.bodyBytes; // Devuelve los bytes de la imagen
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<dynamic>> obtenerPedidosRealizados(String uid) async {
    http.Response? resp;
    try {
      final url = Uri.http(baseUrl, '/obtenerPedidosRealizados', {'uid': uid});

      resp = await http.get(url);

      if (resp.statusCode != 200) {
        throw Exception('Error en la solicitud: ${resp.statusCode}');
      }
    } on http.ClientException catch (e) {
      throw Exception("ClientException: $e");
    } on SocketException catch (e) {
      throw Exception("SocketException: $e");
    } catch (e) {
      throw Exception("Error general: $e");
    }

    final List<dynamic> decodeResp;
    try {
      decodeResp = json.decode(resp.body);
    } catch (e) {
      throw Exception("Eren ror al decodificar la respuesta: $e");
    }

    return decodeResp;
  }
}
