import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dash_delivery/data/baseUrl.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image/image.dart' as img;
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class LoginService extends ChangeNotifier {
  final String baseUrl = Enviroment.baseUrlDev;

  Future<String?> login(String correo, String uid, BuildContext context) async {
    Completer<String?> completer = Completer<String?>();
    final socket = Provider.of<SocketSerDos>(context, listen: false);

    socket.emit('login', {
      'correo': correo,
      'uid': uid,
      'token': PreferencesUser().tokenTel,
    });

    void handleLoginResponse(data) {
      final userAut = AuthGoogleU().dataUser();
      if (!completer.isCompleted) {
        if (data.containsKey('token')) {
          if (data['token'] == 'okczc') {
            if (data['tipo'] == null || data['tipo'].isEmpty) {
              PreferencesUser().token = userAut!.uid;

              completer.complete("validar");
            } else {
              final estatus = data['estatus'];

              if (userAut != null) {
                PreferencesUser().token = userAut.uid;
                socket.emit(
                    'obtenerCoordenadasRestaurante', {'uid': userAut.uid});
                socket.socketS().on('respuestaCoordenadasRestaurante', (data) {
                  // if (mounted) {
                  final lat = data['latitude'];
                  final lng = data['longitude'];

                  PreferencesUser().coordenadas = '$lat , $lng';
                });
              }

              final tipo = data['tipo'];
              if (estatus == "pendiente") {
                completer.complete("PendienteScreen");
              } else if (estatus == "bloqueado") {
                completer.complete("BloqueadoScreen");
              } else {
                completer.complete(tipo);
              }
            }
          } else {
            completer.complete(
                'Ocurrió un error: ${data['msg'] ?? 'Error desconocido'}');
          }
        } else {
          completer.complete(
              'Ocurrió un error: ${data['error'] ?? 'Error desconocido'}');
        }
        socket.socketS().off('loginResponse', handleLoginResponse);
      }
    }

    // void handleLoginResponse(data) {
    //   if (!completer.isCompleted) {
    //     if (data.containsKey('token')) {
    //       if (data['token'] == 'okczc') {

    //         completer.complete(data['tipo'] ?? "validar");

    //       } else {
    //         completer.complete(
    //             'Ocurrió un error: ${data['msg'] ?? 'Error desconocido'}');
    //       }
    //     } else {
    //       completer.complete(
    //           'Ocurrió un error: ${data['error'] ?? 'Error desconocido'}');
    //     }
    //     socket.socketS().off('loginResponse', handleLoginResponse);
    //   }
    // }

    socket.socketS().on('loginResponse', handleLoginResponse);

    return completer.future;
  }

  Future<List<int>> _resizeAndConvertToBytes(File imageFile) async {
    try {
      return await compute(_resizeImage, imageFile);
    } catch (e) {
      return [];
    }
  }

  List<int> _resizeImage(File imageFile) {
    final originalBytes = imageFile.readAsBytesSync();
    final image = img.decodeImage(originalBytes);

    if (image != null) {
      const maxWidth = 800;
      const maxHeight = 800;
      final resizedImage =
          img.copyResize(image, width: maxWidth, height: maxHeight);
      return img.encodePng(resizedImage);
    }
    return originalBytes;
  }

  Future<Map<String, dynamic>> registroRestauranteHtml(
      {required String uid,
      required String tipo,
      required File ineFrente,
      required File ineAtras,
      File? licencia,
      File? mochila,
      File? repartidorPhoto}) async {
    final ineFrenteBytes = ineFrente;
    final ineAtrasBytes = ineAtras;
    return _makeMultipartPostRequest(
      path: '/registroRestaurante',
      fields: {'uid': uid, 'tipo': tipo},
      files: tipo == "rep"
          ? [
              {
                'name': 'ineFrente',
                'file': ineFrenteBytes,
              },
              {
                'name': 'ineAtras',
                'file': ineAtrasBytes,
              },
              {
                'name': 'mochila',
                'file': mochila,
              },
              {
                'name': 'licencia',
                'file': licencia,
              },
              {
                'name': 'repartidorPhoto',
                'file': repartidorPhoto,
              },
            ]
          : [
              {
                'name': 'ineFrente',
                'file': ineFrenteBytes,
              },
              {
                'name': 'ineAtras',
                'file': ineAtrasBytes,
              },
            ],
    );
  }

  // Método para realizar solicitudes POST multipart
  Future<Map<String, dynamic>> _makeMultipartPostRequest({
    required String path,
    required Map<String, String> fields,
    required List<Map<String, dynamic>> files,
  }) async {
    try {
      final url = Uri.http(baseUrl, path);
      var request = http.MultipartRequest('POST', url);

      request.fields.addAll(fields);

      for (var file in files) {
        request.files.add(
            await http.MultipartFile.fromPath(file['name'], file['file'].path));
      }

      var response = await request.send();
      var responseBody = await response.stream.bytesToString();
      return _handleResponse(http.Response(responseBody, response.statusCode));
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'No se pudo conectar con el servidor: $e'
      };
    }
  }

  // Método para manejar la respuesta de las solicitudes
  Map<String, dynamic> _handleResponse(http.Response response) {
    if (response.statusCode != 200) {
      return {
        'estatus': 'Error',
        'msg': 'Error en la solicitud: ${response.statusCode}'
      };
    }

    try {
      final Map<String, dynamic> decodeResp =
          json.decode(response.body.toString());
      return decodeResp;
    } catch (e) {
      return {
        'estatus': 'Error',
        'msg': 'Error al decodificar la respuesta: $e'
      };
    }
  }

  Future<Map<String, dynamic>> registroRestaurante({
    required String nombreRestaurante,
    required String direccion,
    required String coordenadas,
    required String telefono,
    required String categoria,
    required String uid,
    required File ineFrente,
    required File ineAtras,
    required BuildContext context,
  }) async {
    final socketService = Provider.of<SocketSerDos>(context, listen: false);
    PreferencesUser().coordenadas = coordenadas;
    try {
      // final ineFrenteBytes = await _resizeImage(ineFrente);
      // final ineAtrasBytes = await _resizeImage(ineAtras);

      final data = {
        'nombreRestaurante': nombreRestaurante,
        'direccion': direccion,
        'coordenadas': coordenadas,
        'telefono': telefono,
        'categoria': categoria,
        'uid': uid,
        // 'ineFrente': {
        //   'filename': ineFrente.path.split('/').last,
        //   'data': base64Encode(ineFrenteBytes),
        // },
        // 'ineAtras': {
        //   'filename': ineAtras.path.split('/').last,
        //   'data': base64Encode(ineAtrasBytes),
        // },
      };

      final completer = Completer<Map<String, dynamic>>();

      socketService.socketS().once('registroRestauranteResponse', (response) {
        completer.complete(_handleResponsesocket(response));
      });

      socketService.emit('registroRestaurante', data);
      return registroRestauranteHtml(
          uid: uid, ineFrente: ineFrente, ineAtras: ineAtras, tipo: "res");
      // return completer.future;
    } catch (e) {
      return {'estatus': 'Error', 'msg': 'Error al enviar datos: $e'};
    }
  }

  Future<File> _writeBytesToFile(List<int> bytes, String filename) async {
    final tempDir = Directory.systemTemp;
    final tempFile = File('${tempDir.path}/$filename');
    await tempFile.writeAsBytes(bytes);
    return tempFile;
  }

  Future<Map<String, dynamic>> registroRepartidorHtml({
    required String uid,
    required File ineFrente,
    required File ineAtras,
    required File licencia,
    required File mochila,
  }) async {
    final ineFrenteBytes = await _resizeAndConvertToBytes(ineFrente);
    final ineAtrasBytes = await _resizeAndConvertToBytes(ineAtras);
    final licenciaBytes = await _resizeAndConvertToBytes(licencia);
    final mochilaBytes = await _resizeAndConvertToBytes(mochila);

    final ineFrenteTemp =
        await _writeBytesToFile(ineFrenteBytes, 'ineFrente.png');
    final ineAtrasTemp = await _writeBytesToFile(ineAtrasBytes, 'ineAtras.png');
    final licenciaTemp = await _writeBytesToFile(licenciaBytes, 'licencia.png');
    final mochilaTemp = await _writeBytesToFile(mochilaBytes, 'mochila.png');

    return _makeMultipartPostRequest(
      path: '/registroRepartidor',
      fields: {
        'uid': uid,
      },
      files: [
        {'name': 'ineFrente', 'file': ineFrenteTemp},
        {'name': 'ineAtras', 'file': ineAtrasTemp},
        {'name': 'licencia', 'file': licenciaTemp},
        {'name': 'mochila', 'file': mochilaTemp},
      ],
    );
  }

  Future<Map<String, dynamic>> registroRepartidor({
    required String marcaModeloVehiculo,
    required File ineFrente,
    required File ineAtras,
    required File licencia,
    required File mochila,
    required String nombreContacto,
    required String telefonoContacto,
    required String parentesco,
    required String uid,
    required BuildContext context,
    required File repartidorPhoto,
    required String nombreRepartidor,
  }) async {
    print("data entra en el coso este");
    final socketService = Provider.of<SocketSerDos>(context, listen: false);

    try {
      final data = {
        'marcaModeloVehiculo': marcaModeloVehiculo,
        'nombreContacto': nombreContacto,
        'telefonoContacto': telefonoContacto,
        'parentesco': parentesco,
        'nombreRepartidor': nombreRepartidor,
        'uid': uid,
      };

      final completer = Completer<Map<String, dynamic>>();

      socketService.socketS().once('registroRepartidorResponse', (response) {
        completer.complete(_handleResponsesocket(response));
      });

      socketService.emit('registroRepartidor', data);
      return registroRestauranteHtml(
          uid: uid,
          ineFrente: ineFrente,
          ineAtras: ineAtras,
          tipo: "rep",
          mochila: mochila,
          licencia: licencia,
          repartidorPhoto: repartidorPhoto);
      // return registroRepartidorHtml(
      //   uid: uid,
      //   ineFrente: ineFrente,
      //   ineAtras: ineAtras,
      //   licencia: licencia,
      //   mochila: mochila,
      // );
    } catch (e) {
      return {'estatus': 'Error', 'msg': 'Error al enviar datos: $e'};
    }
  }

  Map<String, dynamic> _handleResponsesocket(Map<String, dynamic> response) {
    if (response['estatus'] == 'Correcto') {
      return response;
    } else {
      return {
        'estatus': 'Error',
        'msg': response['msg'] ?? 'Error desconocido'
      };
    }
  }
}
