import 'package:dash_delivery/data/baseUrl.dart';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;
import 'package:socket_io_common/src/util/event_emitter.dart';

//! estatus interno del socket
enum ServerEstatus { online, offline, connecting }

String baseUrl = Enviroment.baseUrlDev;

class SocketSerDos with ChangeNotifier {
  ServerEstatus _serverStatus = ServerEstatus.connecting;
  late io.Socket _socket;

  ServerEstatus get serverStatus => _serverStatus;
  Function get emit => _socket.emit;

  SocketSerDos() {
    _initConfig();
  }

  void _initConfig() {
    _socket = io.io('http://$baseUrl', {
      'transports': ['websocket'],
      'reconnection': true,
    });

    _socket.onConnect((_) {
      _serverStatus = ServerEstatus.online;
      notifyListeners();
    });

    _socket.onDisconnect((_) {
      _serverStatus = ServerEstatus.offline;
      notifyListeners();
    });

    _socket.onConnectError((data) {
      notifyListeners();
    });

    _socket.onReconnectAttempt((attempt) {
      notifyListeners();
    });

    _socket.onReconnect((attempt) {
      _serverStatus = ServerEstatus.online;
      notifyListeners();
    });
  }

  io.Socket socketS() {
    return _socket;
  }

  void disconnect() {
    _socket.disconnect();
  }

  void connect() {
    if (_socket.disconnected) {
      _socket.connect();
    }
  }
}
