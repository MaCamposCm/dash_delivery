import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dash_delivery/data/baseUrl.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image/image.dart' as img;
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class VerificacionService extends ChangeNotifier {
  final String baseUrl = Enviroment.baseUrlDev;

  void verificar(BuildContext context, String uid) async {
    final socket = Provider.of<SocketSerDos>(context, listen: false);

    socket.emit('verificar', {
      'uid': uid,
    });
  }

  Future<String?> verificarUsuario(String uid, BuildContext context) async {
    Completer<String?> completer = Completer<String?>();
    final socket = Provider.of<SocketSerDos>(context, listen: false);

    socket.emit('verificarUsuario', {
      'uid': uid,
    });
    void handleResponse(data) {
      print(data);
      if (!completer.isCompleted) {
        if (data['estatus'] == 'aceptado') {
          if (data['tipo'] == 'restaurante') {
            PreferencesUser().rutaPrincipal = '/dashboard_negocio';
          } else if (data['tipo'] == 'repartidor') {
            PreferencesUser().rutaPrincipal = '/dashboard_repartidor';
          } else if (data['tipo'] == 'admin') {
            PreferencesUser().rutaPrincipal = '/admin';
          }
        }
        completer.complete(data['estatus']);
      }
    }

    socket.socketS().on('verificado', handleResponse);

    return completer.future;
  }

  Future<String?> verificarVersion(BuildContext context) async {
    Completer<String?> completer = Completer<String?>();
    final socket = Provider.of<SocketSerDos>(context, listen: false);

    socket.emit('verificarVersion');
    void handleResponse(data) {
      print(data);
      if (!completer.isCompleted) {
        completer.complete(data);
      }
    }

    socket.socketS().on('versionverificada', handleResponse);

    return completer.future;
  }
}
