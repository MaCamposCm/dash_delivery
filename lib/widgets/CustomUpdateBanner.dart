import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:ui'; // Para el BackdropFilter

class CustomUpdateBanner extends StatelessWidget {
  final String message;
  final String buttonText;
  final String updateUrl;
  final VoidCallback onClose;

  CustomUpdateBanner({
    required this.message,
    required this.buttonText,
    required this.updateUrl,
    required this.onClose,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        // Fondo desenfocado
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            color: Colors.black.withOpacity(0.5), // Color semitransparente
            width: double.infinity,
            height: double.infinity,
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16), // Margen lateral
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(13), // Esquinas redondeadas
            ),

            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/pickers.png',
                  height: 100,
                ),
                Text(
                  message,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                  textAlign: TextAlign.center, // Centrar el texto
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        final Uri uri = Uri.parse(updateUrl);
                        if (await canLaunchUrl(uri)) {
                          await launchUrl(uri);
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                                content: Text('No se pudo abrir el enlace')),
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.green,
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(30), // Bordes redondeados
                        ),
                      ),
                      child: Text(buttonText),
                    ),
                    // ElevatedButton(
                    //   onPressed: onClose,
                    //   style: ElevatedButton.styleFrom(
                    //     foregroundColor: Colors.white,
                    //     backgroundColor: Colors.red,
                    //     shape: RoundedRectangleBorder(
                    //       borderRadius:
                    //           BorderRadius.circular(30), // Bordes redondeados
                    //     ),
                    //   ),
                    //   child: Text("Cerrar"),
                    // ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
