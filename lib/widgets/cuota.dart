import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dash_delivery/services/socket.dart';
import 'package:dash_delivery/services/auth_google.dart';

class CuotaUser extends StatefulWidget {
  const CuotaUser({Key? key}) : super(key: key);

  @override
  _CuotaUserState createState() => _CuotaUserState();
}

class _CuotaUserState extends State<CuotaUser> {
  String _cuota = '0'; // Valor inicial de la cuota
  late SocketSerDos _socket; // Instancia del servicio de socket

  @override
  void initState() {
    super.initState();
    _socket = Provider.of<SocketSerDos>(context, listen: false);
    _obtenerCuota(); // Solicita la cuota

    // Escuchar evento de actualización de cuota
    _socket.socketS().on('cuotaActualizada', (data) {
      setState(() {
        _cuota = data['cuota'] ??
            '0'; // Actualizar la cuota o establecer 0 si es nula
      });
    });
  }

  void _obtenerCuota() async {
    final userAut = await AuthGoogleU().dataUser();
    final String uid = userAut!.uid;
    // Emitir evento para obtener la cuota
    _socket.emit('obtenerCuota', {'uid': uid});

    // Escuchar la respuesta del servidor
    _socket.socketS().once('respuestaCuota', (data) {
      setState(() {
        _cuota = data['cuota'] ??
            '0'; // Actualizar la cuota o establecer 0 si es nula
      });
    });
  }

  @override
  void dispose() {
    _socket.socketS().off('respuestaCuota'); // Limpia la suscripción al evento
    _socket
        .socketS()
        .off('cuotaActualizada'); // Limpia la suscripción al evento
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.circular(8.0), // Bordes redondeados
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 4,
              offset: Offset(0, 2), // Sombra en x y y
            ),
          ],
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'Cuota: ',
              style: TextStyle(
                fontSize: 18,
                color: Colors.white, // Color de texto blanco
              ),
            ),
            Text(
              '\$$_cuota',
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.white, // Color de texto blanco
              ),
            ),
          ],
        ),
      ),
    );
  }
}
