import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FloatingButtons extends StatelessWidget {
  final VoidCallback onYellowButtonPressed; // Callback para el botón amarillo
  final VoidCallback onRedButtonPressed; // Callback para el botón rojo

  const FloatingButtons({
    Key? key,
    required this.onYellowButtonPressed,
    required this.onRedButtonPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        // Botón flotante amarillo usando MaterialButton
        // Positioned(
        //   bottom: 80.0,
        //   right: 16.0,
        //   child: Column(
        //     children: [
        //       MaterialButton(
        //         onPressed: onYellowButtonPressed,
        //         color: Colors.yellow,
        //         textColor: Colors.black,
        //         shape: RoundedRectangleBorder(
        //           borderRadius: BorderRadius.circular(
        //               12.0), // Define el radio de los bordes redondeados
        //         ),
        //         padding: const EdgeInsets.all(16.0),
        //         child: const Text(
        //           '2',
        //           style: TextStyle(fontSize: 20),
        //         ),
        //       ),
        //       const SizedBox(height: 8.0),
        //       const Text(
        //         'Pedidos en progreso',
        //         style: TextStyle(color: Colors.black),
        //       ),
        //     ],
        //   ),
        // ),
        // Botón flotante rojo usando MaterialButton
        Positioned(
          bottom: 10.0,
          right: 0.0,
          child: MaterialButton(
            onPressed: onRedButtonPressed,
            color: Colors.red,
            textColor: Colors.white,
            shape: CircleBorder(),
            padding: const EdgeInsets.all(16.0),
            child: const Icon(
              Icons.help,
              size: 24,
            ),
          ),
        ),
      ],
    );
  }
}
