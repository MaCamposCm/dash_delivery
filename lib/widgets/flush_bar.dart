import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

void showErrorFlushbar(String message, bool isError, BuildContext context) {
  Flushbar(
    message: message,
    duration: const Duration(seconds: 3),
    backgroundColor: isError
        ? const Color.fromRGBO(205, 63, 63, 1)
        : const Color.fromRGBO(51, 96, 137, 1),
    messageColor: Colors.white,
    borderRadius: BorderRadius.circular(10.0),
    margin: const EdgeInsets.all(16.0),
    flushbarPosition: FlushbarPosition.TOP,
    icon: const Icon(
      Icons.error,
      color: Colors.white,
    ),
  ).show(context);
}
