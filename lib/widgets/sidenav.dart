import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';
import 'package:package_info_plus/package_info_plus.dart';

Drawer sidenav(BuildContext context) {
  final prefs = PreferencesUser();
  return Drawer(
    child: Column(
      children: <Widget>[
        const SizedBox(height: 50),
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.add),
                title: const Text('Dar de alta pedido'),
                onTap: () {
                  appRouters.pushReplacementNamed('/dashboard_negocio');
                },
              ),
              ListTile(
                leading: const Icon(Icons.check_circle),
                title: const Text('Ver estatus del pedido'),
                onTap: () {
                  appRouters.pushReplacementNamed('/estatusPedidos');
                },
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(0.0),
          child: Text('v${prefs.version}'),
        ),
        Padding(
            padding: const EdgeInsets.all(16.0),
            child: ElevatedButton(
              onPressed: () async {
                await FirebaseAuth.instance.signOut();
                await AuthGoogleU().logoutGoogle();
                // Acción para cerrar sesión+
                prefs.rutaPrincipal = '/login';
                prefs.coordenadas = '';
                appRouters.pushReplacementNamed('/login');
                prefs.token = '';
                prefs.tokenTel = '';
              },
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: Colors.red, // Color de fondo rojo
                minimumSize:
                    const Size(double.infinity, 50), // Tamaño del botón
              ),
              child: const Text('Cerrar sesión'),
            ))
      ],
    ),
  );
}
