import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';

Drawer sidenavB(BuildContext context) {
  final prefs = PreferencesUser();
  return Drawer(
    child: Column(
      children: <Widget>[
        const SizedBox(height: 50),
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: ElevatedButton(
            onPressed: () async {
              await FirebaseAuth.instance.signOut();
              await AuthGoogleU().logoutGoogle();
              // Acción para cerrar sesión+
              prefs.rutaPrincipal = '/login';
              appRouters.pushReplacementNamed('/login');
            },
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: Colors.red, // Color de fondo rojo
              minimumSize: const Size(double.infinity, 50), // Tamaño del botón
            ),
            child: const Text('Cerrar sesión'),
          ),
        ),
      ],
    ),
  );
}
