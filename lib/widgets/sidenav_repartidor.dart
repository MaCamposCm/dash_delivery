import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:dash_delivery/preferences/preferences_user.dart';
import 'package:dash_delivery/routes/app_routes.dart';
import 'package:dash_delivery/services/auth_google.dart';

Drawer sidenavRepartidor(BuildContext context) {
  final prefs = PreferencesUser();

  return Drawer(
    child: Column(
      children: <Widget>[
        const SizedBox(height: 50),
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.receipt),
                title: const Text('Pedidos pendientes'),
                onTap: () {
                  // Navegar a la pantalla de pedidos pendientes
                  Navigator.pop(context); // Cierra el Drawer
                  appRouters.pushReplacementNamed('/dashboard_repartidor');
                },
              ),
              ListTile(
                leading: const Icon(Icons.remove_red_eye),
                title: const Text('Ver mis pedidos'),
                onTap: () {
                  // Navegar a la pantalla de mis pedidos
                  Navigator.pop(context); // Cierra el Drawer
                  appRouters.pushReplacementNamed('/misPedidos');
                },
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: ElevatedButton(
            onPressed: () async {
              // Acción para cerrar sesión
              await FirebaseAuth.instance.signOut();
              await AuthGoogleU().logoutGoogle();
              prefs.rutaPrincipal = '/login';
              appRouters.pushReplacementNamed('/login');
            },
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: Colors.red, // Color de fondo rojo
              minimumSize: const Size(double.infinity, 50), // Tamaño del botón
            ),
            child: const Text('Cerrar sesión'),
          ),
        ),
      ],
    ),
  );
}
